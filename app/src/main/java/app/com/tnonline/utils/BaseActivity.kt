package app.com.tnonline.utils

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.constraint.ConstraintLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import app.com.tnonline.R
import app.com.tnonline.ui.selfservice.TransactionHistoryActivity

abstract class BaseActivity : AppCompatActivity() {


    var mContext: Context? = null
    private var progressBar: ProgressBar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)
        mContext = this
    }

    @get:LayoutRes
    protected abstract val layoutResId: Int

    fun displayToast(message: String) {
        Toast.makeText(mContext, "" + message, Toast.LENGTH_SHORT).show()
    }

    fun displayToastLong(message: String) {
        Toast.makeText(mContext, "" + message, Toast.LENGTH_LONG).show()
    }

    fun showError() {
        if (Utilities.isNetworkAvailable(mContext)) {
            displayToast(resources.getString(R.string.failed))
        } else {
            displayToast(resources.getString(R.string.network_offline))
        }
    }

    fun showProgressBar(parent: ViewGroup) {
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        progressBar = ProgressBar(this, null, android.R.attr.progressBarStyleLarge)
        progressBar?.indeterminateDrawable?.setColorFilter(ContextCompat.getColor(this, android.R.color.holo_blue_bright),
                android.graphics.PorterDuff.Mode.SRC_ATOP)
        when (parent) {
            is RelativeLayout -> {
                val params = RelativeLayout.LayoutParams(100, 100)
                params.addRule(RelativeLayout.CENTER_IN_PARENT)
                parent.addView(progressBar, params)
            }
            is FrameLayout -> {
                val params = FrameLayout.LayoutParams(100, 100)
                params.gravity = Gravity.CENTER
                parent.addView(progressBar, params)
            }
            is CoordinatorLayout -> {
                val params = CoordinatorLayout.LayoutParams(100, 100)
                params.gravity = Gravity.CENTER
                parent.addView(progressBar, params)
            }
            is ConstraintLayout -> {
                val params = ConstraintLayout.LayoutParams(100, 100)
                params.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                params.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                params.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                params.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                parent.addView(progressBar, params)
            }
        }
        progressBar?.visibility = View.VISIBLE  //To show ProgressBar
    }

    fun dismissProgressBar() {
        progressBar?.visibility = View.GONE     // To Hide ProgressBar
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }


    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
    }

    fun  isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }

    fun internetCheck(context: Context): Boolean {
        return isNetworkAvailable(context)
    }

    fun getStringCode(inputString: String): String? {
          var returnVal: String? = null
          if(inputString.contains("(")&& inputString.contains(")"))
          {
              Log.i("inputString",inputString)
              returnVal = inputString.substring(inputString.lastIndexOf("(") + 1)
              Log.i("inputString",returnVal)
              returnVal = returnVal.substring(0, returnVal.length - 1)
              Log.i("inputString",returnVal)

              return returnVal
          }
         return " "
    }

}