package app.com.tnonline.utils

import android.app.DatePickerDialog
import android.content.Context
import android.net.ConnectivityManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.widget.DatePicker
import android.widget.TextView
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

object Utilities {


    fun loadFragments(manager: FragmentManager, fragment: Fragment, frameId: Int) {
        val transaction = manager.beginTransaction()
        transaction.add(frameId, fragment)
        transaction.commit()
    }

    fun isNetworkAvailable(context: Context?): Boolean {
        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }

    fun isValidEmail(target: String): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

     fun isValidPassword(password: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val passwordPattern = "^(?=.*\\d)(?=.*[@\$!%*#?&])[A-Za-z\\d@\$!%*#?&]{8,}\$"
        //val passwordPattern = "^[a-zA-Z0-9]{8,}\$"
        pattern = Pattern.compile(passwordPattern)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }

    fun getDate(toDate: String?): String {
        var dateFinal = " "
        if(toDate!=null&&!toDate.equals("null")) {
            try {

                val formatter = SimpleDateFormat("dd/mm/yyyy HH:mm:ss")
                val date1 = formatter.parse(toDate)
                dateFinal = SimpleDateFormat("dd/mm/yyyy").format(date1)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        return dateFinal
    }
    fun getTime(toDate: String?): String {
        var dateFinal = " "
        if(toDate!= null && !toDate.equals("null")) {
            try {
                val formatter = SimpleDateFormat("dd/mm/yyyy HH:mm:ss")
                val date1 = formatter.parse(toDate)
                dateFinal = SimpleDateFormat("hh:mm aaa ").format(date1)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        return dateFinal
    }

    fun openDate(mContext: Context, edtDate: TextView) {
        try {

            val format1 = SimpleDateFormat("dd/MM/yyyy")
            val mCurrentDate1 = Calendar.getInstance()
            var  mYear = mCurrentDate1.get(Calendar.YEAR)
            var  mMonth = mCurrentDate1.get(Calendar.MONTH)
            var  mDay = mCurrentDate1.get(Calendar.DAY_OF_MONTH)

            val mDatePicker = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { datepicker, selectedyear, selectedmonth, selectedday ->
                mCurrentDate1.set(selectedyear, selectedmonth, selectedday)

                mYear = selectedyear
                mMonth = selectedmonth + 1
                mDay = selectedday
                edtDate.text = format1.format(mCurrentDate1.time)
            }, mYear, mMonth, mDay)


            mDatePicker.datePicker.minDate = Date().time - 10000 //After currentDate
            mDatePicker.show()

            //   cal.add(Calendar.DAY_OF_YEAR, -1);

            //  mDatePicker.getDatePicker().setMinDate(cal.getTimeInMillis());
            //  mDatePicker.show();


            // Before currentDate
            // mDatePicker.getDatePicker().findViewById(mContext.getResources().getIdentifier("year","id","android")).setVisibility(View.GONE);

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


}