package app.com.tnonline.utils

import app.com.tnonline.data.remote.models.events.FeatureModel

object Singleton {

    var sliderImageList=ArrayList<String>()
    var facility=ArrayList<String>()
    var venue=ArrayList<String>()
    var cuisines=ArrayList<String>()
    var amenities=ArrayList<String>()
    var dineTypes=ArrayList<String>()

    var eventType = ArrayList<String>()
    var cateringFeature = ArrayList<String>()
    var cateringCuisine = ArrayList<String>()
    var tentType = ArrayList<String>()
    var tableType = ArrayList<String>()

    var address:String?=""
    var headerImage:String?=""
    var nameOfHall:String?=""
    var contact:String?=""
    var email:String?=""
    var capacity:String?=""
    var description:String?=""
    var restaurant_id: String? = ""
}