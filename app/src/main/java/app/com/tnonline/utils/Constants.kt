package app.com.tnonline.utils

object Constants {

    const val BASE_URL = "http://ec2-13-233-137-92.ap-south-1.compute.amazonaws.com/"
    const val BASE_URL_TRAIN="https://api.railwayapi.com/v2/"
    //const val TRAIN_KEY="jtfkr0y3cc"
    const val TRAIN_KEY="3m70yjlnwg"
    const val SUCCESS = "success"
    const val ERROR = "error"
    const val SETTINGS="settings"
    const val CITY_NAME = "CITY_NAME"
    const val CITY_IMAGE = "CITY_IMAGE"
    const val CITY_ID = "CITY_ID"
    const val STATE_ID = "STATE_ID"

    const val LOGIN_SUCCESS = "LOGIN_SUCCESS"
    const val LOGIN_AUTH = "LOGIN_AUTH"
    const val LOGIN_EMAIL = "LOGIN_EMAIL"
    const val LOGIN_NAME = "LOGIN_NAME"
    const val DOMAIN = "tnonline.in"
    const val YES = "YES"
    const val NO = "NO"
    const val OPERATOR = "operator"
    const val CODE = "code"
    const val TYPE = "type"
    const val POST_PAID = "PostPaid"
    const val MOBILE = "Mobile"
    const val ORDER_ID= "ORDER_ID"
    const val RECHARGE_ID=  "RECHARGE_ID"
    const val FLIGHT_AREA=  "FLIGHT_AREA"
    const val FLIGHT_CITY=  "FLIGHT_CITY"
    const val FLIGHT_AREA_URL=  "FLIGHT_AREA_URL"
    const val TRAIN_INFO="TRAIN_INFO"
    const val STATION_CODE="STATION_CODE"
    const val INTERNETCONNECTION = "No Internet Connection"


    const val LocalEvent = "Local Event"
    const val PARTY_HALL = "Party Halls"
    const val EVENT_ORGE = "Event Organizer"
    const val EVENT_SUPP = "Event Supplies"

    //info
    const val FLIGHT = "Flight"
    const val TRAIN = "Train"
    const val Bus = "Bus"
    const val Emegency_service = "Emergency Services"

    const val vegetarian = "Vegetarian"
    const val non_veg = "Non Vegetarian"
    const val pizza = "Pizza & Fried Chicken"
    const val bakery = "Bakery & Confectionery"
    const val ice_cream = "Ice cream & Snacks"
    const val chaat = "Chaat"
    const val hot_cold = "Hot & Cool Drinks"
    const val bar = "Bar"

    const val prepaid = "Mobile"
    const val dth = "DTH Recharge"
    const val electricity_bill = "Electricity Bill"

    //classified
    const val doctors = "Doctors"
    const val lawyers = "Lawyers"
    const val taxPractitioners = "Tax Practitioners"

    const val buySell = "Buy & Sell"

    const val fule = "fule Index"
    const val gold_silver = "Gold & Silver"
    const val loan = "Loan Rates"
    const val commodity = "Commodity"
    const val forex = "Forex"
    const val stock = "Stock Quotes"

    const val new_car = "New Car"
    const val used_car = "Used Car"
    const val towing_service = "Towing Service"
    const val auto_service = "Auto Service"


    const val english_news = "English News"
    const val tamil_news = "Tamil News"
    const val telungu_news = "Telugu News"
    const val hindi_news = "Hindi News"

//real estate
    const val buy_sell = "Buy & Sell"
    const val engineers = "Engineers"
    const val interior = "Interior"
    const val labour = "Labour"
    const val material = "Material"
    const val packers = "Packers"
    const val builders = "Builders"
    const val rent = "Rent"
    var loggedIn: Boolean = false
    var isFromRecharge: Boolean = false
    public var isFromMobileRecharge: Boolean = false



    const val MESSAGE = "TNonline team is working hard to bring this feature into APP, please check back later.Thank you for being our value visitor/customer"



}