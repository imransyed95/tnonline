package app.com.tnonline.interfaces

import app.com.tnonline.data.remote.models.appoinment.AppoinmentRequest
import app.com.tnonline.data.remote.models.appoinment.AppoinmentResponse
import app.com.tnonline.data.remote.models.appoinment.restaurant.DineInAppoinmentRequest
import app.com.tnonline.data.remote.models.appoinment.restaurant.DineInResponse
import app.com.tnonline.data.remote.models.city.CityListResponse
import app.com.tnonline.data.remote.models.doctor.response.DoctorResponse
import app.com.tnonline.data.remote.models.doctor.request.DoctorRequest
import app.com.tnonline.data.remote.models.events.*
import app.com.tnonline.data.remote.models.flight.FlightResponse
import app.com.tnonline.data.remote.models.forgotpassword.ForgotPasswordRequest
import app.com.tnonline.data.remote.models.forgotpassword.ForgotPasswordResponse
import app.com.tnonline.data.remote.models.lawyer.request.LawyerRequest
import app.com.tnonline.data.remote.models.lawyer.response.LawyerResponse
import app.com.tnonline.data.remote.models.locality.LocalityDataResponse
import app.com.tnonline.data.remote.models.locality.LocalityRequest
import app.com.tnonline.data.remote.models.locality.SpecialistDataResponse
import app.com.tnonline.data.remote.models.locality.SpecialistRequest
import app.com.tnonline.data.remote.models.login.LoginRequest
import app.com.tnonline.data.remote.models.login.LoginResponse
import app.com.tnonline.data.remote.models.news.tabview.Response


import app.com.tnonline.data.remote.models.recharge.createorder.RechargeRequest
import app.com.tnonline.data.remote.models.recharge.createorder.RechargeResponse
import app.com.tnonline.data.remote.models.recharge.orderstatusupdate.OrderStatusRequest
import app.com.tnonline.data.remote.models.recharge.orderstatusupdate.OrderStatusResponse
import app.com.tnonline.data.remote.models.register.RegisterRequest
import app.com.tnonline.data.remote.models.register.RegisterResponse
import app.com.tnonline.data.remote.models.restaurant.RestaurantRequest
import app.com.tnonline.data.remote.models.restaurant.RestaurantResponse
import app.com.tnonline.data.remote.models.train.arrival.arrival.TrainArrivalsResponse
import app.com.tnonline.data.remote.models.train.arrival.trainlist.TrainListResponse
import app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.AutoCompleteResponse
import app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.TrainBetweenStation.TrainBetweenStationResponse
import retrofit2.Call
import retrofit2.http.*


interface APIInterface {

    //GET LIST OF CITY
    @GET("api/city")
    fun getCityList(): Call<CityListResponse>

    @GET("api/flight_info/1,5,6")
    fun getFlightList(): Call<FlightResponse>
    /*@GET("api/order_list")
    fun getTransHistory(): Call<TransactionHistoryResponse>*/

    @POST("api/register")
    fun registerUser(@Body registerRequest: RegisterRequest): Call<RegisterResponse>

    @POST("api/login")
    fun loginUser(@Body loginRequest: LoginRequest): Call<LoginResponse>

    @POST("api/forgotpassword")
    fun forgotPassword(@Body forgotPassword: ForgotPasswordRequest): Call<ForgotPasswordResponse>

    @POST("api/create_order")
    fun createOrder(@Body rechargeRequest: RechargeRequest): Call<RechargeResponse>

    @POST("api/order_status_update")
    fun orderStatusUpdate(@Body rechargeStatusUpdate: OrderStatusRequest): Call<OrderStatusResponse>

    //trainArrival

    //arrivals/station/mv/hours/4/apikey/jtfkr0y3cc
    @GET("arrivals/station/{stationCode}/hours/2/apikey/{trainKey}")
    fun apiTrainArrival(@Path("stationCode") stationCode: String, @Path("trainKey") trainKey: String): Call<TrainArrivalsResponse>
    // fun apiTrainArrival(): Call<TrainArrivalsResponse>

    @GET("suggest-station/name/{searchableText}/apikey/{trainKey}")
    fun autoCompleteStation(@Path("searchableText") searchableText: String, @Path("trainKey") trainKey: String): Call<AutoCompleteResponse>

    // https://api.railwayapi.com/v2/between/source/MAS/dest/VN/date/10-04-2019/apikey/jtfkr0y3cc/
    @GET("between/source/{source}/dest/{Destination}/date/{date}/apikey/{trainKey}")
    fun trainBetweenStation(@Path("source") source: String, @Path("Destination") Destination: String, @Path("date") date: String, @Path("trainKey") trainKey: String): Call<TrainBetweenStationResponse>

    //trainList
    @GET("api/train_info")
    fun getTrainInfoList(): Call<TrainListResponse>

    //locality List
    @GET("api/locality")
    fun getLocalityList(): Call<SpecialistDataResponse>

    @Headers("Authorization: Bearer eyAidXNlcl9pZCIgOiAxNzcsImVtYWlsIjoia2h1YmFpYkBnbWFpbC5jb20iIH0=")
    @GET("http://ec2-13-233-137-92.ap-south-1.compute.amazonaws.com/api/news/category/tamil")
    fun getLocalTamilNewCategoryFetch(): Call<Response>

    @Headers("Authorization: Bearer eyAidXNlcl9pZCIgOiAxNzcsImVtYWlsIjoia2h1YmFpYkBnbWFpbC5jb20iIH0=")
    @GET("http://ec2-13-233-137-92.ap-south-1.compute.amazonaws.com/api/news/category/english")
    fun getLocalEnglishNewCategoryFetch(): Call<Response>

    //http://ec2-13-233-137-92.ap-south-1.compute.amazonaws.com/api/allnews/Tamil
    @Headers("Authorization: Bearer eyAidXNlcl9pZCIgOiAxNzcsImVtYWlsIjoia2h1YmFpYkBnbWFpbC5jb20iIH0=")
    @GET("http://ec2-13-233-137-92.ap-south-1.compute.amazonaws.com/api/rssnews/tamil/{category}")
    fun getLocalTamilNewList(@Path("category") category: String, @Query("page") page: String): Call<app.com.tnonline.data.remote.models.news.Response>


    @Headers("Authorization: Bearer eyAidXNlcl9pZCIgOiAxNzcsImVtYWlsIjoia2h1YmFpYkBnbWFpbC5jb20iIH0=")
    @GET("http://ec2-13-233-137-92.ap-south-1.compute.amazonaws.com/api/rssnews/english/{category}")
    //@GET("http://ec2-13-233-137-92.ap-south-1.compute.amazonaws.com/api/rssnews/english")
    fun getLocalEnglishNewList(@Path("category") category: String, @Query("page") page: String): Call<app.com.tnonline.data.remote.models.news.Response>

    @POST("api/classified/47")
    fun doctorApi(@Body request: DoctorRequest): Call<DoctorResponse>

    @POST("api/classified/47")
    fun doctorApiPagenation(@Query("page") pageNo: Int, @Body request: DoctorRequest): Call<DoctorResponse>


    @POST("api/classified_master/5")
    fun notUsed(@Body request: LawyerRequest): Call<LawyerResponse>

    @POST("api/classified/48")
    fun lawyerApi(@Body request: DoctorRequest): Call<DoctorResponse>

    @POST("api/classified/48")
    fun lawyerApiPagenation(@Query("page") pageNo: Int, @Body request: DoctorRequest): Call<DoctorResponse>


    @POST("api/classified/49")
    fun taxPractitionApi(@Body request: DoctorRequest): Call<DoctorResponse>

    @POST("api/classified/49")
    fun taxPractitionApiPagenation(@Query("page") pageNo: Int, @Body request: DoctorRequest): Call<DoctorResponse>


    @POST("api/appointment")
    fun appoinmentApi(@Body request: AppoinmentRequest): Call<AppoinmentResponse>

    @POST("api/locality")
    fun localityApi(@Body request: LocalityRequest): Call<LocalityDataResponse>

    @POST("api/specialist")
    fun SpeciallistApi(@Body request: SpecialistRequest): Call<SpecialistDataResponse>


    //event

    //trainList

    @GET("api/get_partyhall")
    fun getParyhallList(@Query ("city_id") city_id: String,@Query ("locality_id") locality_id: Int,@Query ("page") page: String): Call<PartyHallResponse>

    @GET("api/get_events")
    fun getLocalEventList(@Query ("city_id") city_id: String,@Query ("locality_id") locality_id: Int): Call<LocalEventResponse>

    @GET("api/get_organizer_events")
    fun getEventOrganizerList(@Query ("city_id") city_id: String,@Query ("locality_id") locality_id: Int): Call<EventOrganizerResponse>

    @GET("api/get_supplies_events")
    fun getEventSuppliesList(@Query ("city_id") city_id: String,@Query ("locality_id ") locality_id: Int): Call<EventSupplyResponse>

    @GET("api/get_restaurants")
    fun getRestaurantList(@Query("city_id") city_id: String,
                          @Query("category_id") category_id: String,
                          @Query("locality_id") locality_id: String,
                          @Query("state_id") state_id: String,
                          @Query("mid_id") mid_id: String,
                          @Query("amenities") amenities: String,
                          @Query("cuisine") cuisine: String): Call<RestaurantResponse>

    @POST("api/submit_enquery")
    fun dineInAppoinmentApi(@Body request: DineInAppoinmentRequest): Call<DineInResponse>


}