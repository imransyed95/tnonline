package app.com.tnonline.ui.infosection.train

import android.os.Bundle
import android.support.v4.app.Fragment
import app.com.tnonline.R
import app.com.tnonline.adapters.TrainAdapter
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.ui.infosection.train.Fragment.Check_PNR
import app.com.tnonline.ui.infosection.train.Fragment.SeatAvailabilityFragment
import app.com.tnonline.ui.infosection.train.Fragment.TrainStatus
import app.com.tnonline.utils.BaseActivity
import kotlinx.android.synthetic.main.activity_train.*

class TrainActivity : BaseActivity() {
    var flightList = ArrayList<String>()
    var flightAdapter: TrainAdapter? = null
    private val appPreferences = SharedPreferences()
    override val layoutResId: Int
        get() = R.layout.activity_train

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        listenres()

    }

    private fun listenres() {
        seat_available.setOnClickListener {
            changeTab(0)
        }
        seat_available.performClick()

        check_pnr.setOnClickListener {
            changeTab(1)
        }

        train_status.setOnClickListener {
            changeTab(2)
        }
    }

    private fun changeTab(i: Int) {
        when {
            i == 0 -> {
                seat_available.setBackgroundColor(resources.getColor(R.color.colorPrimaryColor))
                seat_available.setTextColor(resources.getColor(R.color.white))
                check_pnr.setBackgroundColor(resources.getColor(R.color.white))
                check_pnr.setTextColor(resources.getColor(R.color.textBlackcolor))
                train_status.setBackgroundColor(resources.getColor(R.color.white))
                train_status.setTextColor(resources.getColor(R.color.textBlackcolor))

                fragmentHolder(0)
            }
            i == 1 -> {
                check_pnr.setBackgroundColor(resources.getColor(R.color.colorPrimaryColor))
                check_pnr.setTextColor(resources.getColor(R.color.white))
                seat_available.setBackgroundColor(resources.getColor(R.color.white))
                seat_available.setTextColor(resources.getColor(R.color.textBlackcolor))
                train_status.setBackgroundColor(resources.getColor(R.color.white))
                train_status.setTextColor(resources.getColor(R.color.textBlackcolor))
                fragmentHolder(1)
            }
            i == 2 -> {
                train_status.setBackgroundColor(resources.getColor(R.color.colorPrimaryColor))
                train_status.setTextColor(resources.getColor(R.color.white))
                check_pnr.setBackgroundColor(resources.getColor(R.color.white))
                check_pnr.setTextColor(resources.getColor(R.color.textBlackcolor))
                seat_available.setBackgroundColor(resources.getColor(R.color.white))
                seat_available.setTextColor(resources.getColor(R.color.textBlackcolor))
                fragmentHolder(2)
            }
        }
    }

    private fun fragmentHolder(type: Int) {
        var fragment: Fragment? = null
        fragment = when (type) {
            0 -> {
                SeatAvailabilityFragment()
            }
            1 -> {
                Check_PNR()
            }
            2 -> {
                TrainStatus()
            }
            else -> {
                null
            }
        }
        supportFragmentManager!!.beginTransaction()
                .replace(R.id.frameTrain, fragment!!)
                .commit()
    }

}
