package app.com.tnonline.ui

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import app.com.tnonline.R
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.city.CityListResponse
import app.com.tnonline.data.remote.models.register.RegisterRequest
import app.com.tnonline.data.remote.models.register.RegisterResponse
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import app.com.tnonline.utils.Utilities
import app.com.tnonline.utils.Utilities.isValidEmail
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RegisterActivity : BaseActivity() {

    override val layoutResId: Int
        get() = R.layout.activity_register

    var cityData = ArrayList<String>()
    var cityId = ArrayList<String>()
    var cityImage = ArrayList<String>()
    var mCityID: String? = null

    private val appPreferences = SharedPreferences()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()
        listOfCityAPI()
        btnRegister.setOnClickListener {
            validation()
        }
        tvlogin.setOnClickListener {
            finish()
        }
        initAdapter()
    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Registration"
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initAdapter() {
        spinnerSelectCity.adapter = ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item, cityData)

        spinnerSelectCity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.getItemAtPosition(position)
                Log.d("mmmmm", "$item")
                mCityID = cityId[position]
                val cityImage = cityImage[position]

                appPreferences.setString(Constants.CITY_NAME, item.toString())
                appPreferences.setString(Constants.CITY_ID, mCityID.toString())
                appPreferences.setString(Constants.CITY_IMAGE, cityImage)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    private fun listOfCityAPI() {
        showProgressBar(llParent)
        val call = APIFactory.getService().getCityList()
        call.enqueue(object : Callback<CityListResponse> {
            override fun onResponse(call: Call<CityListResponse>, response: Response<CityListResponse>) {
                if (response.body()!!.status.equals(Constants.SUCCESS)) {
                    dismissProgressBar()

                    if (response.body()!!.data != null) {
                        for (i in 0 until response.body()!!.data!!.size) {
                            cityData.add(response.body()!!.data!![i].city_name!!)
                            cityId.add(response.body()!!.data!![i].city_id.toString())
                            cityImage.add(response.body()!!.data!![i].city_image.toString())
                        }

                        initAdapter()
                    }

                    displayToast(response.body()!!.msg!!)
                } else if (response.body()!!.status.equals(Constants.ERROR)) {
                    displayToast(response.body()!!.msg!!)
                }
            }

            override fun onFailure(call: Call<CityListResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }

    private fun validation() {
        val password: String = ePasswordRegister.text.toString().trim()
        val confirmPassword: String = edConfirm_Password.text.toString().trim()

        Log.e("password", password)
        Log.e("confirmPassword", confirmPassword)

        if (edNameRegister.text.toString().trim() == ("")) {
            displayToast("Please Enter the name")
        } else if (edMobileNumber.text.toString().trim() == ("")) {
            displayToast("Please Enter the Mobile Number")
        } else if (edEmailRegister.text.toString().trim() == ("")) {
            displayToast("Please Enter your Email")
        } else if (!isValidEmail(edEmailRegister.text.toString().trim())) {
            displayToast("Please Enter your valid Email")
        } else if (password == ("")) {
            displayToast("Please Enter the your Password")
        } else if (!Utilities.isValidPassword(password)) {
            displayToast("Password must be Minimum 8 letters in alpha numeric")
        } else if (confirmPassword == ("")) {
            displayToast("Please Enter your Confirm password")
        } else if (password != confirmPassword) {
            displayToast("Password must be Minimum 8 letters in alpha numeric and special characters")
        } else {
            registerAPI()
        }
    }


    private fun registerAPI() {
        val registerRequest = RegisterRequest()
        registerRequest.full_name = edNameRegister.text.toString().trim()
        registerRequest.city_id = mCityID
        registerRequest.contact_no = edMobileNumber.text.toString().trim()
        registerRequest.email = edEmailRegister.text.toString().trim()
        registerRequest.password = edConfirm_Password.text.toString().trim()

        showProgressBar(llParent)

        val call = APIFactory.getService().registerUser(registerRequest)
        call.enqueue(object : Callback<RegisterResponse> {
            override fun onFailure(call: Call<RegisterResponse>?, t: Throwable?) {
                displayToast(t.toString())
            }

            override fun onResponse(call: Call<RegisterResponse>?, response: Response<RegisterResponse>?) {
                if (response!!.body()!!.status.equals("success")) {
                    displayToast(response.body()!!.message!!)
                    finish()
                } else {
                    displayToast(response.body()!!.message!!)
                }
            }
        })
    }
}