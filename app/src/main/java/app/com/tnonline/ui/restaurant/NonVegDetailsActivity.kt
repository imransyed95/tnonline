package app.com.tnonline.ui.restaurant

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.LinearLayout
import app.com.tnonline.R
import app.com.tnonline.adapters.RestaurantFeautreAdapter
import app.com.tnonline.data.remote.models.events.FeatureModel
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Singleton
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_non_veg_details.*
import kotlinx.android.synthetic.main.activity_non_veg_details.header_image
import kotlinx.android.synthetic.main.activity_non_veg_details.txtAbout


class NonVegDetailsActivity : BaseActivity(), View.OnClickListener {

    var cuisines = ArrayList<FeatureModel>()
    var amenities = ArrayList<FeatureModel>()
    var dineTypes = ArrayList<FeatureModel>()
    var adapter: RestaurantFeautreAdapter? = null
    override val layoutResId: Int
        get() = R.layout.activity_non_veg_details

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val toolbar = findViewById<Toolbar>(app.com.tnonline.R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        Glide.with(this)
                .load(Singleton.headerImage)
                .into(header_image)
        txtHospitalName.text = Singleton.nameOfHall
        haspitalAddress.text = Singleton.address
        mobileNo_tv.text = "+91" + Singleton.contact
        email_tv.text = Singleton.email
        txtAbout.text = Singleton.description
        for (i in 0 until Singleton.cuisines.size) {
            val model = FeatureModel()
            model.name = Singleton.cuisines[i]
            model.image = ""
            cuisines.add(model)
        }

        for (i in 0 until Singleton.amenities.size) {
            val model = FeatureModel()
            model.name = Singleton.amenities[i]
            model.image = ""
            amenities.add(model)
        }

        for (i in 0 until Singleton.dineTypes.size) {
            val model = FeatureModel()
            model.name = Singleton.dineTypes[i]
            model.image = ""
            dineTypes.add(model)
        }
        setCuisnesAdpater()
        setAmenitiesAdpater()
        setDineInAdpater()
        location_iv.setOnClickListener(this)
        mobileNo_tv.setOnClickListener(this)
        email_tv.setOnClickListener(this)
    }

    private fun setDineInAdpater() {
        adapter = RestaurantFeautreAdapter(this@NonVegDetailsActivity, dineTypes as ArrayList<FeatureModel>) { view: View, dataItems: FeatureModel, i: Int ->
            //startActivity(Intent(this, RealEstateDetailsActivity::class.java))
            when (view.id) {
                R.id.cardView -> {

                }

            }
        }
        dineType_rv.layoutManager = LinearLayoutManager(this@NonVegDetailsActivity, LinearLayout.HORIZONTAL, false) as RecyclerView.LayoutManager?
        dineType_rv.adapter = adapter
        dineType_rv.adapter!!.notifyDataSetChanged()
    }

    private fun setAmenitiesAdpater() {
        adapter = RestaurantFeautreAdapter(this@NonVegDetailsActivity, amenities as ArrayList<FeatureModel>) { view: View, dataItems: FeatureModel, i: Int ->
            //startActivity(Intent(this, RealEstateDetailsActivity::class.java))
            when (view.id) {
                R.id.cardView -> {

                }

            }
        }
        amenities_rv.layoutManager = LinearLayoutManager(this@NonVegDetailsActivity, LinearLayout.HORIZONTAL, false) as RecyclerView.LayoutManager?
        amenities_rv.adapter = adapter
        amenities_rv.adapter!!.notifyDataSetChanged()
    }

    private fun setCuisnesAdpater() {
        adapter = RestaurantFeautreAdapter(this@NonVegDetailsActivity, cuisines as ArrayList<FeatureModel>) { view: View, dataItems: FeatureModel, i: Int ->
            //startActivity(Intent(this, RealEstateDetailsActivity::class.java))
            when (view.id) {
                R.id.cardView -> {

                }

            }
        }
        cuisine_rv.layoutManager = LinearLayoutManager(this@NonVegDetailsActivity, LinearLayout.HORIZONTAL, false) as RecyclerView.LayoutManager?
        cuisine_rv.adapter = adapter
        cuisine_rv.adapter!!.notifyDataSetChanged()
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.location_iv -> {
                Singleton.address?.let { loadNavigationView(it) }
            }
            R.id.mobileNo_tv -> {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:"+mobileNo_tv.text)
                startActivity(intent)
            }
            R.id.email_tv->{
                val intent = Intent(Intent.ACTION_SENDTO) // it's not ACTION_SEND
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email")
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email")
                intent.data = Uri.parse("mailto:"+email_tv.text) // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent)
            }
        }
    }


    fun loadNavigationView(address: String) {
        val mapUri = Uri.parse("geo:0,0?q=" + Uri.encode(address))
        val mapIntent = Intent(Intent.ACTION_VIEW, mapUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }
}
