package app.com.tnonline.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.adapters.DashboardAdapter
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.remote.models.dashboard.DashBoardModel
import app.com.tnonline.ui.classified.DoctorListActivity
import app.com.tnonline.ui.classified.LawyersListActivity
import app.com.tnonline.ui.classified.TaxPractitionerActivity
import app.com.tnonline.ui.events.PartyHallActivity
import app.com.tnonline.ui.infosection.flight.FlightActivity
import app.com.tnonline.ui.infosection.flight.FlightActivityNew
import app.com.tnonline.ui.infosection.flight.FlightDetailsShowActivity
import app.com.tnonline.ui.infosection.train.TrainActivity
import app.com.tnonline.ui.news.NewsActivity
import app.com.tnonline.ui.realEsate.RealEstateActivity
import app.com.tnonline.ui.restaurant.NonVegRestaurantActivity
import app.com.tnonline.ui.selfservice.MobileRechargeActivity
import app.com.tnonline.ui.selfservice.TransactionHistoryActivity
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*


class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    var dashBoardModel = ArrayList<DashBoardModel>()
    var dashboardAdapter: DashboardAdapter? = null
    override val layoutResId: Int
        get() = R.layout.activity_main

    private var doubleBackToExitPressedOnce = false
    private val appPreferences = SharedPreferences()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
        title = "Dashboard"
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "HomeActivity")
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "HomeActivity")
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)

        nav_view.setNavigationItemSelectedListener(this)
        /* infoSectionLayout.setOnClickListener(this)
         selfServiceLayout.setOnClickListener(this)
         newsSectionLayout.setOnClickListener(this)
         priceindexLayout.setOnClickListener(this)
         autoMobileLayout.setOnClickListener(this)
 */
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView

        // get menu from navigationView
        val menu = navigationView.menu

        // find MenuItem you want to change
        val nav_logout = menu.findItem(R.id.nav_logout)

        // set new title to the MenuItem

        funImageSet()
        funEventsValueSet()
        funTravelValueSet()
        funRestaurantsValueSet()
        funBillAndRechargeValueSet()
        funClassifiedValueSet()
        funPriceIndexValueSet()
        funAutomobileValueSet()
        funNewsValueSet()
        funRealEstateValueSet()
        val loginValue = appPreferences.getString(Constants.LOGIN_SUCCESS)
        val userName = appPreferences.getString(Constants.LOGIN_NAME)
        val mailId = appPreferences.getString(Constants.LOGIN_EMAIL)
        if (loginValue == Constants.YES) {
            nav_logout.title = "Logout"
        } else {
            nav_logout.title = "Login"
        }

        val header = navigationView.getHeaderView(0)
/*View view=navigationView.inflateHeaderView(R.layout.nav_header_main);*/
        val name = header.findViewById(R.id.userName) as TextView
        val email = (header.findViewById(R.id.mailId) as TextView)

        name.text = userName
        email.text = mailId

    }

    private fun funEventsValueSet() {
        dashBoardModel = ArrayList()
        dashBoardModel.add(DashBoardModel(Constants.LocalEvent, R.drawable.event_local,true))
        dashBoardModel.add(DashBoardModel(Constants.PARTY_HALL, R.drawable.event_party_hall,true))
        dashBoardModel.add(DashBoardModel(Constants.EVENT_ORGE, R.drawable.event_org,true))
        dashBoardModel.add(DashBoardModel(Constants.EVENT_SUPP, R.drawable.event_supplier,true))
        recyViewSet(recyEventss, dashBoardModel)

    }

    private fun funTravelValueSet() {
        dashBoardModel = ArrayList()
        dashBoardModel.add(DashBoardModel(Constants.FLIGHT, R.drawable.travel_flight,true))
        dashBoardModel.add(DashBoardModel(Constants.TRAIN, R.drawable.travel_train))
        dashBoardModel.add(DashBoardModel(Constants.Bus, R.drawable.travel_bus))
        dashBoardModel.add(DashBoardModel(Constants.Emegency_service, R.drawable.travel_emergency))
        recyViewSet(recyTravel, dashBoardModel)

    }

    private fun funRestaurantsValueSet() {
        dashBoardModel = ArrayList()
        dashBoardModel.add(DashBoardModel(Constants.vegetarian, R.drawable.restaurant_veg,true))
        dashBoardModel.add(DashBoardModel(Constants.non_veg, R.drawable.restaurant_chicken,true))
        dashBoardModel.add(DashBoardModel(Constants.pizza, R.drawable.restaurant_pizza,true))
        dashBoardModel.add(DashBoardModel(Constants.bakery, R.drawable.restaurant_bakery,true))
        dashBoardModel.add(DashBoardModel(Constants.ice_cream, R.drawable.restaurant_ice_cream,true))
        dashBoardModel.add(DashBoardModel(Constants.chaat, R.drawable.restaurant_chaat,true))
        dashBoardModel.add(DashBoardModel(Constants.hot_cold, R.drawable.restaurant_hot_cold,true))
        dashBoardModel.add(DashBoardModel(Constants.bar, R.drawable.restaurant_bar,true))
        recyViewSet(recyRestaurants, dashBoardModel)

    }

    private fun funBillAndRechargeValueSet() {
        dashBoardModel = ArrayList()
        dashBoardModel.add(DashBoardModel(Constants.prepaid, R.drawable.recharge_prepaid,true))
        dashBoardModel.add(DashBoardModel(Constants.dth, R.drawable.recharge_dth))
        dashBoardModel.add(DashBoardModel(Constants.electricity_bill, R.drawable.recharge_electricity_bill))
        recyViewSet(recyRecharge_bill, dashBoardModel)

    }

    private fun funClassifiedValueSet() {
        dashBoardModel = ArrayList()
        dashBoardModel.add(DashBoardModel(Constants.doctors, R.drawable.clfi_doctor,true))
        dashBoardModel.add(DashBoardModel(Constants.lawyers, R.drawable.clfi_lawyers,true))
        dashBoardModel.add(DashBoardModel(Constants.taxPractitioners, R.drawable.clfi_tax,true))
        recyViewSet(recyClassified, dashBoardModel)

    }

    private fun funPriceIndexValueSet() {
        dashBoardModel = ArrayList()
        dashBoardModel.add(DashBoardModel(Constants.fule, R.drawable.price_fuel))
        dashBoardModel.add(DashBoardModel(Constants.gold_silver, R.drawable.price_gold))
        dashBoardModel.add(DashBoardModel(Constants.loan, R.drawable.price_loan))
        dashBoardModel.add(DashBoardModel(Constants.commodity, R.drawable.price_commodity))
        dashBoardModel.add(DashBoardModel(Constants.forex, R.drawable.price__forex))
        dashBoardModel.add(DashBoardModel(Constants.stock, R.drawable.price_stock))
        recyViewSet(recyPriceIndex, dashBoardModel)
    }

    private fun funAutomobileValueSet() {
        dashBoardModel = ArrayList()
        dashBoardModel.add(DashBoardModel(Constants.new_car, R.drawable.aut_new_car))
        dashBoardModel.add(DashBoardModel(Constants.used_car, R.drawable.aut_used_car))
        dashBoardModel.add(DashBoardModel(Constants.towing_service, R.drawable.aut_new_car))
        dashBoardModel.add(DashBoardModel(Constants.auto_service, R.drawable.aut_used_car))
        recyViewSet(recyAutoMobile, dashBoardModel)

    }

    private fun funNewsValueSet() {
        dashBoardModel = ArrayList()
        dashBoardModel.add(DashBoardModel(Constants.english_news, R.drawable.news_eng, true))
        dashBoardModel.add(DashBoardModel(Constants.tamil_news, R.drawable.news_tamil, true))
        dashBoardModel.add(DashBoardModel(Constants.hindi_news, R.drawable.news_hindi))
        dashBoardModel.add(DashBoardModel(Constants.telungu_news, R.drawable.news_telugu))
        recyViewSet(recyNews, dashBoardModel)

    }

    private fun funRealEstateValueSet() {
        dashBoardModel = ArrayList()
        dashBoardModel.add(DashBoardModel(Constants.buy_sell, R.drawable.real_buy_sell,true))
        dashBoardModel.add(DashBoardModel(Constants.rent, R.drawable.rea_rent))
        dashBoardModel.add(DashBoardModel(Constants.builders, R.drawable.real_builders))
        dashBoardModel.add(DashBoardModel(Constants.material, R.drawable.real_material))
        dashBoardModel.add(DashBoardModel(Constants.engineers, R.drawable.real_engineers))
        dashBoardModel.add(DashBoardModel(Constants.labour, R.drawable.real_labour))
        dashBoardModel.add(DashBoardModel(Constants.interior, R.drawable.real_interior))
        dashBoardModel.add(DashBoardModel(Constants.packers, R.drawable.real_packers))
        recyViewSet(recyRealestate, dashBoardModel)

    }

    fun recyViewSet(recyclerView: RecyclerView, dashBoardModel: ArrayList<DashBoardModel>) {
        dashboardAdapter = DashboardAdapter(this@MainActivity, dashBoardModel) { view: View, dataItems: DashBoardModel, i: Int ->
            funcClickEvent(dataItems.name)
           // displayToast(dataItems.name.toString())

            //startActivity(Intent(this@MainActivity, RealEstateActivity::class.java))


        }

        recyclerView.layoutManager = GridLayoutManager(this@MainActivity, 4, GridLayoutManager.VERTICAL, false) as RecyclerView.LayoutManager?
        recyclerView.adapter = dashboardAdapter
        recyclerView.adapter!!.notifyDataSetChanged()
        recyclerView.isNestedScrollingEnabled=false
    }

    private fun funcClickEvent(name: String?) {
        when (name) {

            //selfService
            Constants.prepaid -> {
                startActivity(Intent(this@MainActivity, MobileRechargeActivity::class.java))

            }
            //Travels
            Constants.FLIGHT -> {
                if (!appPreferences.getString(Constants.FLIGHT_AREA).isEmpty()) {
                    startActivity((Intent(this@MainActivity, FlightActivityNew::class.java)))
                } else {
                    startActivity((Intent(this@MainActivity, FlightActivity::class.java)))
                }

            }
            Constants.TRAIN -> {
                startActivity((Intent(this@MainActivity, TrainActivity::class.java)))

            }

            Constants.PARTY_HALL -> {
                startActivity((Intent(this@MainActivity, PartyHallActivity::class.java).putExtra("section", Constants.PARTY_HALL)))

            }

            Constants.LocalEvent -> {
                startActivity((Intent(this@MainActivity, PartyHallActivity::class.java).putExtra("section", Constants.LocalEvent)))

            }
            Constants.EVENT_ORGE -> {
                startActivity((Intent(this@MainActivity, PartyHallActivity::class.java).putExtra("section", Constants.EVENT_ORGE)))

            }

            Constants.EVENT_SUPP -> {
                startActivity((Intent(this@MainActivity, PartyHallActivity::class.java).putExtra("section", Constants.EVENT_SUPP)))

            }

            Constants.non_veg -> {
                startActivity((Intent(this@MainActivity, NonVegRestaurantActivity::class.java).putExtra("section", Constants.non_veg)))
            }


            Constants.vegetarian -> {
                startActivity((Intent(this@MainActivity, NonVegRestaurantActivity::class.java).putExtra("section", Constants.vegetarian)))

            }

            Constants.pizza -> {
                startActivity((Intent(this@MainActivity, NonVegRestaurantActivity::class.java).putExtra("section", Constants.pizza)))

            }

            Constants.bakery -> {
                startActivity((Intent(this@MainActivity, NonVegRestaurantActivity::class.java).putExtra("section", Constants.bakery)))

            }

            Constants.bar -> {
                startActivity((Intent(this@MainActivity, NonVegRestaurantActivity::class.java).putExtra("section", Constants.bar)))

            }


            Constants.ice_cream -> {
                startActivity((Intent(this@MainActivity, NonVegRestaurantActivity::class.java).putExtra("section", Constants.ice_cream)))

            }

            Constants.chaat -> {
                startActivity((Intent(this@MainActivity, NonVegRestaurantActivity::class.java).putExtra("section", Constants.chaat)))

            }

            Constants.hot_cold -> {
                startActivity((Intent(this@MainActivity, NonVegRestaurantActivity::class.java).putExtra("section", Constants.hot_cold)))

            }


            Constants.doctors -> {
                startActivity((Intent(this@MainActivity, DoctorListActivity::class.java)))

            }

            Constants.lawyers -> {
                startActivity((Intent(this@MainActivity, LawyersListActivity::class.java)))

            }
            Constants.taxPractitioners -> {
                startActivity((Intent(this@MainActivity, TaxPractitionerActivity::class.java)))

            }

            Constants.buySell -> {
                startActivity((Intent(this@MainActivity, RealEstateActivity::class.java)))

            }

            // News
            Constants.english_news -> {
                var intent : Intent = Intent(this, NewsActivity::class.java)
                intent.putExtra("lang", "English")
                startActivity(intent)
            }
            Constants.tamil_news -> {
                var intent : Intent = Intent(this, NewsActivity::class.java)
                intent.putExtra("lang", "Tamil")
                startActivity(intent)
            }
            else -> {
                displayToast(Constants.MESSAGE)
            }
        }
    }

    private fun funImageSet() {

        val cityImage: String = appPreferences.getString(Constants.CITY_IMAGE)
        val cityName: String = appPreferences.getString(Constants.CITY_NAME)

        toolbar_title.text = cityName

        toolbar_title.setOnClickListener {
            startActivity(Intent(this@MainActivity, SkipLoginActivity::class.java))
        }

/*
        Glide.with(this).load(cityImage).into(object : SimpleTarget<Drawable>() {
            override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageLayout.background = resource
                }
            }
        })*/
    }

    override fun onResume() {
        super.onResume()
        funImageSet()
    }


    override fun onClick(views: View?) {

        when (views?.id) {
            /*   R.id.eventsLayout -> {

               }

               R.id.classifiedsLayout -> {

               }


               R.id.realStateLayout -> {

               }


               R.id.restaurantLayout -> {

               }

               R.id.infoSectionLayout -> {
                   startActivity(Intent(this@MainActivity, InfoSectionActivity::class.java))
               }

               R.id.selfServiceLayout -> {
                   startActivity(Intent(this@MainActivity, SelfServiceActivity::class.java))
               }
               R.id.newsSectionLayout -> {
                   displayToastLong(Constants.MESSAGE)
               }
               R.id.priceindexLayout -> {
                   displayToastLong(Constants.MESSAGE)
               }
               R.id.autoMobileLayout -> {
                   displayToastLong(Constants.MESSAGE)
               }*/
        }

    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        displayToast("Please click BACK again to exit")

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {

            R.id.nav_recherge -> {
                startActivity(Intent(this, MobileRechargeActivity::class.java))
            }
            R.id.nav_transaction_history -> {
                startActivity(Intent(this, TransactionHistoryActivity::class.java))
            }
            R.id.nav_settings -> {
                // Handle the camera action
                startActivity(Intent(this, SettingsActivity::class.java))
            }
            R.id.nav_posting -> {

            }
            R.id.nav_advertisement -> {

            }
            R.id.nav_franchise_with_us -> {

            }
            R.id.nav_faq -> {

            }
            R.id.nav_contact_us -> {

            }
            R.id.nav_add_post -> {

            }
            R.id.nav_other_states -> {

            }
            R.id.nav_logout -> {
                appPreferences.setString(Constants.LOGIN_SUCCESS, Constants.NO)
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun calculateNoOfColumns(context: Context?, value: Int): Int {
        var noOfColumns = 1
        try {
            val displayMetrics = context!!.resources.displayMetrics
            val dpWidth = displayMetrics.widthPixels / displayMetrics.density
            noOfColumns = (dpWidth / value).toInt()
            return noOfColumns
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return noOfColumns
    }
}
