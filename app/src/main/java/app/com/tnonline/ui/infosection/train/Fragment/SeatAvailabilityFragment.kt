package app.com.tnonline.ui.infosection.train.Fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.com.tnonline.R
import app.com.tnonline.ui.infosection.train.TrainDetailedActivity
import kotlinx.android.synthetic.main.fragment_seat_available.view.*
import android.widget.ArrayAdapter
import android.widget.Toast
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.AutoCompleteResponse
import app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.DataItem
import app.com.tnonline.utils.Constants
import kotlinx.android.synthetic.main.fragment_seat_available.*
import kotlinx.android.synthetic.main.new_buy_ticket_dialog.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception


class SeatAvailabilityFragment : Fragment() {

    var list:ArrayList<DataItem>? = ArrayList()
    var stringList: ArrayList<String>? = ArrayList()

    var source_code: String? = String()
    var destination_code: String? = String()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_seat_available, container, false)

        view.btn_search_train?.setOnClickListener {
          //  startActivity(Intent(activity, TrainDetailedActivity::class.java))

            if(!TextUtils.isEmpty(source_code) && !TextUtils.isEmpty(destination_code)){
                val intent: Intent = Intent(activity, TrainDetailedActivity::class.java)
                intent.putExtra("sou_code", source_code)
                intent.putExtra("des_code", destination_code)
                startActivity(intent)
            }else{
                displayToast("Please Select the Valid Source and Destination")
                if(view.source_value.text.length == 0){
                    view.source_value.setError("Please Enter Your Source")
                }
                if(view.destination_value.text.length == 0){
                    view.destination_value.setError("Please Enter Your Destination")
                }
            }
        }

        view.source_value.addTextChangedListener(object :  TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                if(view.source_value.text.length > 2){
                    SourceDataCall(view.source_value.text.toString())
                }
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        view.source_value.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(context, "Source :" + list!![position].code.toString(), Toast.LENGTH_SHORT).show()
            source_code = list!![position].code.toString()

        }

        view.destination_value.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(context, "Destination :" + list!![position].code.toString(), Toast.LENGTH_SHORT).show()
            destination_code = list!![position].code.toString()
        }

        view.destination_value.addTextChangedListener(object :  TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                if(view.destination_value.text.length > 2){
                    DestinationDataCall(view.destination_value.text.toString())
                }
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


        return view
    }

    private fun DestinationDataCall(Destination: String) {
        callAutoCompleteAPI(1, Destination, this.context)
//        val adapter = ArrayAdapter<String>(this, android.R.layout.select_dialog_item, arr)
//
//        view!!.destination_value.setThreshold(2)
//        view!!.destination_value.setAdapter(adapter)
    }

    private fun SourceDataCall(Source: String) {
        callAutoCompleteAPI(0, Source, this.context)
    }

    private fun callAutoCompleteAPI(gate: Int, searchableText: String, context: Context?) {
        val call = APIFactory.getTrainService().autoCompleteStation(searchableText, Constants.TRAIN_KEY)

        call.enqueue(object : Callback<AutoCompleteResponse> {

            override fun onResponse(call: Call<AutoCompleteResponse>, response: Response<AutoCompleteResponse>) {
                if (response.body()!!.responseCode!! == 200) {

                    if (response.body()!!.stations != null) {

                        if(response.body()!!.stations?.size!! > 0) {
                            try {
                                list = ArrayList()
                                stringList = ArrayList()

                                for (num in 0..response.body()!!.stations?.size!! - 1) {
                                    stringList!!.add(response.body()!!.stations!!.get(num)!!.name.toString())
                                    list!!.add(response.body()!!.stations?.get(num)!!)
                                }

                                when (gate) {
                                    0 -> {
                                        val adapter = ArrayAdapter<String>(context, android.R.layout.select_dialog_item, stringList)
                                        view!!.source_value.setThreshold(2)
                                        view!!.source_value.setAdapter(adapter)
                                    }
                                    1 -> {
                                        val adapter = ArrayAdapter<String>(context, android.R.layout.select_dialog_item, stringList)
                                        view!!.destination_value.setThreshold(2)
                                        view!!.destination_value.setAdapter(adapter)
                                    }

                                }
                            }catch (e: Exception){
                                Log.i("TTT", e.toString())
                            }
                        }
                    }
                 //      displayToast(response.body()!!.debit!!.toString())
                } else {
                 //   displayToast("Error")
                }
            }

            override fun onFailure(call: Call<AutoCompleteResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }

    private fun displayToast(s: String) {
        Toast.makeText(this.context, s.toString(), Toast.LENGTH_SHORT).show()
    }


}

