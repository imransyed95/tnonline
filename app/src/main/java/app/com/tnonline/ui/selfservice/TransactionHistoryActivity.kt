package app.com.tnonline.ui.selfservice

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import app.com.tnonline.R
import app.com.tnonline.adapters.TransactionHistoryAdapter
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import kotlinx.android.synthetic.main.activity_transaction_history.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class TransactionHistoryActivity : BaseActivity() {
    //var transHistList = ArrayList<DataItem>()
    var context: Context? = null
    var transactionHistoryAdapter: TransactionHistoryAdapter? = null
    private val appPreferences = SharedPreferences()
    override val layoutResId: Int
        get() = R.layout.activity_transaction_history

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this
        setupActionBar()
        if (internetCheck(context!!)) {
            val tokenString = appPreferences.getString(Constants.LOGIN_AUTH)
            if (!tokenString.isNullOrEmpty()) {
                listOfFlightAPI()
            } else {
                displayToast("Please Login to proceed with Recharge")
            }
        } else {
            displayToast(Constants.INTERNETCONNECTION)
        }

    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Transaction History"
    }

    private fun listOfFlightAPI() {
       /* showProgressBar(llParent)

        val call = APIFactory.getClientWithIntercepter()!!.getTransHistory()
        call.enqueue(object : Callback<TransactionHistoryResponse> {
            override fun onResponse(call: Call<TransactionHistoryResponse>, response: Response<TransactionHistoryResponse>) {
                if (response.body()!!.status.equals(Constants.SUCCESS)) {
                    dismissProgressBar()

                    if (response.body()!!.data != null) {
                        transHistList.addAll(response.body()!!.data!!.filterNotNull())
                        *//*for (i in 0 until response.body()!!.data!!.size) {
                            flightList = response.body()!!.data!!

                        }*//*
                        setupAdapter()
                    }

                    displayToast(response.body()!!.message!!)
                } else if (response.body()!!.status.equals(Constants.ERROR)) {
                    displayToast(response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<TransactionHistoryResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })*/
    }

   /* private fun getErrorBody(response: Response<TransactionHistoryResponse>) {
        if (response.errorBody() != null) {
            try {
                val jsonObject = JSONObject(response.errorBody()!!.string())
                val message = jsonObject.getString("message")
                displayToast(message)
                //   Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()

            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } else {
            displayToast(response.body()!!.message!!)
        }
    }*/


   /* private fun setupAdapter() {
        transactionHistoryAdapter = TransactionHistoryAdapter(this@TransactionHistoryActivity, transHistList)
        { view: View, dataItems: app.com.tnonline.data.remote.models.recharge.transactionhistory.DataItem, i: Int ->
            *//*val flightArea: String = dataItems.iframeUrl.toString()
            val title: String = dataItems.title.toString()
            appPreferences.setString(Constants.FLIGHT_AREA_URL, flightArea)
            appPreferences.setString(Constants.FLIGHT_AREA, title)
            startActivity(Intent(this@FlightActivity, FlightDetailsShowActivity::class.java))
*//*
        }
        recyclerViewFlight.layoutManager = LinearLayoutManager(this@TransactionHistoryActivity, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        recyclerViewFlight.adapter = transactionHistoryAdapter
        recyclerViewFlight.adapter!!.notifyDataSetChanged()
    }*/

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
