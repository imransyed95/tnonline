package app.com.tnonline.ui.restaurant

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.Window
import android.widget.*
import app.com.tnonline.R
import app.com.tnonline.adapters.NonVegAdapter
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.locality.LocalityDataResponse
import app.com.tnonline.data.remote.models.locality.LocalityRequest
import app.com.tnonline.data.remote.models.restaurant.RestaurantRequest
import app.com.tnonline.data.remote.models.restaurant.RestaurantResponse
import app.com.tnonline.data.test.NonVegModel
import app.com.tnonline.ui.restaurant.appionment.DineInAppoinmentActivity
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import app.com.tnonline.utils.Singleton
import kotlinx.android.synthetic.main.activity_non_veg_restaurant.*
import kotlinx.android.synthetic.main.filter_popup_restaurant.*
import kotlinx.android.synthetic.main.real_state_layout.nav_icon_img
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NonVegRestaurantActivity : BaseActivity(),View.OnClickListener  {


    var nonVegItems = ArrayList<NonVegModel>()
    var adapter: NonVegAdapter? = null
    private val appPreferences = SharedPreferences()

    var filter_doctor_dialog: Dialog? = null
    var update_or_refresh_list : TextView? = null
    var close_doctor_dialog : ImageView? = null
    var localityButton : TextView? = null
    var specilityButton : TextView? = null
    var dialogFlag : Int = 0
    var locality_layout : LinearLayout? = null
    var specialist_layout : LinearLayout? = null
    var locality_list : ListView? = null
    var specialist_list : ListView? = null
    var localityID_flag: Int = 0
    var catId: Int = 0
    private var localityList_name = java.util.ArrayList<String>()
    private var localityList_id = java.util.ArrayList<Int>()
    var localityStr:String?=null
    var cityIdStr:String?=null
    var stateIdStr:String?=null


    override val layoutResId: Int
        get() = R.layout.activity_non_veg_restaurant

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var title:String =intent.extras["section"].toString()
        toolbartitle.text=title
        stateIdStr =  appPreferences.getString(Constants.STATE_ID)
        cityIdStr =  appPreferences.getString(Constants.CITY_ID)
        localityStr =  appPreferences.getString(Constants.CITY_ID)
        filter_button.setOnClickListener(this)

        createFilterDialog()
        callLocalityApi()
        nav_icon_img.setOnClickListener(this)
        when {
            title.equals(Constants.vegetarian) -> {
                nonVegItems.clear()
                catId=38
                callApi(38)
            }
            title.equals(Constants.non_veg) -> {
                nonVegItems.clear()
                catId=39
                callApi(39)
            }
            title.equals(Constants.pizza) -> {
                nonVegItems.clear()
                catId=40
                callApi(40)
            }
            title.equals(Constants.bakery) -> {
                nonVegItems.clear()
                catId=41
                callApi(41)
            }
            title.equals(Constants.ice_cream) -> {
                nonVegItems.clear()
                catId=42
                callApi(42)
            }
            title.equals(Constants.chaat) -> {
                nonVegItems.clear()
                catId=43
                callApi(43)
            }
            title.equals(Constants.hot_cold) -> {
                nonVegItems.clear()
                catId=44
                callApi(44)
            }
            title.equals(Constants.bar) -> {
                nonVegItems.clear()
                callApi(45)
            }
        }

    }

    private fun createFilterDialog() {
        filter_doctor_dialog = Dialog(this);
        filter_doctor_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE);
        filter_doctor_dialog!!.setCancelable(false);
        filter_doctor_dialog!!.setContentView(R.layout.filter_popup_restaurant);

        update_or_refresh_list = filter_doctor_dialog!!.findViewById(R.id.update_or_refresh_list) as TextView
        close_doctor_dialog = filter_doctor_dialog!!.findViewById(R.id.close_doctor_dialog) as ImageView
        localityButton = filter_doctor_dialog!!.findViewById(R.id.locality_button) as TextView
        locality_layout = filter_doctor_dialog!!.findViewById(R.id.locality_layout) as LinearLayout
        locality_list = filter_doctor_dialog!!.findViewById(R.id.locality_list) as ListView

        localityButton!!.setBackgroundColor(resources.getColor(R.color.green))
        localityButton!!.setTextColor(resources.getColor(R.color.white))


        localityButton!!.setOnClickListener {
            localityButton!!.setBackgroundColor(resources.getColor(R.color.green))
            localityButton!!.setTextColor(resources.getColor(R.color.white))

            specilityButton!!.setBackgroundColor(resources.getColor(R.color.youtube_light_gray))
            specilityButton!!.setTextColor(resources.getColor(R.color.textBlackcolor))
            specialist_layout!!.visibility = View.GONE
            locality_layout!!.visibility = View.VISIBLE
            dialogFlag = 0

        }


        update_or_refresh_list!!.setOnClickListener {
            //currentPage = 1;
           // display!!.text = " 01 "
            updateList()
        }

        close_doctor_dialog!!.setOnClickListener {
            filter_doctor_dialog!!.dismiss()
        }

        locality_list!!.setOnItemClickListener { parent, view, position, id ->
            if(localityList_name!!.size > 0){
                Toast.makeText(mContext, localityList_name[position].toString(), Toast.LENGTH_SHORT).show()
                localityID_flag = localityList_id[position]
            }
        }


    }

    private fun updateList() {
        close_doctor_dialog!!.performClick()
        callApiByLocationId(localityID_flag)
    }

    private fun callApiByLocationId(localityidFlag: Int) {
        val request=RestaurantRequest()
        request.cityId=appPreferences.getString(Constants.CITY_ID);
        request.amenities=""
        request.categoryId=catId.toString()
        request.stateId=stateIdStr
        request.midId="13"
        request.cuisine=""
        request.localityId=localityidFlag.toString()

        val call = APIFactory.getService().getRestaurantList(appPreferences.getString(Constants.CITY_ID),catId.toString(),localityidFlag.toString(),"","13","","")

        call.enqueue(object:retrofit2.Callback<RestaurantResponse> {
            override fun onFailure(call: Call<RestaurantResponse>, t: Throwable) {
                displayToast(t.toString())
            }

            override fun onResponse(call: Call<RestaurantResponse>, response: Response<RestaurantResponse>) {
                if (response.body() != null) {
                    if (response.body()!!.data != null) {
                        nonVegItems.clear()
                        displayToast("Results Fetched")
                        for (i in 0 until response.body()!!.data!!.data!!.size) {

                            val image= response.body()!!.data!!.data!![i].imageName
                            val restaurantName= response.body()!!.data!!.data!![i].hotelName
                            val address= response.body()!!.data!!.data!![i].address
                            val contact= response.body()!!.data!!.data!![i].contactNo
                            val email= response.body()!!.data!!.data!![i].email
                            val description= response.body()!!.data!!.data!![i].description
                            val cusinList=ArrayList<String>()
                            response.body()!!.data!!.data!![i].cusineTypes?.let { cusinList.addAll(it) }
                            val cusineTypes = cusinList.joinToString (separator = " | ") { it -> it }

                            val cusines=ArrayList<String>()
                            val  dineTypes=ArrayList<String>()
                            val  amenities=ArrayList<String>()
                            for(Data in response.body()!!.data!!.data!![i].cusineTypes!!){
                                cusines.add(Data)
                            }
                            for(Data in response.body()!!.data!!.data!![i].amenities!!){
                                amenities.add(Data)
                            }
                            for(Data in response.body()!!.data!!.data!![i].dine!!){
                                dineTypes.add(Data)
                            }
                            val item1= NonVegModel(response.body()!!.data!!.data!![i].restaurantsId.toString(),image!!, restaurantName!!,address!!,cusineTypes,email,contact,description,cusines,amenities,dineTypes)
                            nonVegItems.add(item1)
                        }
                        GetAllRestaurantAdapter(nonVegItems)
                    }
                }
            }

        })
    }


    private fun callApi(catId: Int) {
        val request=RestaurantRequest()
        request.cityId=appPreferences.getString(Constants.CITY_ID);
        request.amenities=""
        request.categoryId=catId.toString()
        request.stateId=""
        request.midId="13"
        request.cuisine=""
        request.localityId=""
        showProgressBar(root_layout)
        val call = APIFactory.getService().getRestaurantList(appPreferences.getString(Constants.CITY_ID),catId.toString(),"","","13","","")

        call.enqueue(object:retrofit2.Callback<RestaurantResponse> {
            override fun onFailure(call: Call<RestaurantResponse>, t: Throwable) {
                dismissProgressBar()
                displayToast(t.toString())
            }

            override fun onResponse(call: Call<RestaurantResponse>, response: Response<RestaurantResponse>) {
                if (response.body() != null) {
                    if (response.body()!!.data != null) {
                        for (i in 0 until response.body()!!.data!!.data!!.size) {
                            val image= response.body()!!.data!!.data!![i].imageName
                            val restaurantName= response.body()!!.data!!.data!![i].hotelName
                            val address= response.body()!!.data!!.data!![i].address
                            val contact= response.body()!!.data!!.data!![i].contactNo
                            val email= response.body()!!.data!!.data!![i].email
                            val description= response.body()!!.data!!.data!![i].description
                            val cusinList=ArrayList<String>()
                            response.body()!!.data!!.data!![i].cusineTypes?.let { cusinList.addAll(it) }
                            val cusineTypes = cusinList.joinToString (separator = " | ") { it -> it }

                            val cusines=ArrayList<String>()
                            val  dineTypes=ArrayList<String>()
                            val  amenities=ArrayList<String>()
                            for(Data in response.body()!!.data!!.data!![i].cusineTypes!!){
                                cusines.add(Data)
                            }
                            for(Data in response.body()!!.data!!.data!![i].amenities!!){
                                amenities.add(Data)
                            }
                            for(Data in response.body()!!.data!!.data!![i].dine!!){
                                dineTypes.add(Data)
                            }
                            val item1= NonVegModel(response.body()!!.data!!.data!![i].restaurantsId.toString(),image!!, restaurantName!!,address!!,cusineTypes,email,contact,description,cusines,amenities,dineTypes)
                            nonVegItems.add(item1)
                        }
                        dismissProgressBar()
                        GetAllRestaurantAdapter(nonVegItems)
                    }
                }
            }

        })
    }

    private fun GetAllRestaurantAdapter(nonVegItems: ArrayList<NonVegModel>) {
        adapter = NonVegAdapter(this@NonVegRestaurantActivity, nonVegItems) { view: View, dataItems: NonVegModel, i: Int ->
            //startActivity(Intent(this, RealEstateDetailsActivity::class.java))

            when (view.id) {
                R.id.cardView -> {
                    Singleton.restaurant_id=nonVegItems[i].restaurantId
                    Singleton.nameOfHall=nonVegItems[i].name
                    Singleton.cuisines=nonVegItems[i].cuisines
                    Singleton.amenities=nonVegItems[i].amenities
                    Singleton.dineTypes=nonVegItems[i].dineTypes
                    Singleton.headerImage= nonVegItems[i].image
                    Singleton.nameOfHall=nonVegItems[i].name
                    Singleton.address=nonVegItems[i].location
                    Singleton.contact=nonVegItems[i].contact
                    Singleton.email=nonVegItems[i].email
                    Singleton.description=nonVegItems[i].description
                    startActivity(Intent(this@NonVegRestaurantActivity, NonVegDetailsActivity::class.java))
                    // adapter?.notifyDataSetChanged()
                }

                R.id.btn_take_appointment->{
                    Singleton.restaurant_id=nonVegItems[i].restaurantId
                    startActivity(Intent(this@NonVegRestaurantActivity, DineInAppoinmentActivity::class.java))
                }

            }
        }
        non_veg_rv.layoutManager = LinearLayoutManager(this@NonVegRestaurantActivity, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        non_veg_rv.adapter = adapter
        non_veg_rv.adapter!!.notifyDataSetChanged()
    }


    private fun callLocalityApi() {
        var request = LocalityRequest()
        request.city_id = cityIdStr?.toInt()!!
        request.state_id = stateIdStr?.toInt()!!
        request.category_id = 47

        val call = APIFactory.getService().localityApi(request)

        call.enqueue(object : Callback<LocalityDataResponse> {
            override fun onResponse(call: Call<LocalityDataResponse>, response: Response<LocalityDataResponse>) {
                if (response.body()!!.status!! == Constants.SUCCESS) {
                    dismissProgressBar()
                    if (response.body()!!.data != null) {
                        for (i in 0 until response.body()!!.data!!.size) {
                            localityList_name.add(response.body()?.data?.get(i)?.locality_name!!.toString())
                            localityList_id.add(response.body()?.data?.get(i)?.locality_id!!.toInt())
                        }
                        setLocalityListToAdapter(localityList_name)
                    }
                    //   displayToast(response.body()!!.message!!)
                } else {
                    displayToast("Error")
                }
            }
            override fun onFailure(call: Call<LocalityDataResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }

    private fun setLocalityListToAdapter(localityList: java.util.ArrayList<String>) {
        if(localityList != null){
            val adapter = ArrayAdapter<String>(this,
                    R.layout.text_row, R.id.spinner_text, localityList)
            locality_list!!.adapter = adapter
        }
    }



    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.filter_button->{
                filter_doctor_dialog!!.show();
            }
            R.id.nav_icon_img->{
                onBackPressed()
            }
        }
    }

}
