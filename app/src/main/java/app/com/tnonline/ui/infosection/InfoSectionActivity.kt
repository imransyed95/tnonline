package app.com.tnonline.ui.infosection

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import app.com.tnonline.R
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.ui.infosection.flight.FlightActivity
import app.com.tnonline.ui.infosection.flight.FlightActivityNew
import app.com.tnonline.ui.infosection.flight.FlightDetailsShowActivity
import app.com.tnonline.ui.infosection.train.TrainActivity
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import kotlinx.android.synthetic.main.activity_info_section.*

class InfoSectionActivity : BaseActivity(), View.OnClickListener {
private val appPreferences=SharedPreferences()

    override fun onClick(view: View?) {
        when (view?.id) {

            R.id.infoTrainLayout -> {
                startActivity((Intent(this@InfoSectionActivity, TrainActivity::class.java)))
            }
            R.id.infoFlightLayout -> {
                if(!appPreferences.getString(Constants.FLIGHT_AREA).isEmpty()){
                    startActivity((Intent(this@InfoSectionActivity, FlightActivityNew::class.java)))
                }else{
                    startActivity((Intent(this@InfoSectionActivity, FlightActivity::class.java)))
                }

            }
            R.id.infoServiceLayout -> {

            }

        }
    }
    override val layoutResId: Int
        get() = R.layout.activity_info_section


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()
        infoTrainLayout.setOnClickListener(this)
        infoFlightLayout.setOnClickListener(this)
        infoServiceLayout.setOnClickListener(this)
    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Info Section"

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


}