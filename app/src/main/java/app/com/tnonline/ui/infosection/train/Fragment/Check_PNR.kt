package app.com.tnonline.ui.infosection.train.Fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import app.com.tnonline.R
import app.com.tnonline.ui.infosection.train.TrainArrivalsActivity
import app.com.tnonline.ui.infosection.train.TrainDetailedActivity
import kotlinx.android.synthetic.main.fragment_check_pnr.*
import kotlinx.android.synthetic.main.fragment_check_pnr.view.*


class Check_PNR : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_check_pnr, container, false)

        view.btn_search_train.setOnClickListener {
            startActivity(Intent(activity, TrainDetailedActivity::class.java))
        }

        view.pnr_input!!.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if(pnr_input.text.length == 10){
                    callPNR()
                }else if (pnr_input.text.length == 0){

                }
            }
        })

        return view
    }

    private fun callPNR() {
//  jtfkr0y3cc
        startActivity(Intent(
                activity,
                TrainArrivalsActivity::class.java
        ))
    }


}