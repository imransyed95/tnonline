package app.com.tnonline.ui.selfservice

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import app.com.tnonline.R
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.ui.LoginActivity
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import kotlinx.android.synthetic.main.activity_dth_recharge.*

class DTHRechargeActivity : BaseActivity() {


    override val layoutResId: Int
        get() = R.layout.activity_dth_recharge

    private val appPreferences = SharedPreferences()
    var loginValue: String? = null


    val providerDatas = arrayOf("Airtel digital TV", "Dish TV", "Sun Direct", "Tata Sky", "Videcon d2h", "BIG TV")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()

        loginValue = appPreferences.getString(Constants.LOGIN_SUCCESS)

        rechargeBtn.setOnClickListener {
            validation()
        }
        spinnerSelectOperator.adapter = ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item, providerDatas)

        spinnerSelectOperator.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.getItemAtPosition(position)
                Log.d("providerDatas Item", "$item")
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    private fun validation() {
        if (edCustomerId.text.toString().trim() == "") {
            displayToast("Please Enter Customer Id")
        } else if (edAmountDth.text.toString().trim() == "") {
            displayToast("Please Enter Amount DTH")
        } else if (!loginValue.equals(Constants.YES, true)) {
            displayToast("Please Login to proceed with Recharge")
            if (!Constants.isFromRecharge) {
                Constants.isFromRecharge = true
            }
            startActivity(Intent(this@DTHRechargeActivity, LoginActivity::class.java))
        } else {
            //PayUMoney
          //  startActivity(Intent(this@DTHRechargeActivity, PayUMoneyActivity::class.java))
        }
    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "DTH Recharge"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


}