package app.com.tnonline.ui.infosection.flight

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.net.http.SslError

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.webkit.SslErrorHandler
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import app.com.tnonline.R
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants

class FlightDetailsShowActivity : BaseActivity() {
    internal var webview: WebView?=null
    internal var activity: Context? = null
    private var progressBar: ProgressDialog? = null
    internal var mHandler = Handler()
    internal var appPreferences = SharedPreferences()
    override val layoutResId: Int
        get() = R.layout.activity_flight_details_show

    @SuppressLint("JavascriptInterface", "SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {

        window.requestFeature(Window.FEATURE_PROGRESS)
        super.onCreate(savedInstanceState)

        setupActionBar()
        this.webview = findViewById<View>(R.id.flight_webview) as WebView

        val settings = webview!!.settings
        settings.javaScriptEnabled = true
        webview!!.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
        webview!!.visibility = View.VISIBLE
        webview!!.settings.builtInZoomControls = true
        //            webView.getSettings().setCacheMode(2);
        webview!!.settings.domStorageEnabled = true
        webview!!.clearHistory()
        webview!!.clearCache(true)
        webview!!.settings.javaScriptEnabled = true
        webview!!.settings.setSupportZoom(true)
        webview!!.settings.useWideViewPort = false
        webview!!.settings.loadWithOverviewMode = false
        val alertDialog = AlertDialog.Builder(this).create()

        progressBar = ProgressDialog.show(this@FlightDetailsShowActivity, "Flight Time", "Loading...")

        webview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                Log.i("TAG", "Processing webview url click...")
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                Log.i("TAG", "Finished loading URL: $url")
                if (progressBar!!.isShowing) {
                    progressBar!!.dismiss()
                }
            }

            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                Log.e("TAG", "Error: $description")
                Toast.makeText(activity, "Oh no! $description", Toast.LENGTH_SHORT).show()
                alertDialog.setTitle("Error")
                alertDialog.setMessage(description)
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "OK", DialogInterface.OnClickListener { dialog, which -> return@OnClickListener })
                alertDialog.show()
            }
        }
        webview!!.loadUrl(appPreferences.getString(Constants.FLIGHT_AREA_URL))
    }
    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = appPreferences.getString(Constants.FLIGHT_AREA)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}