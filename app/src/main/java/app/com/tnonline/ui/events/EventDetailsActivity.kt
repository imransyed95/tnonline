package app.com.tnonline.ui.events

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.LinearLayout
import app.com.tnonline.R
import app.com.tnonline.adapters.FeautreAdapter
import app.com.tnonline.data.remote.models.events.FeatureModel
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Singleton
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_event_details.*
import java.io.File


class EventDetailsActivity :BaseActivity(), View.OnClickListener {

    var venueItems = ArrayList<FeatureModel>()
    var facilityItems = ArrayList<FeatureModel>()
    var adapter: FeautreAdapter? = null
    override val layoutResId: Int
        get() = R.layout.activity_event_details


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val toolbar = findViewById<Toolbar>(app.com.tnonline.R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        venueItems.clear()
        facilityItems.clear()
        Glide.with(this)
                .load(Singleton.headerImage)
                .into(header_image)
        txtName.text = Singleton.nameOfHall
        txtAddress.text = Singleton.address
        txtcontact_no.text = "+91" + Singleton.contact
        txtemail.text = Singleton.email
        txtCapacity.text = Singleton.capacity
        txtAbout.text = Singleton.description

        for (i in 0 until Singleton.venue.size) {
            val model = FeatureModel()
            model.image = Singleton.venue[i]
            // val fileName = Singleton.venue[i].substring(Singleton.venue[i].lastIndexOf('/')+1)
            val u = Uri.parse(Singleton.venue[i])
            val fileName = File("" + u)
            val iend = fileName.name.indexOf(".")
            var subString: String = ""
            if (iend != -1) {
                subString = fileName.name.substring(0, iend); //this will give abc
            }
            model.name = subString
            venueItems.add(model)
        }

        for (i in 0 until Singleton.facility.size) {
            val model = FeatureModel()
            if (Singleton.facility[i] == "Air conditioning") {
                Singleton.facility[i] = "AC"
            }
            model.name = Singleton.facility[i]
            model.image = ""
            facilityItems.add(model)
        }

        setAdpater()
        setFacilityAdpater()
        location_iv.setOnClickListener(this)
        txtcontact_no.setOnClickListener(this)
        txtemail.setOnClickListener(this)
    }

    private fun setFacilityAdpater() {
        adapter = FeautreAdapter(this@EventDetailsActivity, facilityItems as ArrayList<FeatureModel>) { view: View, dataItems: FeatureModel, i: Int ->
            //startActivity(Intent(this, RealEstateDetailsActivity::class.java))
            when (view.id) {
                R.id.cardView -> {

                }

            }
        }
        facility_rv.layoutManager = LinearLayoutManager(this@EventDetailsActivity, LinearLayout.HORIZONTAL, false) as RecyclerView.LayoutManager?
        facility_rv.adapter = adapter
        facility_rv.adapter!!.notifyDataSetChanged()
    }

    private fun setAdpater() {
        adapter = FeautreAdapter(this@EventDetailsActivity, venueItems as ArrayList<FeatureModel>) { view: View, dataItems: FeatureModel, i: Int ->
            //startActivity(Intent(this, RealEstateDetailsActivity::class.java))
            when (view.id) {
                R.id.cardView -> {

                }

            }
        }
        venue_rv.layoutManager = LinearLayoutManager(this@EventDetailsActivity, LinearLayout.HORIZONTAL, false) as RecyclerView.LayoutManager?
        venue_rv.adapter = adapter
        venue_rv.adapter!!.notifyDataSetChanged()
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.location_iv -> {
                Singleton.address?.let { loadNavigationView(it) }
            }
            R.id.txtcontact_no -> {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:"+txtcontact_no.text)
                startActivity(intent)
            }
            R.id.txtemail->{
                val intent = Intent(Intent.ACTION_SENDTO) // it's not ACTION_SEND
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email")
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email")
                intent.data = Uri.parse("mailto:"+txtemail.text) // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent)
            }
        }
    }


    fun loadNavigationView(address: String) {
        val mapUri = Uri.parse("geo:0,0?q=" + Uri.encode(address))
        val mapIntent = Intent(Intent.ACTION_VIEW, mapUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }

}




