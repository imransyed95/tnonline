package app.com.tnonline.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import app.com.tnonline.R
import app.com.tnonline.adapters.CityImagesAdapter
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.ui.infosection.flight.FlightActivity
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import kotlinx.android.synthetic.main.activity_settings.*
import org.json.JSONArray
import java.io.InputStream
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.locality.LocalityDataResponse
import app.com.tnonline.data.remote.models.locality.LocalityRequest
import app.com.tnonline.data.remote.models.locality.SpecialistDataResponse
import app.com.tnonline.ui.infosection.flight.FlightActivityNew
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SettingsActivity : BaseActivity() {

    private var cityImagesAdapter: CityImagesAdapter? = null
    private val appPreferences = SharedPreferences()
    var trainName = ArrayList<String>()
    private var localityList = ArrayList<String>()
    var goback: TextView? = null

    override val layoutResId: Int
        get() = R.layout.activity_settings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()

        goback = findViewById(R.id.goback) as TextView

        llyCity.setOnClickListener{funSkipLogin()}
        autoCompleteTrain.setText(appPreferences.getString(Constants.STATION_CODE))
        llyAirport.setOnClickListener{
            funcFlight()
        }
        funValueSet()
        setupAdapter()
        cancelButton.setOnClickListener{
            autoCompleteTrain.setText(" ")
            setupAdapter()
            functionAutoComSetValue()
        }
        autoCompleteTrain.setOnClickListener{

            autoCompleteTrain.isFocusableInTouchMode = true
            autoCompleteTrain.requestFocus()
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(autoCompleteTrain, InputMethodManager.SHOW_IMPLICIT)
        }
     functionAutoComSetValue()

        autoCompleteTrain.onItemClickListener=object :AdapterView.OnItemClickListener{
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val item = parent!!.getItemAtPosition(position).toString()
                Log.d("providerDatas Item", "$item")

                appPreferences.setString(Constants.STATION_CODE,item)
            }

        }

        //Get Locality
        getLocalityApi()

        goback!!.setOnClickListener {
            onBackPressed()
        }

    }

    private fun functionAutoComSetValue() {
        val adapter = ArrayAdapter<String>(this, R.layout.spinner_text_center, R.id.spinner_text, trainName)
        adapter.setDropDownViewResource(R.layout.spinner_text_center)
        autoCompleteTrain.threshold = 1
        autoCompleteTrain.setAdapter(adapter)}


    private fun funValueSet() {
        if(TextUtils.isEmpty(appPreferences.getString(Constants.CITY_NAME)))
        {
            txtCity.text=="--"
        }else{
            txtCity.text=appPreferences.getString(Constants.CITY_NAME)
        }
        if(TextUtils.isEmpty(appPreferences.getString(Constants.FLIGHT_CITY)))
        {
            txtAirport.text=="--"
        }else{
            txtAirport.text=appPreferences.getString(Constants.FLIGHT_CITY)
        }


    }

    override fun onResume() {
        super.onResume()
        funValueSet()
    }

    private fun funSkipLogin() {

        val intent=Intent(this, SkipLoginActivity::class.java)
        intent.putExtra(Constants.SETTINGS,"settings")
        startActivity(intent)
    }
    private fun funcFlight() {
        startActivity(Intent(this,FlightActivity::class.java))

    }
    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Settings"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupAdapter() {

        val trainData = JSONArray(readStateJSONFromAsset())
        Log.e("TrainName", "$trainData")

        for (i in 0 until trainData.length()) {
            val jsonObject = trainData.getJSONObject(i)
            val state = jsonObject.getString("stationName")
            trainName.add(state)

            Log.e("TrainName", state)

        }


    }

    private fun readStateJSONFromAsset(): String? {
        val json: String?
        try {
            val inputStream: InputStream = assets.open("tnRailwayStation.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json

    }


    private fun getLocalityApi() {
        showProgressBar(llParent)
        var request = LocalityRequest()
        request.state_id = appPreferences.getString(Constants.STATE_ID).toInt()!!
        request.city_id = appPreferences.getString(Constants.CITY_ID).toInt()!!
        request.category_id = 0

        val call = APIFactory.getService().localityApi(request)

        call.enqueue(object : Callback<LocalityDataResponse> {
            override fun onResponse(call: Call<LocalityDataResponse>, response: Response<LocalityDataResponse>) {
                if (response.body()!!.status!! == Constants.SUCCESS) {
                    dismissProgressBar()

                    if (response.body()!!.data != null) {
                        for (i in 0 until response.body()!!.data!!.size) {
                            localityList.add(response.body()?.data?.get(i)?.locality_name.toString())

                        }
                        setAutoText(localityList)
                    }

                    //   displayToast(response.body()!!.message!!)
                } else {
                    displayToast("Error")
                }
            }

            override fun onFailure(call: Call<LocalityDataResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }

    private fun setAutoText(localityList: ArrayList<String>) {
       /* spinner_locality.adapter = ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item, localityList)
        spinner_locality.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.getItemAtPosition(position)
                Log.d("classList", "$item")
                displayToast("$item")
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }*/
        val adapter = ArrayAdapter<String>(this, R.layout.spinner_text_center, R.id.spinner_text, localityList)
        adapter.setDropDownViewResource(R.layout.spinner_text_center)
        auto_locality.threshold = 1
        auto_locality.setAdapter(adapter)


        auto_locality.setOnClickListener{

            auto_locality.isFocusableInTouchMode = true
            auto_locality.requestFocus()
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(autoCompleteTrain, InputMethodManager.SHOW_IMPLICIT)
        }

        cancel_loc_btn.setOnClickListener {
            auto_locality.setText("  ")
        }
    }


}