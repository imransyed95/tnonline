package app.com.tnonline.ui.infosection.train

import android.os.Bundle
import android.view.MenuItem
import app.com.tnonline.R
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.utils.BaseActivity


class PnrActivity : BaseActivity() {

    private val appPreferences = SharedPreferences()
    override val layoutResId: Int
        get() = R.layout.activity_pnr

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()

    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Check PNR"
    }




    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
