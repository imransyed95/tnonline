package app.com.tnonline.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import app.com.tnonline.R
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.login.LoginRequest
import app.com.tnonline.data.remote.models.login.LoginResponse
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import app.com.tnonline.utils.Constants.ERROR
import app.com.tnonline.utils.Constants.SUCCESS
import app.com.tnonline.utils.Utilities.isValidEmail
import app.com.tnonline.utils.Utilities.isValidPassword
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.log


class LoginActivity : BaseActivity(), View.OnClickListener {


    private val appPreferences = SharedPreferences()

    override val layoutResId: Int
        get() = R.layout.activity_login

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        btnForgotPassword.setOnClickListener(this)
        btnLogin.setOnClickListener(this)
        btnRegister.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnLogin -> {
                loginValidation()
            }

            R.id.btnRegister -> {
                startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
            }

            R.id.btnForgotPassword -> {
                startActivity(Intent(this@LoginActivity, ForgotPasswordActivity::class.java))
            }
        }
    }

    private fun loginValidation() {
        if (edEmail_Login.text.toString().trim() == "") {
            displayToast("Please Enter your mail Id")
        } else if (!isValidEmail(edEmail_Login.text.toString().trim())) {
            displayToast("Please Enter valid Email Id")
        } else if (edPassword_Login.text.toString().trim() == "") {
            displayToast("Please Enter your Password")
        } else if (!isValidPassword(edPassword_Login.text.toString().trim())) {
            displayToast("Password must be Minimum 8 letters in alpha numeric and special characters")
        } else {
            loginAPI()
        }
    }

    private fun loginAPI() {
        showProgressBar(llParentLogin)
        val loginRequest = LoginRequest()
        loginRequest.email = edEmail_Login!!.text!!.trim().toString()
        loginRequest.password = edPassword_Login.text!!.trim().toString()
        val call = APIFactory.getService().loginUser(loginRequest)
        call.enqueue(object : Callback<LoginResponse> {

            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if (response.body()!!.status.equals(SUCCESS)) {
                    dismissProgressBar()
                    displayToast(response.body()!!.message!!)
                    appPreferences.setString(Constants.LOGIN_SUCCESS, Constants.YES)
                    appPreferences.setString(Constants.LOGIN_AUTH, response.body()!!.token!!)
                    appPreferences.setString(Constants.LOGIN_EMAIL, response.body()!!.data!!.email!!)
                    appPreferences.setString(Constants.LOGIN_NAME, response.body()!!.data!!.full_name!!)
                    appPreferences.setString(Constants.CITY_NAME, response.body()!!.city!!.city_name!!)
                    appPreferences.setString(Constants.CITY_ID, response.body()!!.city!!.city_id.toString())
                    appPreferences.setString(Constants.CITY_IMAGE, response.body()!!.city!!.img!!)
                    appPreferences.setString(Constants.STATE_ID, response.body()!!.data!!.state_id.toString())

                    var url = appPreferences.getString(Constants.CITY_IMAGE)

                    if (url.contains(" ")) {
                        url = url.replace(" ", "%20")

                        appPreferences.setString(Constants.CITY_IMAGE, url)
                    } else {
                        Log.e("TAG", "URL has proper URl")
                    }

                    if (Constants.isFromRecharge) {
                        appPreferences.setString(Constants.LOGIN_SUCCESS, Constants.YES)
                        finish()
                    } else {
                        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                    }
                    finish()
                } else if (response.body()!!.status.equals(ERROR)) {
                    displayToast(response.body()!!.message!!)
                    dismissProgressBar()
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }
}