package app.com.tnonline.ui.infosection.train

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import app.com.tnonline.R
import app.com.tnonline.adapters.LawyersListAdapter
import app.com.tnonline.adapters.TrainBetweenStationAdapter
import app.com.tnonline.data.local.SingleTon
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.doctor.response.Datum
import app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.TrainBetweenStation.TrainBetweenStationResponse
import app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.TrainBetweenStation.Trains
import app.com.tnonline.ui.classified.LawyersDetailsActivity
import app.com.tnonline.utils.Constants
import kotlinx.android.synthetic.main.activity_train_detailed.*
import kotlinx.android.synthetic.main.activity_train_detailed.recyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TrainDetailedActivity : app.com.tnonline.utils.BaseActivity() {

    var mLayoutManager: LinearLayoutManager? = null
    private var trainBetweenStationAdapter: TrainBetweenStationAdapter? = null


    override val layoutResId: Int
        get() = R.layout.activity_train_detailed //To change initializer of created properties use File | Settings | File Templates.


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        tv_from.setText(intent.getStringExtra("sou_code"))
        tv_to.setText(intent.getStringExtra("des_code"))
        listAllTrains(intent.getStringExtra("sou_code"), intent.getStringExtra("des_code"))
    }

    private fun listAllTrains(source_code: String, Destination_code: String) {
        val call = APIFactory.getTrainService().trainBetweenStation(source_code!!, Destination_code, "12-04-2019", Constants.TRAIN_KEY)

        call.enqueue(object : Callback<TrainBetweenStationResponse> {
            override fun onResponse(call: Call<TrainBetweenStationResponse>, response: Response<TrainBetweenStationResponse>) {
                if (response.body() != null) {
                    if (response.body()!!.responseCode == 200) {
                        dismissProgressBar()
                        if (response.body()!!.trains != null) {
                            setUpAdapter(response.body()!!.trains)
                        }
                    } else {
                        dismissProgressBar()
                    }
                } else {
                    displayToast("Error")
                    dismissProgressBar()
                }

            }

            override fun onFailure(call: Call<TrainBetweenStationResponse>, t: Throwable) {
                dismissProgressBar()
                displayToast(t.toString())
            }
        })
    }

    private fun setUpAdapter(stations: ArrayList<Trains?>?) {
        trainBetweenStationAdapter = TrainBetweenStationAdapter(this, stations){view: View, string: String,  i: Int ->
            when (view.id) {

            }
        }
        mLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLayoutManager

        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        recyclerView.adapter = trainBetweenStationAdapter
        recyclerView.adapter!!.notifyDataSetChanged()

    }
}
