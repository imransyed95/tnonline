package app.com.tnonline.ui.selfservice

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import app.com.tnonline.R
import app.com.tnonline.utils.BaseActivity
import kotlinx.android.synthetic.main.activity_self_service.*

class SelfServiceActivity : BaseActivity(), View.OnClickListener {


    override val layoutResId: Int
        get() = R.layout.activity_self_service


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()

        infoMobileRechargeLayout.setOnClickListener(this)
        infoDTHLayout.setOnClickListener(this)
        infoElectricityLayout.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
       if (intent.getBooleanExtra("EXIT", false)) {
            finish()
        }
    }
    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Self Service"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.infoMobileRechargeLayout -> {
                startActivity(Intent(this@SelfServiceActivity, MobileRechargeActivity::class.java))
                finish()
            }
            R.id.infoDTHLayout -> {
                startActivity(Intent(this@SelfServiceActivity, DTHRechargeActivity::class.java))
            }
            R.id.infoElectricityLayout -> {
                startActivity(Intent(this@SelfServiceActivity, MobileRechargeActivity::class.java))
            }
        }
    }


}