package app.com.tnonline.ui.infosection.train

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import app.com.tnonline.R
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.train.arrival.arrival.DataItem
import app.com.tnonline.data.remote.models.train.arrival.trainlist.TrainListResponse
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import app.com.tnonline.utils.Utilities
import kotlinx.android.synthetic.main.activity_set_available.*
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.InputStream

class SeatsAvailableActivity : BaseActivity(), View.OnClickListener {
    var context: Context? = null
    var classList = ArrayList<String>()
    var quotaList = ArrayList<String>()
    var trainInfoList = ArrayList<DataItem>()
    var trainName = ArrayList<String>()
    var stringQuota: String? = null
    var stringClass: String? = null
    var stringFromStationCode: String? = null
    var stringToStationCode: String? = null
    private val appPreferences = SharedPreferences()
    override val layoutResId: Int
        get() = R.layout.activity_set_available

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this
        setupActionBar()
        // listOfTrainArrivalAPI()

        classList.add("AC First Class (1A)")
        classList.add("AC 2 Tier (2A)")
        classList.add("AC 3 Tier (3A)")
        classList.add("Anubhuti Class (EA)")
        classList.add("Exec. Charir Car (EC)")
        classList.add("AC Chair Car(CC)")
        classList.add("Sleeper (SL)")
        classList.add("Second Sitting (2S)")
        classList.add("Third AC Economy (3E)")
        classList.add("First Class (FC)")


        quotaList.add("General Quota (GN)")
        quotaList.add("Tatkal (TQ)")
        quotaList.add("Premium Tatkal (PT)")
        quotaList.add("Lower Berth (SS)")
        quotaList.add("Ladies (LD)")
        quotaList.add("Physically Handicapped (HP)")
        quotaList.add("Duty Pass Quota (DP)")
        quotaList.add("Defence Quota (DF)")

        txtCalendar.setOnClickListener(this)


        setSpinnerValues()
        setupAdapter()
    }


    private fun setSpinnerValues() {

        //classs
        spinnerClasse.adapter = ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item, classList)

        spinnerClasse.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.getItemAtPosition(position)
                Log.d("classList", "$item")
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
        spinnerClasse.setSelection(6)

        //Quota

        spinnerQuota.adapter = ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item, quotaList)

        spinnerQuota.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.getItemAtPosition(position)
                        Log.d("classList", "$item")
                stringQuota = getStringCode(quotaList[position])
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
        spinnerQuota.setSelection(0)


    }



    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Seats Availability"
    }

    private fun listOfTrainArrivalAPI() {
        showProgressBar(llParent)

        val call = APIFactory.getService().getTrainInfoList()

        call.enqueue(object : Callback<TrainListResponse> {
            override fun onResponse(call: Call<TrainListResponse>, response: Response<TrainListResponse>) {
                if (response.body()!!.status!! == Constants.SUCCESS) {
                    dismissProgressBar()

                    if (response.body()!!.data != null) {
                        trainInfoList.addAll(response.body()!!.data!!.filterNotNull())
                        /*for (i in 0 until response.body()!!.data!!.size) {
                            trainCode .add( response.body()!!.data!![i]!!.trainCode!!)
                            trainName .add( response.body()!!.data!![i]!!.trainName!!)

                        }*/

                    }

                    //   displayToast(response.body()!!.message!!)
                } else {
                    displayToast("Error")
                }
            }

            override fun onFailure(call: Call<TrainListResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }

    private fun setupAdapter() {
        val trainData = JSONArray(readStateJSONFromAsset())
        Log.e("TrainName", "$trainData")
        for (i in 0 until trainData.length()) {
            val jsonObject = trainData.getJSONObject(i)
            val state = jsonObject.getString("stationName")
            trainName.add(state)
            Log.e("TrainName", state)
        }

        val adapter = ArrayAdapter<String>(this, R.layout.spinner_text_center, R.id.spinner_text, trainName)
        adapter.setDropDownViewResource(R.layout.spinner_text_center)
        autoCompleteFromStation.threshold = 1
        autoCompleteFromStation.setAdapter(adapter)

        val adapterToStation = ArrayAdapter<String>(this, R.layout.spinner_text_center, R.id.spinner_text, trainName)
        adapterToStation.setDropDownViewResource(R.layout.spinner_text_center)
        autoCompleteToStation.threshold = 1
        autoCompleteToStation.setAdapter(adapterToStation)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtCalendar -> {
                Utilities.openDate(context!!, txtCalendar)
            }
            R.id.rechargeBtn -> {
                validation()
            }
            else -> {
            }
        }
    }

    private fun validation() {

        when {
            autoCompleteFromStation.text.toString().trim() == ("") -> displayToast("Please Enter From station")
            autoCompleteToStation.text.toString().trim() == ("") -> displayToast("Please Enter To station")
            txtCalendar.text.toString().trim() == ("") -> displayToast("Please Enter your Journey Date")
            stringQuota.isNullOrEmpty() -> displayToast("Please Enter Train Quota")
            stringClass.isNullOrEmpty() -> displayToast("Please Enter travel class")
            autoCompleteFromStation.text == autoCompleteToStation.text -> displayToast("From station and to station should not be same")
            else -> {

            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun readStateJSONFromAsset(): String? {
        val json: String?
        try {
            val inputStream: InputStream = assets.open("tnRailwayStation.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json

    }
}
