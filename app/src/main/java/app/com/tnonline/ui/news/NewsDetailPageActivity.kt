package app.com.tnonline.ui.news

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.CircularProgressDrawable
import android.widget.ImageView
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.remote.models.news.DataItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import org.json.JSONObject

class NewsDetailPageActivity : AppCompatActivity() {

    var details: String = ""

    var back_button : ImageView? = null
    var image : ImageView? = null

    var title: TextView? = null
    var date: TextView? = null
    var description: TextView? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail_page)

       // details = intent.extras.getString("newdetail")

        back_button = findViewById(R.id.back_button) as ImageView
        image = findViewById(R.id.image) as ImageView

        title = findViewById(R.id.title) as TextView
        date = findViewById(R.id.date) as TextView
        description = findViewById(R.id.description) as TextView



        back_button!!.setOnClickListener {
            onBackPressed()
        }

     //   try{
            val circularProgressDrawable = CircularProgressDrawable(applicationContext)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()
            Glide.with(applicationContext)
                    .load(intent.extras.getString("imageUrl"))
                    .apply(RequestOptions().placeholder( circularProgressDrawable))
                    .into(image!!)

            title!!.setText(intent.extras.getString("title"))
            date!!.setText(intent.extras.getString("pubdate"))
            description!!.setText(intent.extras.getString("description"))

//        }catch (e: Exception){
//
//        }



    }
}
