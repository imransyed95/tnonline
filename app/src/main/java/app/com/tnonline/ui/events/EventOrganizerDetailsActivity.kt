package app.com.tnonline.ui.events

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.LinearLayout
import app.com.tnonline.R
import app.com.tnonline.adapters.EventFeautreAdapter
import app.com.tnonline.data.remote.models.events.FeatureModel
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Singleton
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_event_organizer_details.*

class EventOrganizerDetailsActivity : BaseActivity(), View.OnClickListener {

    var eventType = ArrayList<FeatureModel>()
    var cateringFeature = ArrayList<FeatureModel>()
    var cateringCuisines = ArrayList<FeatureModel>()
    var adapter: EventFeautreAdapter? = null

    override val layoutResId: Int
        get() = R.layout.activity_event_organizer_details

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        eventType.clear()
        cateringFeature.clear()
        cateringCuisines.clear()
        val toolbar = findViewById<Toolbar>(app.com.tnonline.R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        Glide.with(this)
                .load(Singleton.headerImage)
                .into(header_image)
        txtName.text = Singleton.nameOfHall
        txtAddress.text = Singleton.address
        txtcontact_no.text = "+91" + Singleton.contact
        txtemail.text = Singleton.email
        txtAbout.text = Singleton.description

        for (i in 0 until Singleton.eventType.size) {
            val model = FeatureModel()
            model.name = Singleton.eventType[i]
            model.image = ""
            eventType.add(model)
        }

        for (i in 0 until Singleton.cateringFeature.size) {
            val model = FeatureModel()
            model.name = Singleton.cateringFeature[i]
            model.image = ""
            cateringFeature.add(model)
        }

        for (i in 0 until Singleton.cateringCuisine.size) {
            val model = FeatureModel()
            model.name = Singleton.cateringCuisine[i]
            model.image = ""
            cateringCuisines.add(model)
        }

        setEventTypeAdpater()
        setCateringFeatureAdpater()
        setCateringCuisinesAdpater()
        location_iv.setOnClickListener(this)
        txtcontact_no.setOnClickListener(this)
        txtemail.setOnClickListener(this)
    }

    private fun setCateringCuisinesAdpater() {
        adapter = EventFeautreAdapter(this@EventOrganizerDetailsActivity, cateringCuisines as ArrayList<FeatureModel>) { view: View, dataItems: FeatureModel, i: Int ->
            //startActivity(Intent(this, RealEstateDetailsActivity::class.java))
            when (view.id) {
                R.id.cardView -> {

                }

            }
        }
        catering_cuisine_rv.layoutManager = LinearLayoutManager(this@EventOrganizerDetailsActivity, LinearLayout.HORIZONTAL, false) as RecyclerView.LayoutManager?
        catering_cuisine_rv.adapter = adapter
        catering_cuisine_rv.adapter!!.notifyDataSetChanged()
    }


    private fun setEventTypeAdpater() {
        adapter = EventFeautreAdapter(this@EventOrganizerDetailsActivity, eventType as ArrayList<FeatureModel>) { view: View, dataItems: FeatureModel, i: Int ->
            //startActivity(Intent(this, RealEstateDetailsActivity::class.java))
            when (view.id) {
                R.id.cardView -> {

                }

            }
        }
        event_type_rv.layoutManager = LinearLayoutManager(this@EventOrganizerDetailsActivity, LinearLayout.HORIZONTAL, false) as RecyclerView.LayoutManager?
        event_type_rv.adapter = adapter
        event_type_rv.adapter!!.notifyDataSetChanged()
    }

    private fun setCateringFeatureAdpater() {
        adapter = EventFeautreAdapter(this@EventOrganizerDetailsActivity, cateringFeature as ArrayList<FeatureModel>) { view: View, dataItems: FeatureModel, i: Int ->
            //startActivity(Intent(this, RealEstateDetailsActivity::class.java))
            when (view.id) {
                R.id.cardView -> {

                }

            }
        }
        facility_rv.layoutManager = LinearLayoutManager(this@EventOrganizerDetailsActivity, LinearLayout.HORIZONTAL, false) as RecyclerView.LayoutManager?
        facility_rv.adapter = adapter
        facility_rv.adapter!!.notifyDataSetChanged()
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.location_iv -> {
                Singleton.address?.let { loadNavigationView(it) }
            }
            R.id.txtcontact_no -> {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:"+txtcontact_no.text)
                startActivity(intent)
            }
            R.id.txtemail->{
                val intent = Intent(Intent.ACTION_SENDTO) // it's not ACTION_SEND
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email")
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email")
                intent.data = Uri.parse("mailto:"+txtemail.text) // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent)
            }
        }
    }


    fun loadNavigationView(address: String) {
        val mapUri = Uri.parse("geo:0,0?q=" + Uri.encode(address))
        val mapIntent = Intent(Intent.ACTION_VIEW, mapUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }
}
