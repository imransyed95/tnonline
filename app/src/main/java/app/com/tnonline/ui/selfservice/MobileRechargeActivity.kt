package app.com.tnonline.ui.selfservice

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import app.com.tnonline.R
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.recharge.createorder.RechargeRequest
import app.com.tnonline.data.remote.models.recharge.createorder.RechargeResponse
import app.com.tnonline.data.remote.models.recharge.orderstatusupdate.OrderStatusRequest
import app.com.tnonline.data.remote.models.recharge.orderstatusupdate.OrderStatusResponse
import app.com.tnonline.data.remote.models.recharge.orderstatusupdate.PaymentDesc
import app.com.tnonline.ui.LoginActivity
import app.com.tnonline.ui.MainActivity
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import com.payumoney.core.PayUmoneyConfig
import com.payumoney.core.PayUmoneyConstants
import com.payumoney.core.PayUmoneySdkInitializer
import com.payumoney.core.entity.TransactionResponse
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager
import com.payumoney.sdkui.ui.utils.ResultModel
import kotlinx.android.synthetic.main.activity_mobile_recharge.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.IOException
import java.io.InputStream
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import java.util.regex.Pattern


class  MobileRechargeActivity : BaseActivity() {
    private var mPaymentParams: PayUmoneySdkInitializer.PaymentParam? = null
    var providerDatasPrepaid = ArrayList<String>()
    var providerDatasPostPaid = ArrayList<String>()
    var providerDatasCode = ArrayList<String>()
    var stateDatas = ArrayList<String>()
    var stateDatasCode = ArrayList<Int>()
    var rechargeID:String? = null
    var orderId: String? = null
    var rechargeType: Int? = 2
    var operatorCode: String? = null
    var stateCode: Int? = null
    private val appPreferences = SharedPreferences()
    var loginValue: String? = null
    internal var merchant_Key = "tIQvJyK1"
    internal var salt = "RoslSSM13d"
    internal var merchant_ID = "6802740"
   // internal var merchant_ID = "2td5wvhn"
    internal var furl = "https://www.payumoney.com/mobileapp/payumoney/failure.php"
    internal var surl = "https://www.payumoney.com/mobileapp/payumoney/success.php"
    var context: Context? =null
    internal var debug = true
    override val layoutResId: Int
        get() = R.layout.activity_mobile_recharge


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context=this
        setupActionBar()

        spinnerSelectOperator.prompt = "Select Operator"
        spinnerSelectCircle.prompt = "Select Circle"

        val radioGroup = findViewById<RadioGroup>(R.id.radioPay)
        radioGroup.setOnCheckedChangeListener { p0, p1 ->

            Log.e("Radio Group", "$p0")
            Log.e("Radio Group", "$p1")
        }
        rechargeBtn.setOnClickListener {
            validation()
        }

        val providerdataData = JSONArray(readProviderOperatorJSONFromAsset())
        Log.e("Operator", "$providerdataData")

        for (i in 0 until providerdataData.length()) {
            val jsonObject = providerdataData.getJSONObject(i)


            val operatorData = jsonObject.getString(Constants.OPERATOR)
            val code = jsonObject.getString(Constants.CODE)
            val type = jsonObject.getString(Constants.TYPE)

            if (type == Constants.POST_PAID) {
                providerDatasPostPaid.add(operatorData)
            } else if (type == Constants.MOBILE) {
                providerDatasPrepaid.add(operatorData)
            }

            providerDatasCode.add(code)

            Log.e("Operator", "$operatorData , $code , $type")

        }

        spinnerSelectOperator.adapter = ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item, providerDatasPrepaid)



        spinnerSelectOperator.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.getItemAtPosition(position)
                Log.d("providerDatas Item", "$item")

                if (position >= 0) {
                    operatorCode = providerDatasCode[position]
                }

                Log.d("providerDatas Item", "$operatorCode")

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }


        val stateData = JSONArray(readStateJSONFromAsset())
        Log.e("State", "$stateData")

        for (i in 0 until stateData.length()) {
            val jsonObject = stateData.getJSONObject(i)
            val state = jsonObject.getString("state")
            val code = jsonObject.getString("code")


            stateDatas.add(state)
            stateDatasCode.add(code.toInt())


            Log.e("State", "$state , $code")

        }
        spinnerSelectCircle.adapter = ArrayAdapter(    this,
                R.layout.support_simple_spinner_dropdown_item, stateDatas)
        var a=0

            for (i in 0 until stateDatas.size) {
            Log.i("stateDatas",stateDatas.get(i))
               if( stateDatas.get(i).equals("Tamil Nadu"))
               {
                   a=i
                   break
               }
            }

        spinnerSelectCircle.setSelection(a)


        spinnerSelectCircle.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.getItemAtPosition(position)
                Log.d("stateDatas Item", "$item")

                if (position >= 0) {
                    stateCode = stateDatasCode[position]
                }
                Log.d("stateDatas Item", "$stateCode")


            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }


        val radioPre = findViewById<RadioButton>(R.id.radioPrepaid)
        radioPre.setOnClickListener {
            rechargeType = 2

            spinnerSelectOperator.adapter = ArrayAdapter(
                    this,
                    R.layout.support_simple_spinner_dropdown_item, providerDatasPrepaid)
        }

        val radioPost = findViewById<RadioButton>(R.id.radioPostPaid)
        radioPost.setOnClickListener {
            rechargeType = 1
            spinnerSelectOperator.adapter = ArrayAdapter(
                    this,
                    R.layout.support_simple_spinner_dropdown_item, providerDatasPostPaid)

        }


      /*  val bundle = intent.extras

        if (bundle != null) {
            Log.e("Mobile Recharge", "From Payumoney")

            val status = bundle.getBoolean("status")
            val transactionID = bundle.getInt("transaction_id")

            Log.e("Mobile Recharge", " Status : $status , Transaction Id $transactionID")


            orderStatusUpdateAPI(status.toString(), transactionID.toString())

        } else {
            Log.e("Mobile Recharge", "Not from PAyumoney")
        }*/
    }




    private fun readProviderOperatorJSONFromAsset(): String? {
        val json: String?
        try {
            val inputStream: InputStream = assets.open("provider-operator.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json

    }

    private fun readStateJSONFromAsset(): String? {
        val json: String?
        try {
            val inputStream: InputStream = assets.open("state.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json

    }


    private fun validation() {

        loginValue = appPreferences.getString(Constants.LOGIN_SUCCESS)

        if (edMobileNumberRecharge.text.toString().trim() == "") {
            displayToast("Please Enter Mobile Number")
        } else if (edMobileNumberRecharge.length() < 10) {
            displayToast("Please Enter 10 digit Mobile Number")
        }  else if (!isValidPhone(edMobileNumberRecharge.text.toString().trim())) run {
            displayToast("Please Enter Mobile Number")
        } else if (edAmountRecharge.text.toString().trim() == "") {
            displayToast("Please Enter Amount")
        } else if (!loginValue.equals(Constants.YES, true)) {
            displayToast("Please Login to proceed with Recharge")
            if (!Constants.isFromRecharge) {
                Constants.isFromRecharge = true
            }
            startActivity(Intent(this@MobileRechargeActivity, LoginActivity::class.java))
        } else {
            //  Call Create Order API Once its done call Payumoney then Create Order Status.
            if(internetCheck(context!!))
            {
                createOrderAPI()
            }else{
                displayToast(Constants.INTERNETCONNECTION)
            }

        }
    }

    fun isValidPhone(phone: String): Boolean {
        val pattern = Pattern.compile("^[987]\\d{9}$")

        val matcher = pattern.matcher(phone)
        return matcher.matches()
    }
    private fun createOrderAPI() {

        val rechargeRequest = RechargeRequest()
        rechargeRequest.domain = "TNOAPP"
        rechargeRequest.device_type = "Android"
        rechargeRequest.cityId = appPreferences.getString(Constants.CITY_ID)
        rechargeRequest.stateId = stateCode.toString()
        rechargeRequest.number = edMobileNumberRecharge.text.toString().trim()
        rechargeRequest.operatorId = operatorCode
        rechargeRequest.price = edAmountRecharge.text.toString().trim()
        rechargeRequest.rechargeType = rechargeType.toString()


        val NAME = appPreferences.getString(Constants.LOGIN_NAME)
        val EMAIL_ID = appPreferences.getString(Constants.LOGIN_EMAIL)
        val CITY_ID = appPreferences.getString(Constants.CITY_ID)
        val STATE_ID = stateCode.toString()
        val AMOUNT = edAmountRecharge.text.toString().trim()
        val RECHARGE_TYPE = rechargeType
        val MOBILE_NUMBER = edMobileNumberRecharge.text.toString().trim()
        val OPERATOR_ID = operatorCode
        Log.e("RechargeStuffs", "{$NAME , $EMAIL_ID , $CITY_ID , $STATE_ID , $AMOUNT , $RECHARGE_TYPE , $MOBILE_NUMBER , $OPERATOR_ID}")


        Log.e("Recharge Activity", "createOrderAPI ")


        val call = APIFactory.getClientWithIntercepter()!!.createOrder(rechargeRequest)

        call.enqueue(object : retrofit2.Callback<RechargeResponse> {

            override fun onResponse(call: Call<RechargeResponse>, response: Response<RechargeResponse>) {
                if (response.body()!!.status == Constants.SUCCESS) {

                   // displayToast(response.body()!!.message)
                    Log.e("Recharge Activity", "createOrderAPI ")


                    orderId = response.body()!!.data!!.orderId
                    rechargeID = response.body()!!.data!!.rechargeId

                    Log.e("TAG", "$orderId")
                    Log.e("TAG", "$rechargeID")

                    //Call PAYuMoney
               Constants.isFromMobileRecharge = true
                    launchPayUMoneyFlow( NAME,  EMAIL_ID ,CITY_ID  ,STATE_ID , AMOUNT , RECHARGE_TYPE, MOBILE_NUMBER , OPERATOR_ID, orderId!!, rechargeID!! )
/*
                    val intent = Intent(this@MobileRechargeActivity, PayUMoneyActivity::class.java)
                    intent.putExtra(Constants.LOGIN_NAME, NAME)
                    intent.putExtra(Constants.LOGIN_EMAIL, EMAIL_ID)
                    intent.putExtra(Constants.CITY_ID, CITY_ID)
                    intent.putExtra(Constants.STATE_ID, STATE_ID)
                    intent.putExtra("amount", AMOUNT.toDouble())
                    intent.putExtra("recharge_type", RECHARGE_TYPE)
                    intent.putExtra("mobile_number", MOBILE_NUMBER)
                    intent.putExtra("operator_id", OPERATOR_ID)
                    intent.putExtra("id", "one")
                    startActivity(intent)*/
                } else {
                    displayToast(response.body()!!.message)
                }

            }

            override fun onFailure(call: Call<RechargeResponse>, t: Throwable) {


            }
        })
    }


    private fun orderStatusUpdateAPI(status: String, txnId: String) {

        val orderStatusRequest = OrderStatusRequest()
        orderStatusRequest.orderId = orderId
        orderStatusRequest.paymentDesc = PaymentDesc()
        orderStatusRequest.paymentStatus = status
        orderStatusRequest.paymentTxnId = txnId
        orderStatusRequest.rechargeId = rechargeID

Log.i("orderStatusUpdateAPI : ","orderId:"+orderId+" status: "+status+" txnId: "+txnId+" rechargeID :"+rechargeID)
        val call = APIFactory.getClientWithIntercepter()!!.orderStatusUpdate(orderStatusRequest)

        call.enqueue(object : retrofit2.Callback<OrderStatusResponse> {

            override fun onResponse(call: Call<OrderStatusResponse>, response: Response<OrderStatusResponse>) {
                if (response.body()!!.status != null) {
                    if (response.body()!!.status == Constants.SUCCESS) {
                        displayToast(response.body()!!.message)
                    } else {
                        displayToast(response.body()!!.message)
                    }
                } else {
                    Log.d("Status API", "Null ")
                }
            }

            override fun onFailure(call: Call<OrderStatusResponse>, t: Throwable) {
            }

        })
    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Mobile Recharge"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                val intent=Intent(this,MainActivity::class.java)
                /* intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                   intent.putExtra("EXIT", true)*/
                startActivity(intent)
                finish()
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    protected override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result Code is -1 send from Payumoney activity
        Log.d("MainActivity", "request code $requestCode resultcode $resultCode")
        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data != null) {
            val transactionResponse = data.getParcelableExtra<TransactionResponse>(PayUmoneyFlowManager
                    .INTENT_EXTRA_TRANSACTION_RESPONSE)

            val resultModel = data.getParcelableExtra<ResultModel>(PayUmoneyFlowManager.ARG_RESULT)

            // Check which object is non-null
            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
                if (transactionResponse.transactionStatus == TransactionResponse.TransactionStatus.SUCCESSFUL) {
                    if(internetCheck(context!!))
                    {
                        orderStatusUpdateAPI("success", getTxtID(transactionResponse.getPayuResponse()))
                    }else{
                        displayToast(Constants.INTERNETCONNECTION)
                    }

                    AlertDialog.Builder(this)
                            .setCancelable(false)
                            .setTitle("Payment")
                            .setMessage("Transaction Success")
                            .setPositiveButton(android.R.string.ok)
                            { dialog, whichButton -> dialog.dismiss()
                                val intent=Intent(this,MainActivity::class.java)
                                startActivity(intent)
                                finish()
                                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
                            }.show()
                } else {
                  //  orderStatusUpdateAPI("success", getTxtID(transactionResponse.getPayuResponse()))
                    AlertDialog.Builder(this)
                        .setCancelable(false)
                            .setTitle("Payment")
                        .setMessage("Transaction failed")
                        .setPositiveButton(android.R.string.ok) { dialog, whichButton -> dialog.dismiss()
                         }.show()


                }
               getTxtID(transactionResponse.getPayuResponse())
                // Response from Payumoney
              /*  val payuResponse = transactionResponse.getPayuResponse()

                 Response from SURl and FURL
                val merchantResponse = transactionResponse.transactionDetails

                AlertDialog.Builder(this)
                        .setCancelable(false)
                        .setMessage("Payu's Data : $payuResponse\n\n\n Merchant's Data: $merchantResponse")
                        .setPositiveButton(android.R.string.ok) { dialog, whichButton -> dialog.dismiss() }.show()*/

            } else if (resultModel != null && resultModel.error != null) {
                Log.d("TAG", "Error response : " + resultModel.error.transactionResponse)
            } else {
                Log.d("TAG", "Both objects are null!")
            }
        }
    }

    private fun getTxtID(transactionDetails: String?): String {
        var StrPaymentId=""
        if (transactionDetails!=null) {
            try {
                val jsonObject = JSONObject(transactionDetails)
                val message = jsonObject.getString("message")
                val jsonObjectPaymentResult = jsonObject.getJSONObject("result");
                StrPaymentId = jsonObjectPaymentResult.getString("paymentId");
                Log.i("answer",transactionDetails)
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } else {
           // Toast.makeText(this, "" + response.body()!!.getMessage(), Toast.LENGTH_SHORT).show()
        }
return StrPaymentId
    }


    fun hashCal(str: String): String {
        val hashseq = str.toByteArray()
        val hexString = StringBuilder()
        try {
            val algorithm = MessageDigest.getInstance("SHA-512")
            algorithm.reset()
            algorithm.update(hashseq)
            val messageDigest = algorithm.digest()
            for (aMessageDigest in messageDigest) {
                val hex = Integer.toHexString(0xFF and aMessageDigest.toInt())
                if (hex.length == 1) {
                    hexString.append("0")
                }
                hexString.append(hex)
            }
        } catch (ignored: NoSuchAlgorithmException) {
        }

        return hexString.toString()
    }

    private fun launchPayUMoneyFlow(name: String, emaiL_ID: String, citY_ID: String, statE_ID: String, amounts: String, rechargE_TYPE: Int?, mobilE_NUMBER: String, operatoR_ID: String?, orderId: String, rechargeID: String) {

        val payUmoneyConfig = PayUmoneyConfig.getInstance()

        //Use this to set your custom text on result screen button
     //   payUmoneyConfig.doneButtonText = (findViewById(R.id.status_page_et) as EditText).text.toString()

        //Use this to set your custom title for the activity
  // payUmoneyConfig.payUmoneyActivityTitle = "TNOAPP"

        payUmoneyConfig.disableExitConfirmation(false)

        val builder = PayUmoneySdkInitializer.PaymentParam.Builder()

       /* var amount = 0.0
        try {
            amount = java.lang.Double.parseDouble(amounts)

        } catch (e: Exception) {
            e.printStackTrace()
        }*/

        val txnId = orderId
        val phone = mobilE_NUMBER
        val productName = "Android"
        val firstName = name
        val email = emaiL_ID
        val udf1 = ""
        val udf2 = ""
        val udf3 = ""
        val udf4 = ""
        val udf5 = ""
        val udf6 = ""
        val udf7 = ""
        val udf8 = ""
        val udf9 = ""
        val udf10 = ""

        builder.setAmount(amounts)
                .setTxnId(txnId)
                .setPhone(phone)
                .setProductName(productName)
                .setFirstName(firstName)
                .setEmail(email)
                .setsUrl(surl)
                .setfUrl(furl)
                .setUdf1(udf1)
                .setUdf2(udf2)
                .setUdf3(udf3)
                .setUdf4(udf4)
                .setUdf5(udf5)
                .setUdf6(udf6)
                .setUdf7(udf7)
                .setUdf8(udf8)
                .setUdf9(udf9)
                .setUdf10(udf10)
                .setIsDebug(false)
                .setKey(merchant_Key)
                .setMerchantId(merchant_ID)

        try {
            mPaymentParams = builder.build()

            /*
            * Hash should always be generated from your server side.
            * */
            // generateHashFromServer(mPaymentParams);

            /*            */
            /**
             * Do not use below code when going live
             * Below code is provided to generate hash from sdk.
             * It is recommended to generate hash from server side only.
             */
            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams!!)


                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, this@MobileRechargeActivity, R.style.AppTheme_default, true)

        } catch (e: Exception) {
            // some exception occurred
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()

        }

    }

    private fun calculateServerSideHashAndInitiatePayment1(paymentParam: PayUmoneySdkInitializer.PaymentParam): PayUmoneySdkInitializer.PaymentParam {

        val stringBuilder = StringBuilder()
        val params = paymentParam.params
        stringBuilder.append(params[PayUmoneyConstants.KEY] + "|")
        stringBuilder.append(params[PayUmoneyConstants.TXNID] + "|")
        stringBuilder.append(params[PayUmoneyConstants.AMOUNT] + "|")
        stringBuilder.append(params[PayUmoneyConstants.PRODUCT_INFO] + "|")
        stringBuilder.append(params[PayUmoneyConstants.FIRSTNAME] + "|")
        stringBuilder.append(params[PayUmoneyConstants.EMAIL] + "|")
        stringBuilder.append(params[PayUmoneyConstants.UDF1] + "|")
        stringBuilder.append(params[PayUmoneyConstants.UDF2] + "|")
        stringBuilder.append(params[PayUmoneyConstants.UDF3] + "|")
        stringBuilder.append(params[PayUmoneyConstants.UDF4] + "|")
        stringBuilder.append(params[PayUmoneyConstants.UDF5] + "||||||")
        stringBuilder.append(salt)

        val hash = hashCal(stringBuilder.toString())
        paymentParam.setMerchantHash(hash)

        return paymentParam
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent=Intent(this,MainActivity::class.java)
        /* intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
           intent.putExtra("EXIT", true)*/
        startActivity(intent)
        finish()
    }
}