package app.com.tnonline.ui.news

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.*
import app.com.tnonline.R
import app.com.tnonline.adapters.NewsAdapter
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.news.DataItem
import app.com.tnonline.data.remote.models.news.tabview.Response
import android.util.TypedValue
import android.graphics.Rect
import android.support.v4.view.ViewPager
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import app.com.tnonline.fragments.OneNewsFragment
import android.support.v4.app.FragmentPagerAdapter


class NewsActivity : AppCompatActivity() {


    var langFlag: String = ""

    private var tabLayout: TabLayout? = null
    private var viewPager: ViewPager? = null
    var back_button: ImageView? = null
    var pageTitle: TextView? = null
    //Fragment List
    private var mFragmentList = ArrayList<Fragment>()
    //Category List
    private var mCategorytList = ArrayList<String>()
    //Title List
    private var mFragmentCategoryTitleList = ArrayList<String>()
    private var adapter: ViewPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        langFlag = intent.extras.get("lang").toString()

        viewPager = findViewById(R.id.viewpager) as ViewPager
        back_button = findViewById(R.id.back_button) as ImageView
        pageTitle = findViewById(R.id.page_title) as TextView

        if(langFlag.equals("Tamil")){

            val call = APIFactory.getService().getLocalTamilNewCategoryFetch()

            call.enqueue(object : retrofit2.Callback<Response> {
                override fun onResponse(call: retrofit2.Call<Response>, response: retrofit2.Response<Response>) {
                    if (response.body() != null) {
                        var stringArrayList: List<String> = response.body()!!.data!!.category as List<String>

                        for(i in 0..stringArrayList.size - 1){
                            mFragmentCategoryTitleList.add(stringArrayList[i])
                        }

                        for (i in 0 until mFragmentCategoryTitleList.size) {
                            val args = Bundle()
                            args.putString("lang", langFlag)
                            args.putString("title", mFragmentCategoryTitleList[i])
                            args.putString("category", mFragmentCategoryTitleList[i])

                            val fragment = OneNewsFragment()
                            fragment.setArguments(args)
                            mFragmentList.add(fragment)
                        }

                        setupViewPager(viewPager!!)

                        tabLayout = findViewById<TabLayout>(R.id.tabs)
                        tabLayout!!.setupWithViewPager(viewPager)
                        // Tab ViewPager setting
                        viewPager!!.setOffscreenPageLimit(mFragmentList.size)
                        tabLayout!!.setupWithViewPager(viewPager)
                    } else {
                        // Error
                    }
                }

                override fun onFailure(call: retrofit2.Call<Response>, t: Throwable) {

                }
            })

            pageTitle!!.setText("தமிழ் செய்திகள்")
//            mFragmentCategoryTitleList.add("செய்திகள்")
//            mFragmentCategoryTitleList.add("விளையாட்டு")
//            mFragmentCategoryTitleList.add("சினிமா")
//            mFragmentCategoryTitleList.add("கலை&கலாச்சாரம்")
//            mFragmentCategoryTitleList.add("தொழில்நுட்பம்")
//            //mFragmentCategoryTitleList.add("அரசியல்")
//            //mFragmentCategoryTitleList.add("வர்த்தகம்")
//            mFragmentCategoryTitleList.add("பயண")
//            mFragmentCategoryTitleList.add("கல்வி")
//            mFragmentCategoryTitleList.add("உலகம்")
        }else{

            val call = APIFactory.getService().getLocalEnglishNewCategoryFetch()

            call.enqueue(object : retrofit2.Callback<Response> {
                override fun onResponse(call: retrofit2.Call<Response>, response: retrofit2.Response<Response>) {
                    if (response.body() != null) {
                        var stringArrayList: List<String> = response.body()!!.data!!.category as List<String>

                        for(i in 0..stringArrayList.size - 1){
                            mFragmentCategoryTitleList.add(stringArrayList[i])
                        }
                        for (i in 0 until mFragmentCategoryTitleList.size) {
                            val args = Bundle()
                            args.putString("lang", langFlag)
                            args.putString("title", mFragmentCategoryTitleList[i].toString())
                            args.putString("category", mFragmentCategoryTitleList[i])

                            val fragment = OneNewsFragment()
                            fragment.setArguments(args)
                            mFragmentList.add(fragment)
                        }

                        setupViewPager(viewPager!!)

                        tabLayout = findViewById<TabLayout>(R.id.tabs)
                        tabLayout!!.setupWithViewPager(viewPager)
                        // Tab ViewPager setting
                        viewPager!!.setOffscreenPageLimit(mFragmentList.size)
                        tabLayout!!.setupWithViewPager(viewPager)
                    } else {
                        // Error
                    }
                }

                override fun onFailure(call: retrofit2.Call<Response>, t: Throwable) {

                }
            })

            pageTitle!!.setText("English News")
//            mFragmentCategoryTitleList.add("HOME")
//            mFragmentCategoryTitleList.add("SPORTS")
//            mFragmentCategoryTitleList.add("MOVIES")
//            mFragmentCategoryTitleList.add("LIFESTYLE")
//            mFragmentCategoryTitleList.add("TECHNOLOGY")
//            mFragmentCategoryTitleList.add("TRAVEL")
//            mFragmentCategoryTitleList.add("EDUCATION")
//            mFragmentCategoryTitleList.add("INTERNATIONAL")
        }

        mCategorytList.add("popular")
        mCategorytList.add("Sports")
        mCategorytList.add("Entertainment")
        mCategorytList.add("Lifestyle")
        mCategorytList.add("Technology")
        mCategorytList.add("Travel")
        mCategorytList.add("Education")
        mCategorytList.add("International")


        for (i in 0 until mFragmentCategoryTitleList.size) {
            val args = Bundle()
            args.putString("lang", langFlag)
            args.putString("title", mFragmentCategoryTitleList[i].toString())
            args.putString("category", mCategorytList[i])

            val fragment = OneNewsFragment()
            fragment.setArguments(args)
            mFragmentList.add(fragment)
        }

        setupViewPager(viewPager!!)

        tabLayout = findViewById<TabLayout>(R.id.tabs)
        tabLayout!!.setupWithViewPager(viewPager)
        // Tab ViewPager setting
        viewPager!!.setOffscreenPageLimit(mFragmentList.size)
        tabLayout!!.setupWithViewPager(viewPager)

        /////////////////////////////////////////////////
        back_button!!.setOnClickListener {
            onBackPressed()
        }

        //////////////////////////////////////////////////
    }

    private fun setupViewPager(viewPager: ViewPager) {
        adapter = ViewPagerAdapter(supportFragmentManager, mFragmentList, mFragmentCategoryTitleList)
        viewPager.adapter = adapter
    }

    //ViewPagerAdapter settings
    internal inner class ViewPagerAdapter(fm: FragmentManager, fragments: List<Fragment>, titleLists: List<String>) : FragmentPagerAdapter(fm) {
        private var mFragmentList = ArrayList<Fragment>()
        private var mFragmentTitleList = ArrayList<String>()

        init {
            this.mFragmentList = fragments as ArrayList<Fragment>
            this.mFragmentTitleList = titleLists as ArrayList<String>
        }

        override fun getItem(position: Int): Fragment {
            return mFragmentList!!.get(position)
        }

        override fun getCount(): Int {
            return if (mFragmentList == null) 0 else mFragmentList!!.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList.get(position)
        }

    }


    inner class GridSpacingItemDecoration(private val spanCount: Int, private val spacing: Int, private val includeEdge: Boolean) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private fun dpToPx(dp: Int): Int {
        val r = resources
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics))
    }

}

