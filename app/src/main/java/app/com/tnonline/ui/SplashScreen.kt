package app.com.tnonline.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import app.com.tnonline.R
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Handler
import android.widget.TextView
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.utils.Constants


class SplashScreen : AppCompatActivity() {

    private val appPreferences = SharedPreferences()
    var loginValue: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activty_splash_screen)

        loginValue = appPreferences.getString(Constants.LOGIN_SUCCESS)
        var versionName = ""
        try {
            versionName = applicationContext.packageManager.getPackageInfo(applicationContext.packageName, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }


        val versionname = findViewById<TextView>(R.id.txt_version)
        versionname.text = versionName

        Handler().postDelayed({
            /* Create an Intent that will start the Menu-Activity. */

            /*if (!loginValue.equals(Constants.YES, true)) {
                val mainIntent = Intent(this@SplashScreen, SkipLoginActivity::class.java)
                startActivity(mainIntent)
                finish()
            } else {
                val mainIntent = Intent(this@SplashScreen, MainActivity::class.java)
                startActivity(mainIntent)
                finish()
            }*/

            val mainIntent = Intent(this@SplashScreen, WelcomeActivity::class.java)
            startActivity(mainIntent)
            finish()


        }, 2000)
    }

}