package app.com.tnonline.ui.events

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import app.com.tnonline.R
import app.com.tnonline.data.local.SingleTon
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Singleton
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_all_event_details.*
import kotlinx.android.synthetic.main.activity_all_event_details.collapsing_toolbar
import kotlinx.android.synthetic.main.activity_all_event_details.header_image
import kotlinx.android.synthetic.main.activity_all_event_details.location_iv
import kotlinx.android.synthetic.main.activity_all_event_details.txtAbout
import kotlinx.android.synthetic.main.activity_all_event_details.txtAddress
import kotlinx.android.synthetic.main.activity_all_event_details.txtName
import kotlinx.android.synthetic.main.activity_all_event_details.txtcontact_no
import kotlinx.android.synthetic.main.activity_all_event_details.txtemail
import kotlinx.android.synthetic.main.activity_dactor_details.*


class AllEventDetailsActivity : BaseActivity(), View.OnClickListener {

    override val layoutResId: Int
        get() = R.layout.activity_all_event_details


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val toolbar = findViewById<Toolbar>(app.com.tnonline.R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        Glide.with(this)
                .load(Singleton.headerImage)
                .into(header_image)
        txtName.text = Singleton.nameOfHall
        txtAddress.text = Singleton.address
        txtcontact_no.text = "+91" + Singleton.contact
        txtemail.text = Singleton.email
        txtAbout.text = Singleton.description
        location_iv.setOnClickListener(this)
        txtcontact_no.setOnClickListener(this)
        txtemail.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.location_iv -> {
                Singleton.address?.let { loadNavigationView(it) }
            }
            R.id.txtcontact_no -> {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:"+txtcontact_no.text)
                startActivity(intent)
            }
            R.id.txtemail->{
                val intent = Intent(Intent.ACTION_SENDTO) // it's not ACTION_SEND
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email")
                intent.putExtra(Intent.EXTRA_TEXT, "Body of email")
                intent.data = Uri.parse("mailto:"+txtemail.text) // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent)
            }
        }
    }


    fun loadNavigationView(address: String) {
        val mapUri = Uri.parse("geo:0,0?q=" + Uri.encode(address))
        val mapIntent = Intent(Intent.ACTION_VIEW, mapUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }
}
