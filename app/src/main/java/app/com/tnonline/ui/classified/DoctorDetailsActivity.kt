package app.com.tnonline.ui.classified

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.Toolbar
import app.com.tnonline.R
import app.com.tnonline.data.local.SingleTon
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_dactor_details.*
import android.content.Intent
import android.net.Uri
import android.content.pm.ResolveInfo
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat.startActivity
import android.Manifest.permission
import android.Manifest.permission.CALL_PHONE
import android.support.v4.app.ActivityCompat
import android.widget.Toast


class DoctorDetailsActivity : AppCompatActivity() {

    private var mContext: Context?=null

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(app.com.tnonline.R.layout.activity_dactor_details)


        mContext = this

        val toolbar = findViewById<Toolbar>(app.com.tnonline.R.id.toolbar)
        setSupportActionBar(toolbar)

        toolbar.title = SingleTon.instance.doctorDetailsList.company_name

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)

            collapsing_toolbar.title = SingleTon.instance.doctorDetailsList.company_name
        }

        txtHospitalName.text =  SingleTon.instance.doctorDetailsList.company_name
        haspitalAddress.text =  SingleTon.instance.doctorDetailsList.address
        txtDoctorName.text =  SingleTon.instance.doctorDetailsList.host_name
        txtDegree.text =  SingleTon.instance.doctorDetailsList.educational_qualification
        txtDateShow.text =  SingleTon.instance.doctorDetailsList.appointments
        txtPhoneNumber.text =  SingleTon.instance.doctorDetailsList.contact_no
        txtPhoneNumber.text =  SingleTon.instance.doctorDetailsList.contact_no //+" | "+ SingleTon.instance.doctorDetailsList.host_email
        txtMailAddress.text = SingleTon.instance.doctorDetailsList.host_email
        txtAbout.text =  SingleTon.instance.doctorDetailsList.description

        val specialityStr =SingleTon.instance.doctorDetailsList.specialist_in?.replace(","," | ")

        txtSpecalist.text =  specialityStr

        val circularProgressDrawable = CircularProgressDrawable(mContext!!)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        val image =SingleTon.instance.doctorDetailsList.image

        if(image?.isEmpty()!!){
            Glide.with(mContext!!)
                    .load(R.drawable.no_image_icon)
                    .apply(RequestOptions().placeholder( circularProgressDrawable))
                    .into(header_image)
        }else{
            Glide.with(mContext!!)
                    .load(image)
                    .apply(RequestOptions().placeholder( circularProgressDrawable))
                    .into(header_image)
        }

        txtPhoneNumber.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:"+SingleTon.instance.doctorDetailsList.contact_no.toString())
                startActivity(callIntent)
            }else{
                val PERMISSION_ALL = 1
                Toast.makeText(applicationContext, "Please Enable the permission for Calling in you Setting Permissions.",Toast.LENGTH_SHORT).show()
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), PERMISSION_ALL)
            }
        }

        txtMailAddress.setOnClickListener {
            val intent = Intent(android.content.Intent.ACTION_SEND)
            val recipients = arrayOf(SingleTon.instance.doctorDetailsList.host_email)
            intent.putExtra(Intent.EXTRA_EMAIL, recipients)
            intent.type = "text/html"
            val pm = packageManager
            val matches = pm.queryIntentActivities(intent, 0)
            var best: ResolveInfo? = null
            for (info in matches)
                if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                    best = info
            if (best != null)
                intent.setClassName(best.activityInfo.packageName, best.activityInfo.name)

            startActivity(intent)
        }
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

    }
}
