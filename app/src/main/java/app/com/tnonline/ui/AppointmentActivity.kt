package app.com.tnonline.ui

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.View
import app.com.tnonline.R
import app.com.tnonline.utils.BaseActivity
import java.text.SimpleDateFormat
import java.util.*
import android.widget.DatePicker
import app.com.tnonline.data.local.SingleTon
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.appoinment.AppoinmentRequest
import app.com.tnonline.data.remote.models.appoinment.AppoinmentResponse
import app.com.tnonline.utils.Constants
import app.com.tnonline.utils.Utilities
import app.com.tnonline.utils.Utilities.isValidEmail
import kotlinx.android.synthetic.main.activity_appointment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.app.DatePickerDialog.OnDateSetListener
import android.widget.TimePicker
import kotlinx.android.synthetic.main.activity_appointment.btn_submit
import kotlinx.android.synthetic.main.activity_appointment.et_comment
import kotlinx.android.synthetic.main.activity_appointment.et_email
import kotlinx.android.synthetic.main.activity_appointment.et_mobile
import kotlinx.android.synthetic.main.activity_appointment.et_name
import kotlinx.android.synthetic.main.activity_appointment.root_layout
import kotlinx.android.synthetic.main.activity_appointment.tvDate
import kotlinx.android.synthetic.main.activity_appointment.tvTime
import kotlinx.android.synthetic.main.activity_dine_in_appoinment.*
import java.lang.Exception


class AppointmentActivity : BaseActivity(), View.OnClickListener {

    var mDate: String = ""
    var mMonth: String = ""
    var mMonthNum: String = ""
    var mYear: String = ""

    val myCalendar = Calendar.getInstance()
    var date: DatePickerDialog.OnDateSetListener? = null
    var timeSetListener: TimePickerDialog.OnTimeSetListener? = null
    override val layoutResId: Int
        get() = R.layout.activity_appointment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


       // setupActionBar()
        inItViews()


    }

    private fun inItViews() {
        tvDate.setOnClickListener(this)
        btn_submit.setOnClickListener(this)
        tvTime.setOnClickListener(this)
        root_layout.setOnClickListener(this)
        date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val myFormat = "dd/MM/yy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)

            tvDate.text = sdf.format(myCalendar.time)
        }

        timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            myCalendar.set(Calendar.HOUR_OF_DAY, hour)
            myCalendar.set(Calendar.MINUTE, minute)

            tvTime.text = SimpleDateFormat("HH:mm").format(myCalendar.time)
        }


    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.btn_submit -> {
                validation()
            }

            R.id.tvDate -> {
                showCalenderDialog()
            }

            R.id.root_layout->{
                finish()
            }
            R.id.tvTime -> {

                // TimePickerDialog(applicationContext, timeSetListener, myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), true).show()

                var hour = myCalendar.get(Calendar.HOUR_OF_DAY)
                var minutes = myCalendar.get(Calendar.MINUTE)

                var mTimePicker: TimePickerDialog



                mTimePicker= TimePickerDialog(this,
                        TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                            hour = hourOfDay
                            minutes = minute

                            var timeSet = ""
                            if (hour > 12) {
                                hour -= 12
                                timeSet = "PM"
                            } else if (hour === 0) {
                                hour += 12
                                timeSet = "AM"
                            } else if (hour === 12) {
                                timeSet = "PM"
                            } else {
                                timeSet = "AM"
                            }

                            var min = ""
                            if (minutes < 10)
                                min = "0$minutes"
                            else
                                min = minutes.toString()

                            // Append in a StringBuilder
                            val aTime = StringBuilder().append(hour).append(':')
                                    .append(min).append(" ").append(timeSet).toString()


                            //tvTime.text = hourOfDay.toString()+ ":" + minute
                            tvTime.text = aTime
                        }, hour, minutes, false)

                mTimePicker.show()
            }
            R.id.goBackOnPress -> {
                onBackPressed()
            }
        }


    }

    private fun validation() {

        if (et_name.text.toString().trim() == "") {
            //displayToast("Please Enter your name")
            et_name.setError("Please Enter your name")
        } else if (et_mobile.text.toString().trim() == "") {
            //displayToast("Please Enter your mobile number")
            et_mobile.setError("Please Enter your mobile number")
        } else if (et_mobile.text.toString().trim().length != 10) {
            //displayToast("Mobile number must be 10 digit")
            et_mobile.setError("Mobile number must be 10 digit")
        } else if (!isValidEmail(et_email.text.toString().trim())) {
            //displayToast("Please Enter valid email id")
            et_email.setError("Please Enter valid email id")
        } else {
            callApi()
        }

    }

    private fun callApi() {
      //  showProgressBar(llRoot)
        val request = AppoinmentRequest()
        request.classifieds_id = SingleTon.instance.classified_id
        request.name = et_name.text.toString().trim()
        request.number = et_mobile.text.toString().trim()
        request.email = et_email.text.toString().trim()
        request.appointment_date = tvDate.text.toString().trim()
        request.appointment_time = tvTime.text.toString()
        request.comments = et_comment.text.toString().trim()
        request.professionals = 1315

        val call = APIFactory.getService().appoinmentApi(request)
        call.enqueue(object : Callback<AppoinmentResponse> {
            override fun onFailure(call: Call<AppoinmentResponse>?, t: Throwable?) {
                dismissProgressBar()
                displayToast(t.toString())
            }

            override fun onResponse(call: Call<AppoinmentResponse>?, response: Response<AppoinmentResponse>?) {
                dismissProgressBar()
                if (response != null) {
                    try {
                        if (response.body()!!.status.equals(Constants.SUCCESS)) {
                            displayToast(response.body()?.msg.toString())
                            onBackPressed()
                        } else if (response.body()!!.status.equals(Constants.ERROR)) {
                            dismissProgressBar()
                            displayToast("failure")
                            onBackPressed()
                        }
                    }catch (e: Exception){
                        onBackPressed()
                    }
                }

            }

        })

    }

    private fun showCalenderDialog() {

        DatePickerDialog(this@AppointmentActivity,
                date,
                // set DatePickerDialog to point to today's date when it loads up
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show()

    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        /* actionBar!!.setBackgroundDrawable( ColorDrawable(resources.getColor(R.color.adap)))
         actionBar.setDisplayShowTitleEnabled(false);
         actionBar.setDisplayShowTitleEnabled(true);*/
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Online Appointment"
    }
}
