package app.com.tnonline.ui.infosection.train

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import app.com.tnonline.R
import app.com.tnonline.adapters.TrainArrivalsAdapter
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.train.arrival.arrival.TrainArrivalsResponse
import app.com.tnonline.data.remote.models.train.arrival.trainlist.TrainsItem
import app.com.tnonline.ui.MainActivity
import app.com.tnonline.ui.SettingsActivity
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import kotlinx.android.synthetic.main.activity_train_arrivals.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TrainArrivalsActivity : BaseActivity() {
    var trainArrival = ArrayList<TrainsItem>()
    var adapter: TrainArrivalsAdapter? = null
    private val appPreferences = SharedPreferences()
    override val layoutResId: Int
        get() = R.layout.activity_train_arrivals

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupActionBar()
        listOfTrainArrivalAPI()
    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        if (appPreferences.getString(Constants.STATION_CODE).isEmpty()) {
            title = "Train Arrivals " + "( " + getStringCode(appPreferences.getString(Constants.STATION_CODE)) + " )"
        } else {
            title = "Train Arrivals"
        }

    }

    private fun listOfTrainArrivalAPI() {
        showProgressBar(llParent)
        val stationCode = getStringCode(appPreferences.getString(Constants.STATION_CODE))

        Log.d("station_code", "fkjgflfgfgf " + appPreferences.getString(Constants.STATION_CODE))

        if (stationCode != null && stationCode != " ") {


            val call = APIFactory.getTrainService().apiTrainArrival(stationCode!!, Constants.TRAIN_KEY)

            call.enqueue(object : Callback<TrainArrivalsResponse> {
                override fun onResponse(call: Call<TrainArrivalsResponse>, response: Response<TrainArrivalsResponse>) {
                    if (response.body() != null) {
                        if (response.body()!!.responseCode == 200) {
                            dismissProgressBar()

                            if (response.body()!!.trains != null) {
                                trainArrival.addAll(response.body()!!.trains!!.filterNotNull())
                                /*for (i in 0 until response.body()!!.data!!.size) {
                                    flightList = response.body()!!.data!!

                                }*/
                                setupAdapter(trainArrival)
                            }

                            //   displayToast(response.body()!!.message!!)
                        } else {
                            dismissProgressBar()
                            settingsAlertShow()
                        }
                    } else {
                        displayToast("Error")
                        dismissProgressBar()
                    }

                }

                override fun onFailure(call: Call<TrainArrivalsResponse>, t: Throwable) {
                    dismissProgressBar()
                    displayToast(t.toString())
                }
            })
        } else {
            dismissProgressBar()
            settingsAlertShow()
        }
    }

    private fun settingsAlertShow() {
        AlertDialog.Builder(this@TrainArrivalsActivity)
                .setCancelable(false)
                .setTitle("Train Arrivals")
                .setMessage("Station code not Set, Kindly visit settings")
                .setPositiveButton(android.R.string.ok)
                { dialog, whichButton ->
                    dialog.dismiss()

                    startActivity(Intent(this@TrainArrivalsActivity, SettingsActivity::class.java))
                    finish()
                    overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
                }.show()
    }

    private fun setupAdapter(trainArrival: ArrayList<TrainsItem>) {


        adapter = TrainArrivalsAdapter(this@TrainArrivalsActivity, trainArrival)
        { view: View, dataItems: TrainsItem, i: Int ->
            val TraiNumber: String = dataItems.number.toString()
            startActivity(Intent(this@TrainArrivalsActivity, MainActivity::class.java))
        }

        recyclerViewTrainArrival.layoutManager = LinearLayoutManager(this@TrainArrivalsActivity, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        recyclerViewTrainArrival.adapter = adapter
        recyclerViewTrainArrival.adapter!!.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
