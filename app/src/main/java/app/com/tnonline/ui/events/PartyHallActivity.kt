package app.com.tnonline.ui.events

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.Window
import android.widget.*
import app.com.tnonline.R
import app.com.tnonline.adapters.PaginationScrollListener
import app.com.tnonline.adapters.PartyHallAdapter
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.events.EventOrganizerResponse
import app.com.tnonline.data.remote.models.events.EventSupplyResponse
import app.com.tnonline.data.remote.models.events.LocalEventResponse
import app.com.tnonline.data.remote.models.events.PartyHallResponse
import app.com.tnonline.data.remote.models.locality.LocalityDataResponse
import app.com.tnonline.data.remote.models.locality.LocalityRequest
import app.com.tnonline.data.test.PartHallModel
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import app.com.tnonline.utils.Singleton
import kotlinx.android.synthetic.main.activity_non_veg_restaurant.*
import kotlinx.android.synthetic.main.activity_party_hall.*
import kotlinx.android.synthetic.main.activity_party_hall.filter_button
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PartyHallActivity : BaseActivity(),View.OnClickListener {

    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    var mCurrentPageNo = 0
    var mTotalPageSize = 0
    var mNextPageNo = 0
    var mLastPageNo = 0

    var filter_doctor_dialog: Dialog? = null
    var update_or_refresh_list : TextView? = null
    var close_doctor_dialog : ImageView? = null
    var localityButton : TextView? = null
    var specilityButton : TextView? = null
    var dialogFlag : Int = 0
    var locality_layout : LinearLayout? = null
    var specialist_layout : LinearLayout? = null
    var locality_list : ListView? = null
    var specialist_list : ListView? = null
    var localityID_flag: Int = 0
    var catId: Int = 0
    private var localityList_name = java.util.ArrayList<String>()
    private var localityList_id = java.util.ArrayList<Int>()
    var localityStr:String?=null
    var cityIdStr:String?=null
    var stateIdStr:String?=null
    var eventTitle:String?=null
    override val layoutResId: Int
        get() = R.layout.activity_party_hall

    var partyItems = ArrayList<PartHallModel>()
    var adapter: PartyHallAdapter? = null
    private val appPreferences = SharedPreferences()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        back_button.setOnClickListener(this)
        stateIdStr =  appPreferences.getString(Constants.STATE_ID)
        cityIdStr =  appPreferences.getString(Constants.CITY_ID)
        localityStr =  appPreferences.getString(Constants.CITY_ID)
        filter_button.setOnClickListener(this)
        createFilterDialog()
        callLocalityApi()
        val title: String = intent.extras["section"].toString()
        eventTitle=title
        if (title.equals(Constants.PARTY_HALL)) {
            partyItems.clear()
            types_header.visibility = View.VISIBLE
            callPartyHallApi(7)
        } else if (title.equals(Constants.LocalEvent)) {
            partyItems.clear()
            types_header.visibility = View.GONE
            callLocalEventApi(7)
        } else if (title.equals(Constants.EVENT_ORGE)) {
            partyItems.clear()
            types_header.visibility = View.GONE
            callEventOrganizerApi(7)
        } else if (title.equals(Constants.EVENT_SUPP)) {
            partyItems.clear()
            types_header.visibility = View.GONE
            callEventSupplyApi(7)
        }

        setPagination()
    }


    private fun callEventOrganizerApi(localityId:Int) {
        val call = APIFactory.getService().getEventOrganizerList(appPreferences.getString(Constants.CITY_ID),localityId)
        call.enqueue(object : retrofit2.Callback<EventOrganizerResponse> {
            override fun onResponse(call: Call<EventOrganizerResponse>, response: retrofit2.Response<EventOrganizerResponse>) {
                if (response.body() != null) {
                    if (response.body()!!.data != null) {
                        for (i in 0 until response.body()!!.data!!.data!!.size) {
                            val image = response.body()!!.data!!.data!![i].imageName
                            val hallName = response.body()!!.data!!.data!![i].eventTitle
                            val address = response.body()!!.data!!.data!![i].address
                            val eventType = ArrayList<String>()
                            val cateringFeature = ArrayList<String>()
                            val cateringCuisine = ArrayList<String>()
                            val tentType = ArrayList<String>()
                            val tableType = ArrayList<String>()
                            val venueList = ArrayList<String>()
                            val facility = ArrayList<String>()
                            val contact = response.body()!!.data!!.data!![i].contactNo
                            val email = response.body()!!.data!!.data!![i].email
                            val description = response.body()!!.data!!.data!![i].eventDescription
                            for (Data in response.body()!!.data!!.data!![i].eventType!!) {
                                eventType.add(Data)
                            }
                            for (Data in response.body()!!.data!!.data!![i].cateringFeatures!!) {
                                cateringFeature.add(Data)
                            }
                            for (Data in response.body()!!.data!!.data!![i].cateringCusine!!) {
                                cateringCuisine.add(Data)
                            }

                            val item1 = PartHallModel(image!!, hallName!!, address!!, "3", contact, email, null, description, venueList, facility, eventType, cateringFeature, cateringCuisine, tableType, tentType)
                            partyItems.add(item1)
                        }
                        PartyHallAdapter(partyItems)
                    }else{
                        PartyHallAdapter(partyItems)
                    }
                } else {
                    // Error
                }
            }

            override fun onFailure(call: Call<EventOrganizerResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })

    }

    private fun callEventSupplyApi(localityId:Int) {
        val call = APIFactory.getService().getEventSuppliesList(appPreferences.getString(Constants.CITY_ID),localityId)
        call.enqueue(object : retrofit2.Callback<EventSupplyResponse> {
            override fun onResponse(call: Call<EventSupplyResponse>, response: retrofit2.Response<EventSupplyResponse>) {
                if (response.body() != null) {
                    if (response.body()!!.data != null) {
                        for (i in 0 until response.body()!!.data!!.data!!.size) {
                            val image = response.body()!!.data!!.data!![i].imageName
                            val hallName = response.body()!!.data!!.data!![i].companyName
                            val address = response.body()!!.data!!.data!![i].address
                            val venueList = ArrayList<String>()
                            val facility = ArrayList<String>()
                            val eventType = ArrayList<String>()
                            val cateringFeature = ArrayList<String>()
                            val cateringCuisine = ArrayList<String>()
                            val tentType = ArrayList<String>()
                            val tableType = ArrayList<String>()
                            val contact = response.body()!!.data!!.data!![i].contactNo
                            val email = response.body()!!.data!!.data!![i].email
                            val description = response.body()!!.data!!.data!![i].description
                            for (Data in response.body()!!.data!!.data!![i].tentType!!) {
                                tentType.add(Data)
                            }
                            for (Data in response.body()!!.data!!.data!![i].tableType!!) {
                                tableType.add(Data)
                            }
                            val item1 = PartHallModel(image!!, hallName!!, address!!, "4", contact, email, null, description, venueList, facility, eventType, cateringFeature, cateringCuisine, tableType, tentType)
                            partyItems.add(item1)
                        }
                        PartyHallAdapter(partyItems)
                    }else{
                        PartyHallAdapter(partyItems)
                    }
                } else {
                    // Error
                }
            }

            override fun onFailure(call: Call<EventSupplyResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })

    }

    private fun callLocalEventApi(localityId:Int) {
        val call = APIFactory.getService().getLocalEventList(appPreferences.getString(Constants.CITY_ID),localityId)
        call.enqueue(object : retrofit2.Callback<LocalEventResponse> {
            override fun onResponse(call: Call<LocalEventResponse>, response: retrofit2.Response<LocalEventResponse>) {
                if (response.body() != null) {
                    if (response.body()!!.data != null) {
                        for (i in 0 until response.body()!!.data!!.data!!.size) {
                            val image = response.body()!!.data!!.data!![i].coverImage
                            val hallName = response.body()!!.data!!.data!![i].titleOfEvent
                            val address = response.body()!!.data!!.data!![i].address1
                            val venueList = ArrayList<String>()
                            val facility = ArrayList<String>()
                            val eventType = ArrayList<String>()
                            val cateringFeature = ArrayList<String>()
                            val cateringCuisine = ArrayList<String>()
                            val tentType = ArrayList<String>()
                            val tableType = ArrayList<String>()
                            val contact = response.body()!!.data!!.data!![i].hostMobileNo
                            val email = response.body()!!.data!!.data!![i].hostEmail
                            val description = response.body()!!.data!!.data!![i].eventDescription
                            val item1 = PartHallModel(image, hallName!!, address!!, "1", contact, email, null, description, venueList, facility, eventType, cateringFeature, cateringCuisine, tableType, tentType)
                            partyItems.add(item1)
                        }
                        PartyHallAdapter(partyItems)
                    }else{
                        PartyHallAdapter(partyItems)
                    }
                } else {
                    // Error
                }
            }

            override fun onFailure(call: Call<LocalEventResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })

    }

    private fun callPartyHallApi(localityId:Int) {
        val call = APIFactory.getService().getParyhallList(appPreferences.getString(Constants.CITY_ID), localityId,"1")
        call.enqueue(object : retrofit2.Callback<PartyHallResponse> {
            override fun onResponse(call: Call<PartyHallResponse>, response: retrofit2.Response<PartyHallResponse>) {
                if (response.body() != null) {
                    if (response.body()!!.data != null) {
                        mCurrentPageNo = response.body()!!.data!!.currentPage!!
                        for (i in 0 until response.body()!!.data!!.data!!.size) {
                            val image = response.body()!!.data!!.data!![i].imageName
                            val hallName = response.body()!!.data!!.data!![i].nameOfHall
                            val address = response.body()!!.data!!.data!![i].adrress
                            val contact = response.body()!!.data!!.data!![i].userMobile
                            val email = response.body()!!.data!!.data!![i].userEmail
                            val capacity = response.body()!!.data!!.data!![i].capacity
                            val description = response.body()!!.data!!.data!![i].description
                            val venueList = ArrayList<String>()
                            val facility = ArrayList<String>()
                            val eventType = ArrayList<String>()
                            val cateringFeature = ArrayList<String>()
                            val cateringCuisine = ArrayList<String>()
                            val tentType = ArrayList<String>()
                            val tableType = ArrayList<String>()
                            for (Data in response.body()!!.data!!.data!![i].venueConfiguration!!) {
                                venueList.add(Data)
                            }
                            for (Data in response.body()!!.data!!.data!![i].facility!!) {
                                facility.add(Data)
                            }

                            mTotalPageSize = response.body()!!.data!!.lastPage!!
                            mNextPageNo = mCurrentPageNo + 1
                            mLastPageNo = response.body()!!.data!!.lastPage!!
                            val item1 = PartHallModel(image!!, hallName!!, address!!, "2", contact!!, email!!, capacity!!, description!!, venueList, facility, eventType, cateringFeature, cateringCuisine, tableType, tentType)
                            partyItems.add(item1)
                        }
                        PartyHallAdapter(partyItems)
                        //testAdapater(response.body()!!.data!!.data!!)
                    }else{
                        PartyHallAdapter(partyItems)
                    }
                } else {
                    // Error
                }
            }

            override fun onFailure(call: Call<PartyHallResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })

    }

    private fun PartyHallAdapter(partyItems: List<PartHallModel>) {
        adapter = PartyHallAdapter(this@PartyHallActivity, partyItems as ArrayList<PartHallModel>) { view: View, dataItems: PartHallModel, i: Int ->
            //startActivity(Intent(this, RealEstateDetailsActivity::class.java))
            when (view.id) {
                R.id.cardView -> {
                    if (partyItems[i].type == "3") {
                        Singleton.facility = partyItems[i].facilities
                        Singleton.venue = partyItems[i].venueType
                        Singleton.eventType = partyItems[i].eventType
                        Singleton.cateringFeature = partyItems[i].cateringFeature
                        Singleton.cateringCuisine = partyItems[i].cateringCuisine
                        Singleton.headerImage = partyItems[i].image
                        Singleton.nameOfHall = partyItems[i].name
                        Singleton.address = partyItems[i].address
                        Singleton.contact = partyItems[i].contact
                        Singleton.email = partyItems[i].email
                        Singleton.capacity = partyItems[i].capacity
                        Singleton.description = partyItems[i].description
                        startActivity(Intent(this@PartyHallActivity, EventOrganizerDetailsActivity::class.java))
                        adapter?.notifyDataSetChanged()

                    } else if (partyItems[i].type == "4") {
                        Singleton.facility = partyItems[i].facilities
                        Singleton.tentType = partyItems[i].tentType
                        Singleton.tableType = partyItems[i].tableType
                        Singleton.headerImage = partyItems[i].image
                        Singleton.nameOfHall = partyItems[i].name
                        Singleton.address = partyItems[i].address
                        Singleton.contact = partyItems[i].contact
                        Singleton.email = partyItems[i].email
                        Singleton.capacity = partyItems[i].capacity
                        Singleton.description = partyItems[i].description
                        startActivity(Intent(this@PartyHallActivity, EventSuppliesDetailsActivity::class.java))
                        adapter?.notifyDataSetChanged()

                    } else if (partyItems[i].type == "1") {
                        Singleton.facility = partyItems[i].facilities
                        Singleton.tentType = partyItems[i].tentType
                        Singleton.tableType = partyItems[i].tableType
                        Singleton.headerImage = partyItems[i].image
                        Singleton.nameOfHall = partyItems[i].name
                        Singleton.address = partyItems[i].address
                        Singleton.contact = partyItems[i].contact
                        Singleton.email = partyItems[i].email
                        Singleton.capacity = partyItems[i].capacity
                        Singleton.description = partyItems[i].description
                        startActivity(Intent(this@PartyHallActivity, AllEventDetailsActivity::class.java))
                        adapter?.notifyDataSetChanged()
                    } else {
                        Singleton.facility = partyItems[i].facilities
                        Singleton.venue = partyItems[i].venueType
                        Singleton.headerImage = partyItems[i].image
                        Singleton.nameOfHall = partyItems[i].name
                        Singleton.address = partyItems[i].address
                        Singleton.contact = partyItems[i].contact
                        Singleton.email = partyItems[i].email
                        Singleton.capacity = partyItems[i].capacity
                        Singleton.description = partyItems[i].description
                        if (partyItems[i].facilities.size > 0) {
                            startActivity(Intent(this@PartyHallActivity, EventDetailsActivity::class.java))
                            adapter?.notifyDataSetChanged()
                        } else {
                            Toast.makeText(this, "under development", Toast.LENGTH_SHORT).show()
                        }
                    }

                }

            }
        }
        val layoutManager = LinearLayoutManager(this@PartyHallActivity, LinearLayout.VERTICAL, false)
        party_hall_rv.layoutManager = layoutManager as RecyclerView.LayoutManager?
        party_hall_rv.adapter = adapter
        party_hall_rv.adapter!!.notifyDataSetChanged()
        party_hall_rv?.addOnScrollListener(object : PaginationScrollListener(layoutManager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                //isLoading = true
                //you have to call loadmore items to get more data
                // getMoreItems()
            }
        })

    }

    private fun getMoreItems() {
        isLoading = false
        val call = APIFactory.getService().getParyhallList(appPreferences.getString(Constants.CITY_ID),7, mNextPageNo.toString())
        call.enqueue(object : retrofit2.Callback<PartyHallResponse> {
            override fun onResponse(call: Call<PartyHallResponse>, response: retrofit2.Response<PartyHallResponse>) {
                if (response.body() != null) {
                    if (response.body()!!.data != null) {
                        mCurrentPageNo = response.body()!!.data!!.currentPage!!
                        for (i in 0 until response.body()!!.data!!.data!!.size) {
                            val image = response.body()!!.data!!.data!![i].imageName
                            val hallName = response.body()!!.data!!.data!![i].nameOfHall
                            val address = response.body()!!.data!!.data!![i].adrress
                            val contact = response.body()!!.data!!.data!![i].userMobile
                            val email = response.body()!!.data!!.data!![i].userEmail
                            val capacity = response.body()!!.data!!.data!![i].capacity
                            val description = response.body()!!.data!!.data!![i].description
                            val venueList = ArrayList<String>()
                            val facility = ArrayList<String>()
                            for (Data in response.body()!!.data!!.data!![i].venueConfiguration!!) {
                                venueList.add(Data)
                            }
                            for (Data in response.body()!!.data!!.data!![i].facility!!) {
                                facility.add(Data)
                            }

                            mTotalPageSize = response.body()!!.data!!.lastPage!!
                            mNextPageNo = mCurrentPageNo + 1
                            mLastPageNo = response.body()!!.data!!.lastPage!!
                            // val item1 = PartHallModel(image!!, hallName!!, address!!, "", contact!!, email!!, capacity!!, description!!, venueList, facility)
                            // partyItems.add(item1)
                        }
                        PartyHallAdapter(partyItems)
                        //testAdapater(response.body()!!.data!!.data!!)
                    }
                } else {
                    // Error
                }
            }

            override fun onFailure(call: Call<PartyHallResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })

    }



    private fun createFilterDialog() {
        filter_doctor_dialog = Dialog(this);
        filter_doctor_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE);
        filter_doctor_dialog!!.setCancelable(false);
        filter_doctor_dialog!!.setContentView(R.layout.filter_popup_restaurant);

        update_or_refresh_list = filter_doctor_dialog!!.findViewById(R.id.update_or_refresh_list) as TextView
        close_doctor_dialog = filter_doctor_dialog!!.findViewById(R.id.close_doctor_dialog) as ImageView
        localityButton = filter_doctor_dialog!!.findViewById(R.id.locality_button) as TextView
        locality_layout = filter_doctor_dialog!!.findViewById(R.id.locality_layout) as LinearLayout
        locality_list = filter_doctor_dialog!!.findViewById(R.id.locality_list) as ListView

        localityButton!!.setBackgroundColor(resources.getColor(R.color.green))
        localityButton!!.setTextColor(resources.getColor(R.color.white))


        localityButton!!.setOnClickListener {
            localityButton!!.setBackgroundColor(resources.getColor(R.color.green))
            localityButton!!.setTextColor(resources.getColor(R.color.white))

            specilityButton!!.setBackgroundColor(resources.getColor(R.color.youtube_light_gray))
            specilityButton!!.setTextColor(resources.getColor(R.color.textBlackcolor))
            specialist_layout!!.visibility = View.GONE
            locality_layout!!.visibility = View.VISIBLE
            dialogFlag = 0

        }


        update_or_refresh_list!!.setOnClickListener {
            //currentPage = 1;
            // display!!.text = " 01 "
            updateList()
        }

        close_doctor_dialog!!.setOnClickListener {
            filter_doctor_dialog!!.dismiss()
        }

        locality_list!!.setOnItemClickListener { parent, view, position, id ->
            if(localityList_name!!.size > 0){
                Toast.makeText(mContext, localityList_name[position].toString(), Toast.LENGTH_SHORT).show()
                localityID_flag = localityList_id[position]
            }
        }


    }

    private fun updateList() {
        close_doctor_dialog!!.performClick()
        callApiByLocationId(localityID_flag)
    }

    private fun callApiByLocationId(localityidFlag: Int) {
        if (eventTitle.equals(Constants.PARTY_HALL)) {
            partyItems.clear()
            callPartyHallApi(localityidFlag)
        } else if (eventTitle.equals(Constants.LocalEvent)) {
            partyItems.clear()
            callLocalEventApi(localityidFlag)
        } else if (eventTitle.equals(Constants.EVENT_ORGE)) {
            partyItems.clear()
            callEventOrganizerApi(localityidFlag)
        } else if (eventTitle.equals(Constants.EVENT_SUPP)) {
            partyItems.clear()
            callEventSupplyApi(localityidFlag)
        }
    }


    private fun callLocalityApi() {
        var request = LocalityRequest()
        request.city_id = cityIdStr?.toInt()!!
        request.state_id = stateIdStr?.toInt()!!
        request.category_id = 47

        val call = APIFactory.getService().localityApi(request)

        call.enqueue(object : Callback<LocalityDataResponse> {
            override fun onResponse(call: Call<LocalityDataResponse>, response: Response<LocalityDataResponse>) {
                if (response.body()!!.status!! == Constants.SUCCESS) {
                    dismissProgressBar()
                    if (response.body()!!.data != null) {
                        for (i in 0 until response.body()!!.data!!.size) {
                            localityList_name.add(response.body()?.data?.get(i)?.locality_name!!.toString())
                            localityList_id.add(response.body()?.data?.get(i)?.locality_id!!.toInt())
                        }
                        setLocalityListToAdapter(localityList_name)
                    }
                    //   displayToast(response.body()!!.message!!)
                } else {
                    displayToast("Error")
                }
            }
            override fun onFailure(call: Call<LocalityDataResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }


    private fun setLocalityListToAdapter(localityList: java.util.ArrayList<String>) {
        if(localityList != null){
            val adapter = ArrayAdapter<String>(this,
                    R.layout.text_row, R.id.spinner_text, localityList)
            locality_list!!.adapter = adapter
        }
    }


    private fun setPagination() {

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.back_button->{
                onBackPressed()
            }
            R.id.filter_button->{
                filter_doctor_dialog!!.show();
            }
        }
    }


}
