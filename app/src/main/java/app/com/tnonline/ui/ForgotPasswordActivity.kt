package app.com.tnonline.ui

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.MenuItem
import app.com.tnonline.R
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.forgotpassword.ForgotPasswordRequest
import app.com.tnonline.data.remote.models.forgotpassword.ForgotPasswordResponse
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import app.com.tnonline.utils.Utilities
import kotlinx.android.synthetic.main.activity_forgot_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ForgotPasswordActivity : BaseActivity() {
    override val layoutResId: Int
        get() = R.layout.activity_forgot_password

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()
        btnSendForgotPassword.setOnClickListener {
            if (layoutForgotpasswordEmail.text.toString().trim() == "") {
                displayToast("Please Enter Mail ID")
            } else if (!Utilities.isValidEmail(layoutForgotpasswordEmail.text.toString().trim())) {
                displayToast("Please Enter valid Email Id")
            } else {
                forgotPasswordAPI()
            }
        }


        


    }


    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Forgot Password"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun forgotPasswordAPI() {

        val forgotPasswordRequest = ForgotPasswordRequest()
        forgotPasswordRequest.email = layoutForgotpasswordEmail.text.toString().trim()

        val call = APIFactory.getService().forgotPassword(forgotPasswordRequest)

        call.enqueue(object : Callback<ForgotPasswordResponse> {

            override fun onResponse(call: Call<ForgotPasswordResponse>, response: Response<ForgotPasswordResponse>) {
                if (response.body()!!.status.equals(Constants.SUCCESS)) {
                    displayToast(response.body()!!.message!!)
                    finish()
                } else if (response.body()!!.status.equals(Constants.ERROR)) {
                    displayToast(response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<ForgotPasswordResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }

}