package app.com.tnonline.ui.infosection.flight

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.widget.Filter
import android.widget.LinearLayout
import app.com.tnonline.R
import app.com.tnonline.adapters.FlightAdapter
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.flight.DataItem
import app.com.tnonline.data.remote.models.flight.FlightResponse
import app.com.tnonline.ui.realEsate.RealEstateDetailsActivity
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import kotlinx.android.synthetic.main.activity_flight.*
import kotlinx.android.synthetic.main.activity_flight.llParent
import kotlinx.android.synthetic.main.activity_flight.recyclerViewFlight
import kotlinx.android.synthetic.main.activity_flight_new.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FlightActivity : BaseActivity() {
    var flightList = ArrayList<DataItem>()
    var flightAdapter: FlightAdapter? = null


    private val appPreferences = SharedPreferences()
    override val layoutResId: Int
        get() = R.layout.activity_flight

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupActionBar()
        listOfFlightAPI()
    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Airport list"
    }

    private fun listOfFlightAPI() {
        showProgressBar(llParent)

        val call = APIFactory.getService().getFlightList()
        call.enqueue(object : Callback<FlightResponse> {
            override fun onResponse(call: Call<FlightResponse>, response: Response<FlightResponse>) {
                if (response.body()!!.status.equals(Constants.SUCCESS)) {
                    dismissProgressBar()

                    if (response.body()!!.data != null) {
                        flightList.addAll(response.body()!!.data!!)
                        /*for (i in 0 until response.body()!!.data!!.size) {
                            flightList = response.body()!!.data!!

                        }*/
                        setupAdapter()
                    }

                    displayToast(response.body()!!.message!!)
                } else if (response.body()!!.status.equals(Constants.ERROR)) {
                    displayToast(response.body()!!.message!!)
                }
            }

            override fun onFailure(call: Call<FlightResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }

    private fun setupAdapter() {
        flightAdapter = FlightAdapter(this@FlightActivity, flightList)
        { view: View, dataItems: app.com.tnonline.data.remote.models.flight.DataItem, i: Int ->
            val flightArea: String = dataItems.iframeUrl.toString()
            val title: String = dataItems.title.toString()
            val flightCity: String = dataItems.cityName.toString()
            appPreferences.setString(Constants.FLIGHT_AREA_URL, flightArea)
            appPreferences.setString(Constants.FLIGHT_AREA, title)
            appPreferences.setString(Constants.FLIGHT_CITY, flightCity)

            //startActivity(Intent(this@FlightActivity, FlightDetailsShowActivity::class.java))
            onBackPressed()
        }
        recyclerViewFlight.layoutManager = LinearLayoutManager(this@FlightActivity, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        recyclerViewFlight.adapter = flightAdapter
        recyclerViewFlight.adapter!!.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
