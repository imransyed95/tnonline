package app.com.tnonline.ui.restaurant.appionment

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import app.com.tnonline.R
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.appoinment.AppoinmentRequest
import app.com.tnonline.data.remote.models.appoinment.AppoinmentResponse
import app.com.tnonline.data.remote.models.appoinment.restaurant.DineInAppoinmentRequest
import app.com.tnonline.data.remote.models.appoinment.restaurant.DineInResponse
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import app.com.tnonline.utils.Utilities
import kotlinx.android.synthetic.main.activity_dine_in_appoinment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import app.com.tnonline.utils.Singleton

class DineInAppoinmentActivity  : BaseActivity(), View.OnClickListener {

    var mDate: String = ""
    var mMonth: String = ""
    var mMonthNum: String = ""
    var mYear: String = ""

    val myCalendar = Calendar.getInstance()
    var date: DatePickerDialog.OnDateSetListener? = null
    var timeSetListener: TimePickerDialog.OnTimeSetListener? = null
    override val layoutResId: Int
        get() = R.layout.activity_dine_in_appoinment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inItViews()
    }

    private fun inItViews() {
        tvDate.setOnClickListener(this)
        btn_submit.setOnClickListener(this)
        tvTime.setOnClickListener(this)
        root_layout.setOnClickListener(this)
        date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val myFormat = "dd/MM/yy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)

            tvDate.text = sdf.format(myCalendar.time)
        }

        timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            myCalendar.set(Calendar.HOUR_OF_DAY, hour)
            myCalendar.set(Calendar.MINUTE, minute)

            tvTime.text = SimpleDateFormat("HH:mm").format(myCalendar.time)
        }


    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.btn_submit -> {
                validation()
            }

            R.id.tvDate -> {
                showCalenderDialog()
            }

            R.id.root_layout->{
                finish()
            }
            R.id.tvTime -> {

                // TimePickerDialog(applicationContext, timeSetListener, myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), true).show()

                var hour = myCalendar.get(Calendar.HOUR_OF_DAY)
                var minutes = myCalendar.get(Calendar.MINUTE)

                var mTimePicker: TimePickerDialog



                mTimePicker= TimePickerDialog(this,
                        TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                            hour = hourOfDay
                            minutes = minute

                            var timeSet = ""
                            if (hour > 12) {
                                hour -= 12
                                timeSet = "PM"
                            } else if (hour === 0) {
                                hour += 12
                                timeSet = "AM"
                            } else if (hour === 12) {
                                timeSet = "PM"
                            } else {
                                timeSet = "AM"
                            }

                            var min = ""
                            if (minutes < 10)
                                min = "0$minutes"
                            else
                                min = minutes.toString()

                            // Append in a StringBuilder
                            val aTime = StringBuilder().append(hour).append(':')
                                    .append(min).append(" ").append(timeSet).toString()


                            //tvTime.text = hourOfDay.toString()+ ":" + minute
                            tvTime.text = aTime
                        }, hour, minutes, false)

                mTimePicker.show()
            }
            R.id.goBackOnPress -> {
                onBackPressed()
            }
        }


    }

    private fun validation() {

        if (et_name.text.toString().trim() == "") {
            //displayToast("Please Enter your name")
            et_name.setError("Please Enter your name")
        } else if (et_mobile.text.toString().trim() == "") {
            //displayToast("Please Enter your mobile number")
            et_mobile.setError("Please Enter your mobile number")
        }else if (et_no_guest.text.toString().trim() == "") {
            //displayToast("Please Enter your mobile number")
            et_mobile.setError("Please Enter Number of Guests")
        }
        else if (et_mobile.text.toString().trim().length != 10) {
            //displayToast("Mobile number must be 10 digit")
            et_mobile.setError("Mobile number must be 10 digit")
        } else if (!Utilities.isValidEmail(et_email.text.toString().trim())) {
            //displayToast("Please Enter valid email id")
            et_email.setError("Please Enter valid email id")
        } else {
            callApi()
        }

    }

    private fun callApi() {
        //  showProgressBar(llRoot)
        val request = DineInAppoinmentRequest()
        request.restaurantsId = Singleton.restaurant_id
        request.name = et_name.text.toString().trim()
        request.phoneNumber = et_mobile.text.toString().trim()
        request.email = et_email.text.toString().trim()
        request.partyDate = tvDate.text.toString().trim()
        request.partyTime = tvTime.text.toString()
        request.enquiryFor = sp_enquiry_for.selectedItem.toString()
        request.comments=et_comment.text.toString()
        request.members=et_no_guest.text.toString()

        val call = APIFactory.getService().dineInAppoinmentApi(request)
        call.enqueue(object : Callback<DineInResponse> {
            override fun onFailure(call: Call<DineInResponse>?, t: Throwable?) {
                dismissProgressBar()
                displayToast(t.toString())
            }

            override fun onResponse(call: Call<DineInResponse>?, response: Response<DineInResponse>?) {
                dismissProgressBar()
                if (response != null) {
                    try {
                        if (response.body()!!.status.equals(Constants.SUCCESS)) {
                            displayToast(response.body()?.message.toString())
                            onBackPressed()
                        } else if (response.body()!!.status.equals(Constants.ERROR)) {
                            dismissProgressBar()
                            displayToast("failure")
                            onBackPressed()
                        }
                    }catch (e: Exception){
                        onBackPressed()
                    }
                }

            }

        })

    }

    private fun showCalenderDialog() {

        DatePickerDialog(this@DineInAppoinmentActivity,
                date,
                // set DatePickerDialog to point to today's date when it loads up
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show()

    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        /* actionBar!!.setBackgroundDrawable( ColorDrawable(resources.getColor(R.color.adap)))
         actionBar.setDisplayShowTitleEnabled(false);
         actionBar.setDisplayShowTitleEnabled(true);*/
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Online Appointment"
    }
}
