package app.com.tnonline.ui.classified

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import app.com.tnonline.R
import app.com.tnonline.adapters.DoctorListAdapter
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.local.SingleTon
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.city.DataItem
import app.com.tnonline.data.remote.models.doctor.response.Datum
import app.com.tnonline.data.remote.models.doctor.response.DoctorResponse
import app.com.tnonline.data.remote.models.doctor.request.DoctorRequest
import app.com.tnonline.ui.AppointmentActivity

import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import kotlinx.android.synthetic.main.activity_doctor.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import android.app.Dialog
import android.view.View
import android.view.Window
import android.widget.*
import app.com.tnonline.data.remote.models.locality.LocalityDataResponse
import app.com.tnonline.data.remote.models.locality.LocalityRequest
import app.com.tnonline.data.remote.models.locality.SpecialistDataResponse
import app.com.tnonline.data.remote.models.locality.SpecialistRequest


class DoctorListActivity : BaseActivity() {
    private var doctorListAdapter: DoctorListAdapter? = null
    var searchView: SearchView? = null
    var page_title: TextView? = null
    var back_button : ImageView? = null
    var filter_button : ImageView? = null
    var modelArrayList = ArrayList<DataItem>()
    private val appPreferences = SharedPreferences()
    var localityID_flag: Int = 0
    var specialistID_flag : Int = 74

    var localityStr:String?=null
    var cityIdStr:String?=null
    var stateIdStr:String?=null

    var filter_doctor_dialog: Dialog? = null
    var update_or_refresh_list : TextView? = null
    var close_doctor_dialog : ImageView? = null
    var localityButton : TextView? = null
    var specilityButton : TextView? = null
    var dialogFlag : Int = 0
    var locality_layout : LinearLayout? = null
    var specialist_layout : LinearLayout? = null
    var locality_list : ListView? = null
    var specialist_list : ListView? = null

    private var localityList_name = ArrayList<String>()
    private var localityList_id = ArrayList<Int>()

    private var specialist_name = ArrayList<String>()
    private var specialist_id = ArrayList<Int>()

    //Pagenation
    var display :TextView? = null
    var pre : ImageButton? =null
    var nex : ImageButton? = null
    var currentPage = 1
    var currentListCount = 10

    override val layoutResId: Int
        get() = R.layout.activity_doctor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        stateIdStr =  appPreferences.getString(Constants.STATE_ID)
        cityIdStr =  appPreferences.getString(Constants.CITY_ID)
        localityStr =  appPreferences.getString(Constants.CITY_ID)

        page_title = findViewById(R.id.page_title) as TextView
        back_button = findViewById(R.id.back_button) as ImageView
        filter_button = findViewById(R.id.filter_button) as ImageView

        pre = findViewById(R.id.pre) as ImageButton
        nex = findViewById(R.id.nex) as ImageButton
        display = findViewById(R.id.display) as TextView


        page_title!!.text = "Doctors"

        //setupActionBar()
        // listOfFlightAPI()

        createFilterDialog()
        callLocalityApi()
        callspecialistApi()
        callDoctorApi(localityID_flag)


        pre!!.setOnClickListener {
            decrease()
        }

        nex!!.setOnClickListener {
            increase()
        }

    }

    private fun callspecialistApi() {
        var request = SpecialistRequest()
        request.state_id = stateIdStr?.toInt()
        request.city_id = cityIdStr?.toInt()
        request.locality_id = localityID_flag
        val call = APIFactory.getService().SpeciallistApi(request)

        call.enqueue(object : Callback<SpecialistDataResponse>{
            override fun onResponse(call: Call<SpecialistDataResponse>, response: Response<SpecialistDataResponse>) {
                if(response.body()!!.status!! == Constants.SUCCESS){
                    dismissProgressBar()
                    if(response.body()!!.data != null){
                        if(response.body()!!.data!!.size > 0){
                            currentListCount = response.body()!!.data!!.size
                        }
                        if(currentListCount == 10){
                            checkforNextButtonVisibility()
                        }
                        for (i in 0 until response.body()!!.data!!.size) {
                            specialist_name.add(response.body()?.data?.get(i)?.propertytype_name!!.toString())
                            specialist_id.add(response.body()?.data?.get(i)?.propertytype_id!!.toInt())
                        }
                        setSpecialistToAdapter(specialist_name)
                    }
                }else{
                    displayToast("Error")
                }
            }
            override fun onFailure(call: Call<SpecialistDataResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }

    private fun callLocalityApi() {
        var request = LocalityRequest()
        request.city_id = cityIdStr?.toInt()!!
        request.state_id = stateIdStr?.toInt()!!
        request.category_id = 47

        val call = APIFactory.getService().localityApi(request)

        call.enqueue(object : Callback<LocalityDataResponse> {
            override fun onResponse(call: Call<LocalityDataResponse>, response: Response<LocalityDataResponse>) {
                if (response.body()!!.status!! == Constants.SUCCESS) {
                    dismissProgressBar()
                    if (response.body()!!.data != null) {
                        for (i in 0 until response.body()!!.data!!.size) {
                            localityList_name.add(response.body()?.data?.get(i)?.locality_name!!.toString())
                            localityList_id.add(response.body()?.data?.get(i)?.locality_id!!.toInt())
                        }
                        setLocalityListToAdapter(localityList_name)
                    }
                    //   displayToast(response.body()!!.message!!)
                } else {
                    displayToast("Error")
                }
            }
            override fun onFailure(call: Call<LocalityDataResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }

    private fun setLocalityListToAdapter(localityList: ArrayList<String>) {
        if(localityList != null){
            val adapter = ArrayAdapter<String>(this,
                    R.layout.text_row, R.id.spinner_text, localityList)
            locality_list!!.adapter = adapter
        }
    }

    private fun setSpecialistToAdapter(specialist_name: ArrayList<String>) {
        if(specialist_name != null){
            val adapter = ArrayAdapter<String>(this,
                    R.layout.text_row, R.id.spinner_text, specialist_name)
            specialist_list!!.adapter = adapter
        }
    }

    private fun callDoctorApi(localityID: Int) {
     //   showProgressBar(llParent)
        val request = DoctorRequest()
        request.city_id = cityIdStr?.toInt()!!
        request.state_id = stateIdStr?.toInt()!!
        request.locality_id = localityID
    //    request.specialist_in = specialistID_flag

        val call = APIFactory.getService().doctorApiPagenation(currentPage, request)
        call.enqueue(object : Callback<DoctorResponse> {
            override fun onResponse(call: Call<DoctorResponse>, response: Response<DoctorResponse>) {
                dismissProgressBar()
                if (response.body()!!.status.equals(Constants.SUCCESS)) {
                    displayToast("Success")
                    setupAdapter(response.body()!!.data?.data)
                    var res = response.body().toString()
                } else if (response.body()!!.status.equals(Constants.ERROR)) {
                    dismissProgressBar()
                    displayToast("failure")
                }
            }

            override fun onFailure(call: Call<DoctorResponse>, t: Throwable) {
                dismissProgressBar()
                displayToast(t.toString())
            }
        })

        back_button!!.setOnClickListener {
            onBackPressed()
        }

        filter_button!!.setOnClickListener {
            filter_doctor_dialog!!.show();
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_doctor_search, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = menu!!.findItem(R.id.action_search)
                .actionView as SearchView
        searchView!!.setSearchableInfo(searchManager
                .getSearchableInfo(componentName))
        searchView!!.setMaxWidth(Integer.MAX_VALUE)

        // listening to search query text change
        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                doctorListAdapter!!.getFilter().filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                doctorListAdapter!!.getFilter().filter(query)
                return false
            }
        })
        return true
    }

    private fun setupAdapter(data: List<Datum>?) {
        doctorListAdapter = DoctorListAdapter(this@DoctorListActivity, data) { view, i ->
            when (view.id) {
                R.id.lly_head -> {
                    startActivity(Intent(this@DoctorListActivity, DoctorDetailsActivity::class.java))
                    val dataList: Datum = data!!.get(i)
                    SingleTon.instance.classified_id = dataList.classifieds_id
                    doctorListAdapter?.notifyDataSetChanged()
                }
                R.id.btn_take_appointment->{
                    val dataList: Datum = data!!.get(i)
                    SingleTon.instance.classified_id = 47 //dataList.classifieds_id
                    showAppoinmentDialog()
                }

            }


        }
        recyclerView.layoutManager = LinearLayoutManager(this@DoctorListActivity, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        recyclerView.adapter = doctorListAdapter
        recyclerView.adapter!!.notifyDataSetChanged()
    }

    private fun showAppoinmentDialog() {
        startActivity(Intent(this@DoctorListActivity, AppointmentActivity::class.java))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        filter_doctor_dialog!!.dismiss()
    }

    private fun createFilterDialog() {
        filter_doctor_dialog = Dialog(this);
        filter_doctor_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE);
        filter_doctor_dialog!!.setCancelable(false);
        filter_doctor_dialog!!.setContentView(R.layout.popup_dialog_filter_doctors);

        update_or_refresh_list = filter_doctor_dialog!!.findViewById(R.id.update_or_refresh_list) as TextView
        close_doctor_dialog = filter_doctor_dialog!!.findViewById(R.id.close_doctor_dialog) as ImageView
        localityButton = filter_doctor_dialog!!.findViewById(R.id.locality_button) as TextView
        specilityButton = filter_doctor_dialog!!.findViewById(R.id.specility_button) as TextView
        locality_layout = filter_doctor_dialog!!.findViewById(R.id.locality_layout) as LinearLayout
        specialist_layout = filter_doctor_dialog!!.findViewById(R.id.specialist_layout) as LinearLayout
        locality_list = filter_doctor_dialog!!.findViewById(R.id.locality_list) as ListView
        specialist_list = filter_doctor_dialog!!.findViewById(R.id.specialist_list) as ListView

        localityButton!!.setBackgroundColor(resources.getColor(R.color.green))
        localityButton!!.setTextColor(resources.getColor(R.color.white))


        localityButton!!.setOnClickListener {
            localityButton!!.setBackgroundColor(resources.getColor(R.color.green))
            localityButton!!.setTextColor(resources.getColor(R.color.white))

            specilityButton!!.setBackgroundColor(resources.getColor(R.color.youtube_light_gray))
            specilityButton!!.setTextColor(resources.getColor(R.color.textBlackcolor))
            specialist_layout!!.visibility = View.GONE
            locality_layout!!.visibility = View.VISIBLE
            dialogFlag = 0

        }

        specilityButton!!.setOnClickListener {
            localityButton!!.setBackgroundColor(resources.getColor(R.color.youtube_light_gray))
            localityButton!!.setTextColor(resources.getColor(R.color.textBlackcolor))

            specilityButton!!.setBackgroundColor(resources.getColor(R.color.green))
            specilityButton!!.setTextColor(resources.getColor(R.color.white))
            specialist_layout!!.visibility = View.VISIBLE
            locality_layout!!.visibility = View.GONE
            dialogFlag = 1
        }

        update_or_refresh_list!!.setOnClickListener {
            currentPage = 1;
            display!!.text = " 01 "
            updateList()
        }

        close_doctor_dialog!!.setOnClickListener {
            filter_doctor_dialog!!.dismiss()
        }

        locality_list!!.setOnItemClickListener { parent, view, position, id ->
            if(localityList_name!!.size > 0){
                    Toast.makeText(mContext, localityList_name[position].toString(), Toast.LENGTH_SHORT).show()
                    localityID_flag = localityList_id[position]
            }
        }

        specialist_list!!.setOnItemClickListener { parent, view, position, id ->
            if(specialist_name!!.size > 0){
                Toast.makeText(mContext, specialist_name[position].toString(), Toast.LENGTH_SHORT).show()
                specialistID_flag = specialist_id[position]
            }
        }

    }

    private fun updateList() {
        close_doctor_dialog!!.performClick()
        callDoctorApi(localityID_flag)
    }

    private fun increase() {
        if(currentListCount >= 10) {
            nex!!.visibility = View.VISIBLE
            currentPage = currentPage + 1
            display!!.text = "Page : "+currentPage.toString()
            if(currentPage > 1) {
                pre!!.visibility = View.VISIBLE
            }
            callApi()
        }else{
            if(currentPage > 1) {
                pre!!.visibility = View.VISIBLE
            }
            Toast.makeText(applicationContext, "No more list to load ",Toast.LENGTH_LONG).show()
        }
    }

    private fun decrease() {
        if(currentPage > 1){
            currentPage = currentPage - 1
            display!!.text = "Page : "+currentPage.toString()
            pre!!.visibility = View.VISIBLE
            callApi()
            if(currentPage == 1){
                pre!!.visibility = View.INVISIBLE
            }
        }else{
            if(currentPage == 1){
                pre!!.visibility = View.INVISIBLE
// No need for func()
            }else{
                currentPage = 1
                pre!!.visibility = View.INVISIBLE
                callApi()
            }
        }
    }

    private fun callApi() {
    //    Toast.makeText(applicationContext , "Api called", Toast.LENGTH_LONG).show()
        updateList()
    }

    // call this method from Api response successs { currentListCount = data!!.list[].size() }
    private fun checkforNextButtonVisibility(){
        if(currentListCount < 10){
            nex!!.visibility = View.INVISIBLE
        }
    }

}
