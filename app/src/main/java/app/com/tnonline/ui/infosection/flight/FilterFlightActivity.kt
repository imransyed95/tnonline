package app.com.tnonline.ui.infosection.flight

import android.content.Intent
import android.os.Bundle
import app.com.tnonline.R
import app.com.tnonline.utils.BaseActivity
import kotlinx.android.synthetic.main.activity_flight_new.*
import kotlinx.android.synthetic.main.filter_layout.*

class FilterFlightActivity : BaseActivity()  {

    override val layoutResId: Int
        get() = R.layout.filter_layout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        nav_icon_img.setOnClickListener{
          finish()
        }
    }
}