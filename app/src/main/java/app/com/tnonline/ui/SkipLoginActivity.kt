package app.com.tnonline.ui

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import app.com.tnonline.R
import app.com.tnonline.adapters.CityImagesAdapter
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.city.CityListResponse
import app.com.tnonline.data.remote.models.city.DataItem
import app.com.tnonline.utils.BaseActivity
import app.com.tnonline.utils.Constants
import kotlinx.android.synthetic.main.activity_skip_login_two.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.*

class SkipLoginActivity : BaseActivity(),SearchView.OnQueryTextListener {


    var context: Context? = null
    var searchView:SearchView?=null
    var cityData = ArrayList<DataItem>()
    private var cityImagesAdapter: CityImagesAdapter? = null
    private val appPreferences = SharedPreferences()
    var stringSettings = ""
    override val layoutResId: Int
        get() = R.layout.activity_skip_login_two

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bundle=intent.extras
        if(bundle!=null)
        {
            stringSettings = intent.getStringExtra(Constants.SETTINGS)
        }

        context = this
        setupActionBar()
        if (internetCheck(context!!)) {
            listOfCityAPI()
        } else {
            displayToast(Constants.INTERNETCONNECTION)
        }
        cityListingRecyclerView.adapter = cityImagesAdapter


    }


    private fun listOfCityAPI() {
        // showProgressBar(llParent)
        val call = APIFactory.getService().getCityList()
        call.enqueue(object : Callback<CityListResponse> {
            override fun onResponse(call: Call<CityListResponse>, response: Response<CityListResponse>) {
                try {
                    if (response.body()!!.status.equals(Constants.SUCCESS)) {
                        dismissProgressBar()

                        if (response.body()!!.data != null) {
                            for (i in 0 until response.body()!!.data!!.size) {
                                cityData.addAll(response.body()!!.data!!)
                                //cityData = response.body()!!.data!!
                                setupAdapter()
                            }
                        }

                        // displayToast(response.body()!!.msg!!)
                    } else if (response.body()!!.status.equals(Constants.ERROR)) {
                        displayToast(response.body()!!.msg!!)
                    }
                }catch (e: Exception){

                }
            }

            override fun onFailure(call: Call<CityListResponse>, t: Throwable) {
                displayToast(t.toString())
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_sell_search, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
      searchView = menu!!.findItem(R.id.action_search)
                .actionView as SearchView
        searchView!!.setSearchableInfo(searchManager
                .getSearchableInfo(componentName))
        searchView!!.setMaxWidth(Integer.MAX_VALUE)

        // listening to search query text change
        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                cityImagesAdapter?.getFilter()?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                cityImagesAdapter?.getFilter()?.filter(query)
                return false
            }
        })
        return true
    }

    override fun onBackPressed() {
        // close search view on back button pressed
        if (!searchView!!.isIconified()) {
            searchView!!.setIconified(true)
            return
        }
        super.onBackPressed()
    }


    private fun setupActionBar() {
        val actionBar = supportActionBar
        title = "Select Location"
    }

    private fun setupAdapter() {
        cityImagesAdapter = CityImagesAdapter(this@SkipLoginActivity, cityData, cityData)
        { view: View, dataItems: DataItem, i: Int ->

            val cityId: String = dataItems.city_id.toString()
            val cityName: String = dataItems.city_name.toString()
            val cityImage: String = dataItems.img.toString()
            val stateId: String = dataItems.state_id.toString()

            appPreferences.setString(Constants.CITY_NAME, cityName)
            appPreferences.setString(Constants.CITY_ID, cityId)
            appPreferences.setString(Constants.CITY_IMAGE, cityImage)
            appPreferences.setString(Constants.STATE_ID, stateId)


            Log.e("MMMMMM", "{$cityId , $cityName , $cityImage}")



            if (stringSettings == Constants.SETTINGS) {
                onBackPressed()
            } else {
                startActivity(Intent(this@SkipLoginActivity, MainActivity::class.java))
                finish()
            }

        }

        cityListingRecyclerView.layoutManager = GridLayoutManager(this@SkipLoginActivity,3, GridLayoutManager.VERTICAL, false) as RecyclerView.LayoutManager?
        cityListingRecyclerView.adapter = cityImagesAdapter
        cityListingRecyclerView.adapter!!.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        cityImagesAdapter!!.getFilter().filter(query)
        return false
    }

    override fun onQueryTextChange(query: String?): Boolean {

        if (cityData != null && cityData.size > 0) {
            cityImagesAdapter!!.getFilter().filter(query)
        }
        return false    }
}