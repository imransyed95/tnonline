package app.com.tnonline.ui.realEsate

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import app.com.tnonline.R
import app.com.tnonline.adapters.RealEstateAdapter
import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.data.test.RealEstateModel
import app.com.tnonline.utils.BaseActivity
import kotlinx.android.synthetic.main.real_state_layout.*

class RealEstateActivity:BaseActivity() {

    var realEstateItems = ArrayList<RealEstateModel>()
    var adapter: RealEstateAdapter? = null
    private val appPreferences = SharedPreferences()

    override val layoutResId: Int
        get() = R.layout.real_state_layout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        nav_icon_img.setOnClickListener {
     //       onBackPressed()
        }
        val item1=RealEstateModel(R.drawable.airtel,"2 BHK Residential Apartment","$48.25L - $60.55L")
        val item2=RealEstateModel(R.drawable.airtel,"3 BHK Residential Apartment","$58.25L - $70.55L")
        val item3=RealEstateModel(R.drawable.airtel,"4 BHK Residential Apartment","$68.25L - $90.55L")
        realEstateItems.add(item1)
        realEstateItems.add(item2)
        realEstateItems.add(item3)
        setupAdapter(realEstateItems)
    }


    private fun setupAdapter(realEstateModel: ArrayList<RealEstateModel>) {
        adapter = RealEstateAdapter(this@RealEstateActivity, realEstateModel) { view: View, dataItems: RealEstateModel, i: Int ->
            //startActivity(Intent(this, RealEstateDetailsActivity::class.java))

            when (view.id) {
                R.id.cardView -> {
                    startActivity(Intent(this@RealEstateActivity, RealEstateDetailsActivity::class.java))
                   // adapter?.notifyDataSetChanged()
                }

            }
        }
        real_state_rv.layoutManager = LinearLayoutManager(this@RealEstateActivity, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        real_state_rv.adapter = adapter
        real_state_rv.adapter!!.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        super.onBackPressed()

    }
}