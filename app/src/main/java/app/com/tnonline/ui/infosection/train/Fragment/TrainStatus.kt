package app.com.tnonline.ui.infosection.train.Fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.com.tnonline.R
import app.com.tnonline.ui.infosection.train.TrainDetailedActivity
import kotlinx.android.synthetic.main.fragment_seat_available.view.*

class TrainStatus : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_train_status, container, false)

        view.btn_search_train?.setOnClickListener {
            startActivity(Intent(activity, TrainDetailedActivity::class.java))
        }

        return view
    }


}