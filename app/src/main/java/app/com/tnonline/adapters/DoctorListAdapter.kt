package app.com.tnonline.adapters

import android.content.Context
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import app.com.tnonline.R
import app.com.tnonline.data.local.SingleTon
import app.com.tnonline.data.remote.models.doctor.response.Datum
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.adapter_doctor_list.view.*


class DoctorListAdapter(var context: Context, var modelArrayList: List<Datum>?,
                        private var listener: (View, Int) -> Unit) : RecyclerView.Adapter<DoctorListAdapter.MyViewHolder>(), Filterable {

    var secondList: List<Datum>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoctorListAdapter.MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_doctor_list_card, parent, false)

        secondList = modelArrayList
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return modelArrayList!!.size
    }

    override fun onBindViewHolder(holder: DoctorListAdapter.MyViewHolder, position: Int) {
        holder.bindItems(listener, position, modelArrayList!!)


    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(listener: (View, Int) -> Unit, position: Int, data: List<Datum>) {
            val dataList = data[position]

            itemView.txtHospitalName.text = dataList.company_name
            itemView.tv_address.text = dataList.address

            val specialityStr = dataList.specialist_in?.replace(",", " | ")
            itemView.tv_specialist.text = specialityStr
            //itemView.tv_availability.text = dataList.


            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            if (dataList.image?.isEmpty()!!) {
                Glide.with(context)
                        .load(R.drawable.no_image_icon)
                        .apply(RequestOptions().placeholder(circularProgressDrawable))
                        .into(itemView.ivDoctor)
            } else {
                Glide.with(context)
                        .load(dataList.image)
                        .apply(RequestOptions().placeholder(circularProgressDrawable))
                        .into(itemView.ivDoctor)
                Log.i("TTT", dataList.image.toString())

            }



            itemView.lly_head.setOnClickListener {
                listener(it, position)
                SingleTon.instance.doctorDetailsList = dataList
            }
            itemView.btn_take_appointment.setOnClickListener {
                listener(it, position)

            }


        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                if (charString.isEmpty()) {
                    modelArrayList = secondList
                } else {
                    val filteredList = ArrayList<Datum>()
                    for (row in secondList!!) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.company_name!!.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }

                    modelArrayList = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = modelArrayList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                modelArrayList = results!!.values as ArrayList<Datum>
                notifyDataSetChanged()

            }
        }

    }

}