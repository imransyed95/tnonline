package app.com.tnonline.adapters

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.remote.models.dashboard.DashBoardModel


class DashboardAdapter(var context: Context, var items: ArrayList<DashBoardModel>,
                       private var listener: (View, DashBoardModel, Int) -> Unit) : RecyclerView.Adapter<DashboardAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardAdapter.MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_dashboard, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: DashboardAdapter.MyViewHolder, position: Int) {
        holder.txtLabel.text = items[position].name
        holder.imgView.setImageResource(items[position].image!!)

        holder.itemView.setOnClickListener {
            listener(it, items[position], position)
        }
        if (items[position].visible){
            holder.vieView.visibility=View.GONE
        }


    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtLabel: AppCompatTextView = itemView.findViewById(R.id.txtLabel)
        var imgView: AppCompatImageView=itemView.findViewById(R.id.dashImage)
        var vieView:View=itemView.findViewById(R.id.viView)

    }


}