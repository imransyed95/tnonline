package app.com.tnonline.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.remote.models.flight.DataItem



class FlightAdapterNew(var context: Context, var items: ArrayList<DataItem>,
                       private var listener: (View, DataItem, Int) -> Unit) : RecyclerView.Adapter<FlightAdapterNew.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightAdapterNew.MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_flight_new, parent, false)
        return MyViewHolder(itemView)
    }

   /* override fun getItemCount(): Int {
        return items.size
    }*/
    override fun getItemCount(): Int {
        return 20
    }

    override fun onBindViewHolder(holder: FlightAdapterNew.MyViewHolder, position: Int) {
      /*  holder.txtFlight.text = items[position].title

        holder.itemView.setOnClickListener {
            listener(it, items[position], position)
        }*/

    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtFlight: TextView = itemView.findViewById(R.id.txtFlight)
        var txtDept: TextView = itemView.findViewById(R.id.txtDept)
        var txtTerm: TextView = itemView.findViewById(R.id.txtTerm)
        var txtGate: TextView = itemView.findViewById(R.id.txtGate)
        var txtArrives: TextView = itemView.findViewById(R.id.txtArrives)

    }


}