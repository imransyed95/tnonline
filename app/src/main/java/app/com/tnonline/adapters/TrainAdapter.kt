package app.com.tnonline.adapters
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.com.tnonline.R



class TrainAdapter(var context: Context, var items: ArrayList<String>,
                   private var listener: (View, String, Int) -> Unit) : RecyclerView.Adapter<TrainAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrainAdapter.MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_flight, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: TrainAdapter.MyViewHolder, position: Int) {
        holder.txtFlightName.text = items[position]

        holder.itemView.setOnClickListener {
            listener(it, items[position], position)


        }

    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtFlightName: TextView = itemView.findViewById(R.id.txtFlightName)

    }


}