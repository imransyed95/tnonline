package app.com.tnonline.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.remote.models.train.arrival.trainlist.TrainsItem


class TrainArrivalsAdapter(var context: Context, var items: ArrayList<TrainsItem>,
                           private var listener: (View, TrainsItem, Int) -> Unit) : RecyclerView.Adapter<TrainArrivalsAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrainArrivalsAdapter.MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_train_arrivals, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: TrainArrivalsAdapter.MyViewHolder, position: Int) {
        holder.txtTrainName.text ="Train Name : " +items[position].name
        holder.txtTrainNumber.text = "Train Number : " +items[position].number
       // holder.txtRechargeAmount.setText(context.resources.getText(R.string.Rs) items[position].price)
        holder.txtTime.text = items[position].scharr+ " --> "+items[position].schdep
        holder.txtDelay.text = "Delay : " +items[position].delayarr

        holder.itemView.setOnClickListener {
            listener(it, items[position], position)
        }
    }

    private fun getDate(updatedAt: String?) {
        var fullDate=updatedAt


    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTrainNumber: TextView = itemView.findViewById(R.id.txtTrainNumber)
        var txtDelay: TextView = itemView.findViewById(R.id.txtTrainDelay)
        var txtTrainName: TextView = itemView.findViewById(R.id.txtTrainName)
        var txtTime: TextView = itemView.findViewById(R.id.txtTrainTime)



    }


}