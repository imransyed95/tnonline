package app.com.tnonline.adapters

import android.content.Context
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.remote.models.flight.DataItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


class FlightAdapter(var context: Context, var items: ArrayList<DataItem>,
                    private var listener: (View, DataItem, Int) -> Unit) : RecyclerView.Adapter<FlightAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightAdapter.MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_flight, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: FlightAdapter.MyViewHolder, position: Int) {
        holder.txtFlightName.text = items[position].title

        holder.itemView.setOnClickListener {
            listener(it, items[position], position)
        }

    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtFlightName: TextView = itemView.findViewById(R.id.txtFlightName)

    }


}