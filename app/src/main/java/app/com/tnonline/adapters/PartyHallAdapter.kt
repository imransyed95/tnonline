package app.com.tnonline.adapters

import android.content.Context
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.test.PartHallModel
import app.com.tnonline.data.test.RealEstateModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.adapter_doctor_list.view.*

class PartyHallAdapter (var context: Context, var items: ArrayList<PartHallModel>,
                        private var listener: (View, PartHallModel, Int) -> Unit) : RecyclerView.Adapter<PartyHallAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.party_hall_list_items, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtAppartmentName.text = items[position].name
        holder.txtPrice.text = items[position].address
        if(items[position].type!="2"){
            holder.featureLayout.visibility=View.GONE
            holder.type.visibility=View.GONE
        }
            /*Glide.with(context)
                    .load(items[position].image)
                    .into(holder.imageApartment)*/
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()
        Glide.with(context).asBitmap().load(items[position].image)
                .apply(RequestOptions()
                        .fitCenter()
                        .placeholder(circularProgressDrawable))
                .into(holder.imageApartment)

        holder.itemView.setOnClickListener {
            listener(it, items[position], position)


        }
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageApartment: ImageView = itemView.findViewById(R.id.party_hall_img)
        var txtAppartmentName: TextView = itemView.findViewById(R.id.apartment_name)
        var txtPrice: TextView = itemView.findViewById(R.id.price_tv)
        var featureLayout:LinearLayout=itemView.findViewById(R.id.appartment_layout_feature)
        var type: TextView = itemView.findViewById(R.id.type_tv)
    }

}