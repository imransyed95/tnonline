package app.com.tnonline.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.remote.models.events.FeatureModel
import app.com.tnonline.data.test.PartHallModel
import com.bumptech.glide.Glide

class EventFeautreAdapter(var context: Context, var items: ArrayList<FeatureModel>,
                          private var listener: (View, FeatureModel, Int) -> Unit) : RecyclerView.Adapter<EventFeautreAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.resturant_features_list_items, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtname.text = items[position].name
        holder.imageFeature.setImageResource(R.drawable.ic_dot)
        holder.itemView.setOnClickListener {
            listener(it, items[position], position)

        }
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageFeature: ImageView = itemView.findViewById(R.id.image)
        var txtname: TextView = itemView.findViewById(R.id.txtname)

    }
}