package app.com.tnonline.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.remote.models.events.FeatureModel
import app.com.tnonline.data.test.PartHallModel
import com.bumptech.glide.Glide

class EventSupplyAdapter(var context: Context, var items: ArrayList<FeatureModel>,
                         private var listener: (View, FeatureModel, Int) -> Unit) : RecyclerView.Adapter<EventSupplyAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.features_list_items, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.txtname.text = items[position].name

        if(items[position].image==""){
            if(items[position].name=="AC"){
                holder.imageFeature.setImageResource(R.drawable.ic_ac)
            }else if(items[position].name=="Wi-fi"){
                holder.imageFeature.setImageResource(R.drawable.ic_wifi_black_24dp)
            }else if(items[position].name=="Elevators"){
                holder.imageFeature.setImageResource(R.drawable.lift)
            }else if(items[position].name=="Catering Indoor"){
                holder.imageFeature.setImageResource(R.drawable.ac)
            }else if(items[position].name=="DJ"){
                holder.imageFeature.setImageResource(R.drawable.party_dj)
            }else if(items[position].name=="Stage"){
                holder.imageFeature.setImageResource(R.drawable.stage)
            }else if(items[position].name=="Partking"){
                holder.imageFeature.setImageResource(R.drawable.ic_parking)
            }else{
                holder.imageFeature.setImageResource(R.drawable.ic_parking)
            }
        }else{
            Glide.with(context)
                    .load(items[position].image)
                    .into(holder.imageFeature)

        }
        holder.itemView.setOnClickListener {
            listener(it, items[position], position)

        }
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageFeature: ImageView = itemView.findViewById(R.id.image)
        var txtname: TextView = itemView.findViewById(R.id.txtname)

    }
}