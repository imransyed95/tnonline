package app.com.tnonline.adapters
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.TrainBetweenStation.Trains


class TrainBetweenStationAdapter(var context: Context, var items: ArrayList<Trains?>?,
                                 private var listener: (View, String, Int) -> Unit) : RecyclerView.Adapter<TrainBetweenStationAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrainBetweenStationAdapter.MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_train_search, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    override fun onBindViewHolder(holder: TrainBetweenStationAdapter.MyViewHolder, position: Int) {
        holder.train_name.text = this!!.items!![position]!!.name

//        holder.itemView.setOnClickListener {
//            listener(it, items[position], position)
//
//
//        }

    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var train_name: TextView = itemView.findViewById(R.id.item_train_name)

    }


}