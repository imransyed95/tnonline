package app.com.tnonline.adapters

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.remote.models.city.DataItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.util.*


class CityImagesAdapter(var context: Context,var modelArrayList: ArrayList<DataItem>, val secondList: ArrayList<DataItem>,
  private var listener: (View, DataItem, Int) -> Unit) : RecyclerView.Adapter<CityImagesAdapter.MyViewHolder>() ,Filterable {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityImagesAdapter.MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_city_two, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return modelArrayList.size
    }

    override fun onBindViewHolder(holder: CityImagesAdapter.MyViewHolder, position: Int) {
        holder.cityName.text = modelArrayList[position].city_name
        var url: String? = modelArrayList[position].img
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()
        Log.e("mmmmmm", "$url")

var isButtonClicked=false
        if (url!!.contains(" ")) {
            Log.e("mmmmmm contains", "$url")

            url = url.replace(" ", "%20")
            Log.e("mmmmmm replace", "$url")

        }


        Glide.with(context)
                .load(url)
                .apply(RequestOptions().placeholder( circularProgressDrawable))
                .into(holder.cityImage)


        holder.itemView.setOnClickListener {
            listener(it, modelArrayList[position], position)
            isButtonClicked = !isButtonClicked // toggle the boolean flag
            holder.consLayt.setBackgroundResource(if (isButtonClicked) R.drawable.bg_rounded_corner_green else R.drawable.bg_rounded_corner)
            holder.ievView.setBackgroundColor(if (isButtonClicked) ContextCompat.getColor(context,R.color.transparent_) else ContextCompat.getColor(context,R.color.transparent))

        }

    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cityImage: ImageView = itemView.findViewById(R.id.cityImage)
        var cityName: TextView = itemView.findViewById(R.id.cityName)
        var consLayt: ConstraintLayout = itemView.findViewById(R.id.conl_head)
        var ievView:View=itemView.findViewById(R.id.viView)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                if (charString.isEmpty()) {
                    modelArrayList = secondList
                } else {
                    val filteredList = ArrayList<DataItem>()
                    for (row in secondList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.city_name!!.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }

                    modelArrayList = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = modelArrayList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                modelArrayList = results!!.values as ArrayList<DataItem>
                notifyDataSetChanged()

            }
        }

    }

}