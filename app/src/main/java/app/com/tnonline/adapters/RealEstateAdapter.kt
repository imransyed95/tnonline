package app.com.tnonline.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.test.RealEstateModel

class RealEstateAdapter (var context: Context, var items: ArrayList<RealEstateModel>,
                         private var listener: (View, RealEstateModel, Int) -> Unit) : RecyclerView.Adapter<RealEstateAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.real_estate_rv_items, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtAppartmentName.text = items[position].name
        holder.txtPrice.text = items[position].price

        holder.itemView.setOnClickListener {
            listener(it, items[position], position)


        }
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageApartment: ImageView = itemView.findViewById(R.id.imageView4)
        var txtAppartmentName: TextView = itemView.findViewById(R.id.apartment_name)
        var txtPrice: TextView = itemView.findViewById(R.id.price_tv)
    }

}