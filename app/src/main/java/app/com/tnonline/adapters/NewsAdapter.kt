package app.com.tnonline.adapters

import android.content.Context
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.remote.models.news.DataItem
import app.com.tnonline.data.remote.models.news.News
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class NewsAdapter(var context: Context, var items: ArrayList<News>,
                       private var listener: (View, News, Int) -> Unit) : RecyclerView.Adapter<NewsAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.news_card, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtLabel.text = items[position].title
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()
        Glide.with(context)
                .load(items[position].image!!)
                .apply(RequestOptions().placeholder( circularProgressDrawable))
                .into(holder.imgView)


        holder.itemView.setOnClickListener {
            listener(it, items[position], position)
        }

        holder.imgView.setOnClickListener {
            listener(it, items[position], position)
        }



    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtLabel: TextView = itemView.findViewById(R.id.title)
        var imgView: ImageView =itemView.findViewById(R.id.thumbnail)
        var vieView: View =itemView.findViewById(R.id.count)

    }


}