package app.com.tnonline.adapters

import android.content.Context
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import app.com.tnonline.R
import app.com.tnonline.data.test.NonVegModel
import app.com.tnonline.data.test.PartHallModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class NonVegAdapter (var context: Context, var items: ArrayList<NonVegModel>,
                        private var listener: (View, NonVegModel, Int) -> Unit) : RecyclerView.Adapter<NonVegAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.non_veg_rv_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtHotelName.text = items[position].name
        holder.txtLocation.text = items[position].location
        holder.txtDetails.text = items[position].details
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()
        Glide.with(context).asBitmap().load(items[position].image)
                .apply(RequestOptions()
                        .fitCenter()
                        .placeholder(circularProgressDrawable))
                .into(holder.imageApartment)

        holder.itemView.setOnClickListener {
            listener(it, items[position], position)
        }


        holder.takeAppoinment.setOnClickListener {
            listener(it, items[position], position)
        }

    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageApartment: ImageView = itemView.findViewById(R.id.imageView4)
        var txtHotelName: TextView = itemView.findViewById(R.id.hotel_name)
        var txtLocation: TextView = itemView.findViewById(R.id.address_tv)
        var txtDetails: TextView = itemView.findViewById(R.id.detail_tv)
        var takeAppoinment: Button = itemView.findViewById(R.id.btn_take_appointment)
    }

}