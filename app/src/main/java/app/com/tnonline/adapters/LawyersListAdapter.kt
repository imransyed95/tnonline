package app.com.tnonline.adapters

import android.content.Context
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import app.com.tnonline.R
import app.com.tnonline.data.local.SingleTon
import kotlin.collections.ArrayList
import app.com.tnonline.data.remote.models.doctor.response.Datum
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.adapter_lawyers_list.view.*


class LawyersListAdapter(var context: Context, var modelArrayList: List<Datum>?, val secondList: List<Datum>?,
                         private var listener: (View, Int) -> Unit) : RecyclerView.Adapter<LawyersListAdapter.MyViewHolder>() ,Filterable {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LawyersListAdapter.MyViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_lawyers_list, parent, false)

        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return modelArrayList!!.size
    }

    override fun onBindViewHolder(holder: LawyersListAdapter.MyViewHolder, position: Int) {
        holder.bindItems(listener, position, (modelArrayList as ArrayList<Datum>?)!!)

    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(listener: (View, Int) -> Unit, position: Int, dataList: ArrayList<Datum>) {
            val dataItems = dataList[position]

            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            itemView.tv_adv_name.text = dataItems.company_name
            itemView.tv_address.text = dataItems.address

            val specialityStr =dataItems.specialist_in?.replace(","," | ")

            itemView.tv_specialist.text = specialityStr

            if(dataItems.image?.isEmpty()!!){
                Glide.with(context)
                        .load(R.drawable.no_image_icon)
                        .apply(RequestOptions().placeholder( circularProgressDrawable))
                        .into(itemView.ivLawyer)
            }else{
                Glide.with(context)
                        .load(dataItems.image)
                        .apply(RequestOptions().placeholder( circularProgressDrawable))
                        .into(itemView.ivLawyer)
            }


            itemView.lly_head.setOnClickListener {
                listener(it,position)
                SingleTon.instance.doctorDetailsList = dataItems
            }
            itemView.btn_take_appointment.setOnClickListener {
                listener(it,position)
            }
        }

    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                if (charString.isEmpty()) {
                    modelArrayList = secondList
                } else {
                    val filteredList = ArrayList<Datum>()
                    for (row in secondList!!) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.company_name!!.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }

                    modelArrayList = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = modelArrayList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                modelArrayList = results!!.values as ArrayList<Datum>
                notifyDataSetChanged()

            }
        }

    }

}