package app.com.tnonline.fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import app.com.tnonline.R
import app.com.tnonline.adapters.NewsAdapter
import app.com.tnonline.data.remote.APIFactory
import app.com.tnonline.data.remote.models.news.DataItem
import app.com.tnonline.data.remote.models.news.News
import app.com.tnonline.data.remote.models.news.Response
import app.com.tnonline.ui.news.NewsDetailPageActivity


/**
 * A simple [Fragment] subclass.
 *
 */
class OneNewsFragment : Fragment() {

    var langFlag: String = ""
    var category: String = ""
    var title: String = ""

    var recyler_view: RecyclerView? = null
    var mLayoutManager: LinearLayoutManager? = null
    var newsAdapter: NewsAdapter? = null

    //Pagenation
    var display : TextView? = null
    var pre : ImageButton? =null
    var nex : ImageButton? = null
    var currentPage = 1
    var totalPages = 1
    var currentListCount = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view : View = inflater.inflate(R.layout.fragment_one_news, container, false)
        recyler_view = view.findViewById(R.id.recyler_view) as RecyclerView

        pre = view.findViewById(R.id.pre) as ImageButton
        nex = view.findViewById(R.id.nex) as ImageButton
        display = view.findViewById(R.id.display) as TextView

        if (getArguments() != null) {
            category = getArguments()!!.getString("category")
            title = getArguments()!!.getString("title")
            langFlag = getArguments()!!.getString("lang")
        }

        pageButton()


        langApi(langFlag, category)

        return view

    }

    private fun pageButton() {

        pre!!.setOnClickListener {
            decrease()
        }

        nex!!.setOnClickListener {
            increase()
        }
    }

    private fun langApi(langFlag: String, category: String) {
        if(langFlag.equals("Tamil")) {
            listOfTamilNewsAPI(category)
        }else{
            listOfEnglishNewsAPI(category)
        }
    }

    private fun listOfTamilNewsAPI(category: String) {

        val call = APIFactory.getService().getLocalTamilNewList(category , currentPage.toString())

        call.enqueue(object : retrofit2.Callback<Response> {
            override fun onResponse(call: retrofit2.Call<Response>, response: retrofit2.Response<Response>) {
                if (response.body() != null) {
                    if(response.body()!!.data!!.size > 0){
                        //currentPage = response.body()!!.data!!.currentPage!!
                        //totalPages = response.body()!!.data!!.lastPage!!
                        callMethod(response.body()!!.data!!)
                    }
                } else {
                    // Error
                }
            }

            override fun onFailure(call: retrofit2.Call<Response>, t: Throwable) {

            }
        })
    }

    private fun listOfEnglishNewsAPI(category: String) {

        val call = APIFactory.getService().getLocalEnglishNewList(category, currentPage.toString())

        call.enqueue(object : retrofit2.Callback<Response> {
            override fun onResponse(call: retrofit2.Call<Response>, response: retrofit2.Response<Response>) {
                if (response.body() != null) {
                    if(response.body()!!.data!!.size > 0){
                       // currentPage = response.body()!!.data!!.currentPage!!
                       // totalPages = response.body()!!.data!!.lastPage!!
                        callMethod(response.body()!!.data!!)
                    }
                } else {
                    // Error
                }
            }

            override fun onFailure(call: retrofit2.Call<Response>, t: Throwable) {

            }
        })
    }

    private fun callMethod(data: List<News?>) {
        mLayoutManager = LinearLayoutManager(activity)
        recyler_view!!.layoutManager = mLayoutManager

        newsAdapter = NewsAdapter(context!!, data as ArrayList<News>) { view: View, dat: News, i: Int ->

            var intent = Intent(activity, NewsDetailPageActivity::class.java)
            intent.putExtra("imageUrl", dat.image.toString())
            intent.putExtra("title", dat.title.toString())
            intent.putExtra("pubdate", dat.pub.toString())
            var string1=dat.desc.toString()
            string1=string1.replace("<p>","",false)
            string1=string1.replace("</p>","",false)



            intent.putExtra("description",string1)

            startActivity(intent)

        }
        recyler_view!!.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?

        //   recyler_view!!.addItemDecoration(GridSpacingItemDecoration(2, dpToPx(10), true))

        recyler_view!!.adapter = newsAdapter
        recyler_view!!.adapter!!.notifyDataSetChanged()

    }


    private fun increase() {
        if(totalPages > currentPage) {
            nex!!.visibility = View.VISIBLE
            currentPage = currentPage + 1
            display!!.text = "Page : " + currentPage.toString()
            if(currentPage > 1) {
                pre!!.visibility = View.VISIBLE
            }
            langApi(langFlag, category)
        }else{
            if(currentPage > 1) {
                pre!!.visibility = View.VISIBLE
            }
            Toast.makeText(context, "No more list to load ", Toast.LENGTH_LONG).show()
        }
    }

    private fun decrease() {
        if(currentPage > 1){
            currentPage = currentPage - 1
            display!!.text = "Page : "+currentPage.toString()
            pre!!.visibility = View.VISIBLE
            langApi(langFlag, category)
            if(currentPage == 1){
                pre!!.visibility = View.INVISIBLE
            }
        }else{
            if(currentPage == 1){
                pre!!.visibility = View.INVISIBLE
// No need for func()
            }else{
                currentPage = 1
                pre!!.visibility = View.INVISIBLE
                langApi(langFlag, category)
            }
        }
    }


}
