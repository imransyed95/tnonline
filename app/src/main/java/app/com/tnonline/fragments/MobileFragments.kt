package app.com.tnonline.fragments

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import app.com.tnonline.R
import kotlinx.android.synthetic.main.frag_mobile.*

class MobileFragments : AppCompatActivity() {

    private val selectOperator = arrayOf("Airtel", "Jio", "Vodafone", "DOCOMO", "BSNL")
    private val selectCircle = arrayOf("Tamil Nadu", "Andhra Pradesh", "Karnataka")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.frag_mobile)

        spinnerSelectOperator.adapter = ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item, selectOperator
        )
        spinnerSelectCircle.adapter = ArrayAdapter(
                this,
                R.layout.support_simple_spinner_dropdown_item, selectCircle
        )


        ivbackRecharge.setOnClickListener {
            finish()
        }
    }
}