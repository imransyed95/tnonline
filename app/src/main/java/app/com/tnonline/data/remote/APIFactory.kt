package app.com.tnonline.data.remote

import android.util.Log
import app.com.tnonline.interfaces.APIInterface
import app.com.tnonline.utils.Constants.BASE_URL
import app.com.tnonline.utils.Constants.BASE_URL_TRAIN
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class APIFactory {

    companion object {

        private var client = OkHttpClient()

        fun getService(): APIInterface {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            client = OkHttpClient.Builder()
                    .readTimeout(2, TimeUnit.MINUTES)
                    .addInterceptor(interceptor)
                    .connectTimeout(2, TimeUnit.MINUTES).build()

            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client).build()

            return retrofit.create(APIInterface::class.java)
        }
        fun getTrainService(): APIInterface {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            client = OkHttpClient.Builder()
                    .readTimeout(2, TimeUnit.MINUTES)
                    .addInterceptor(interceptor)
                    .connectTimeout(2, TimeUnit.MINUTES).build()

            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL_TRAIN)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client).build()
            Log.i("webUrl",retrofit.create(APIInterface::class.java).toString())
            return retrofit.create(APIInterface::class.java)
        }


        fun getClientWithIntercepter(): APIInterface? {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            client = OkHttpClient.Builder().connectTimeout(240, TimeUnit.SECONDS)
                    .readTimeout(240, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .addInterceptor(AuthenticationInterceptor()).build()

            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()

            return retrofit.create(APIInterface::class.java)
        }

    }
}