package app.com.tnonline.data.remote.models.news

//import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

//@Generated("com.robohorse.robopojogenerator")
data class DataItem(

	@field:SerializedName("image_url")
	val imageUrl: String? = null,

	@field:SerializedName("link")
	val link: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("language")
	val language: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("created_by")
	val createdBy: Any? = null,

	@field:SerializedName("updated_at")
	val updatedAt: Any? = null,

	@field:SerializedName("provider")
	val provider: String? = null,

	@field:SerializedName("mail_status")
	val mailStatus: Any? = null,

	@field:SerializedName("updated_by")
	val updatedBy: Any? = null,

	@field:SerializedName("rss_id")
	val rssId: Int? = null,

	@field:SerializedName("category")
	val category: String? = null,

	@field:SerializedName("publish_date")
	val publishDate: String? = null,

	@field:SerializedName("pubdate")
	val pubdate: String? = null,

	@field:SerializedName("status")
	val status: Any? = null
)