package app.com.tnonline.data.remote.models.register

data class RegisterRequest(
        var password: String? = null,
        var full_name: String? = null,
        var contact_no: String? = null,
        var email: String? = null,
        var city_id: String? = null
)
