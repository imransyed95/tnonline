package app.com.tnonline.data.remote.models.restaurant


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RestaurantResponse (
        @SerializedName("status")
        @Expose
        val status: String? = null,
        @SerializedName("message")
        @Expose
        val message: String? = null,
        @SerializedName("data")
        @Expose
        val data: Datass? = null
)

class Datass {

    @SerializedName("current_page")
    @Expose
    var currentPage: Int? = null
    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null
    @SerializedName("first_page_url")
    @Expose
    var firstPageUrl: String? = null
    @SerializedName("last_page")
    @Expose
    var lastPage: Int? = null
    @SerializedName("last_page_url")
    @Expose
    var lastPageUrl: String? = null
    @SerializedName("next_page_url")
    @Expose
    var nextPageUrl: String? = null
    @SerializedName("per_page")
    @Expose
    var perPage: Int? = null
    @SerializedName("prev_page_url")
    @Expose
    var prevPageUrl: Any? = null
    @SerializedName("total")
    @Expose
    var total: Int? = null

}


class Datum {

    @SerializedName("restaurants_id")
    @Expose
    var restaurantsId: Int? = null
    @SerializedName("host_name")
    @Expose
    var hostName: String? = null
    @SerializedName("host_email")
    @Expose
    var hostEmail: String? = null
    @SerializedName("phone_number")
    @Expose
    var phoneNumber: String? = null
    @SerializedName("alternate_number")
    @Expose
    var alternateNumber: String? = null
    @SerializedName("hotel_name")
    @Expose
    var hotelName: String? = null
    @SerializedName("pincode")
    @Expose
    var pincode: Any? = null
    @SerializedName("cusine_types")
    @Expose
    var cusineTypes: List<String>? = null
    @SerializedName("amenities")
    @Expose
    var amenities: List<String>? = null
    @SerializedName("dine")
    @Expose
    var dine: List<String>? = null
    @SerializedName("instagram_id")
    @Expose
    var instagramId: String? = null
    @SerializedName("twitter_id")
    @Expose
    var twitterId: String? = null
    @SerializedName("fb_id")
    @Expose
    var fbId: String? = null
    @SerializedName("website_address")
    @Expose
    var websiteAddress: String? = null
    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("comments")
    @Expose
    var comments: String? = null
    @SerializedName("address")
    @Expose
    var address: String? = null
    @SerializedName("address2")
    @Expose
    var address2: String? = null
    @SerializedName("locality_id")
    @Expose
    var localityId: Int? = null
    @SerializedName("state_id")
    @Expose
    var stateId: Int? = null
    @SerializedName("user_id")
    @Expose
    var userId: Int? = null
    @SerializedName("city_name")
    @Expose
    var cityName: String? = null
    @SerializedName("city_image")
    @Expose
    var cityImage: String? = null
    @SerializedName("city_link")
    @Expose
    var cityLink: String? = null
    @SerializedName("locality_name")
    @Expose
    var localityName: String? = null
    @SerializedName("contact_no")
    @Expose
    var contactNo: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("user_name")
    @Expose
    var userName: String? = null
    @SerializedName("plan_start_date")
    @Expose
    var planStartDate: String? = null
    @SerializedName("plan_end_date")
    @Expose
    var planEndDate: String? = null
    @SerializedName("image_name")
    @Expose
    var imageName: String? = null
    @SerializedName("latitude")
    @Expose
    var latitude: String? = null
    @SerializedName("longitude")
    @Expose
    var longitude: String? = null
    @SerializedName("working_days")
    @Expose
    var workingDays: List<String>? = null
    @SerializedName("working_time")
    @Expose
    var workingTime: List<String>? = null

}