package app.com.tnonline.data.remote.models.forgotpassword

data class ForgotPasswordResponse(
        val message: String? = null,
        val status: String? = null
)
