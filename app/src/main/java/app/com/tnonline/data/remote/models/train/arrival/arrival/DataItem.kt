package app.com.tnonline.data.remote.models.train.arrival.arrival

data class DataItem(
	val trainName: String? = null,
	val id: Int? = null,
	val trainCode: String? = null
)
