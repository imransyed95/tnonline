package app.com.tnonline.data.remote.models.forgotpassword

data class ForgotPasswordRequest(
	var email: String? = null
)
