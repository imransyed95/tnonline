package app.com.tnonline.data.remote.models.trainPage.SeatAvailablity

import com.google.gson.annotations.SerializedName

data class DataItem(
		@field:SerializedName("name")
		val name: String? = null,
		@field:SerializedName("code")
		val code: String? = null,
		@field:SerializedName("lat")
		val lat: Double? = null,
		@field:SerializedName("lng")
		val lng: Double? = null
)
