package app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.TrainBetweenStation

import com.google.gson.annotations.SerializedName

data class Trains(
        @field:SerializedName("number")
        val number: String? = null,
        @field:SerializedName("name")
        val name: String? = null,
        @field:SerializedName("travel_time")
        val travel_time: String? = null,
        @field:SerializedName("src_departure_time")
        val src_departure_time: String? = null,
        @field:SerializedName("dest_arrival_time")
        val dest_arrival_time: String? = null,
        @field:SerializedName("from_station")
        val from_station: fromStation? = null,
        @field:SerializedName("to_station")
        val to_station: fromStation? = null,
        @field:SerializedName("classes")
        val classes: ArrayList<Classes?>? = null,
        @field:SerializedName("days")
        val days: ArrayList<Days?>? = null
)