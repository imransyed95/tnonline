package app.com.tnonline.data.test

import app.com.tnonline.R

data class NonVegModel(
        val restaurantId: String = "",
        val image: String = "",
        val name: String = "",
        val location: String = "",
        val details: String = "",
        val email:String?= "",
        val contact:String?= "",
        val description:String?="",
        var cuisines:ArrayList<String>,
        var amenities:ArrayList<String>,
        var dineTypes:ArrayList<String>
)