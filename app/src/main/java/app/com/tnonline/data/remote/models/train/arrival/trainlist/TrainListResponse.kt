package app.com.tnonline.data.remote.models.train.arrival.trainlist

import app.com.tnonline.data.remote.models.train.arrival.arrival.DataItem

data class TrainListResponse(
        val data: ArrayList<DataItem?>? = null,
        val message: String? = null,
        val status: String? = null
)
