package app.com.tnonline.data.remote.models.appoinment.restaurant

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class DineInAppoinmentRequest(
        @SerializedName("restaurants_id")
        @Expose
        var restaurantsId: String? = null,
        @SerializedName("enquiry_for")
        @Expose
        var enquiryFor: String? = null,
        @SerializedName("name")
        @Expose
        var name: String? = null,
        @SerializedName("phone_number")
        @Expose
        var phoneNumber: String? = null,
        @SerializedName("email")
        @Expose
        var email: String? = null,
        @SerializedName("party_date")
        @Expose
        var partyDate: String? = null,
        @SerializedName("party_time")
        @Expose
        var partyTime: String? = null,
        @SerializedName("members")
        @Expose
        var members: String? = null,
        @SerializedName("comments")
        @Expose
        var comments: String? = null
)


