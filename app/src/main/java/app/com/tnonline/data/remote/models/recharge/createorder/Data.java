package app.com.tnonline.data.remote.models.recharge.createorder;

public class Data {
    private String recharge_type;
    private String operator_id;
    private String created_at;
    private String created_by;
    private String number;
    private String recharge_id;
    private String user_id;
    private String price;
    private String domain;
    private String state_id;
    private String order_id;
    private String city_id;
    private String status;

    public void setRechargeType(String rechargeType) {
        this.recharge_type = rechargeType;
    }

    public String getRechargeType() {
        return recharge_type;
    }

    public void setOperatorId(String operatorId) {
        this.operator_id = operatorId;
    }

    public String getOperatorId() {
        return operator_id;
    }

    public void setCreatedAt(String createdAt) {
        this.created_at = createdAt;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public void setCreatedBy(String createdBy) {
        this.created_by = createdBy;
    }

    public String getCreatedBy() {
        return created_by;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setRechargeId(String rechargeId) {
        this.recharge_id = rechargeId;
    }

    public String getRechargeId() {
        return recharge_id;
    }

    public void setUserId(String userId) {
        this.user_id = userId;
    }

    public String getUserId() {
        return user_id;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getDomain() {
        return domain;
    }

    public void setStateId(String stateId) {
        this.state_id = stateId;
    }

    public String getStateId() {
        return state_id;
    }

    public void setOrderId(String orderId) {
        this.order_id = orderId;
    }

    public String getOrderId() {
        return order_id;
    }

    public void setCityId(String cityId) {
        this.city_id = cityId;
    }

    public String getCityId() {
        return city_id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "Data{" +
                        "recharge_type = '" + recharge_type + '\'' +
                        ",operator_id = '" + operator_id + '\'' +
                        ",created_at = '" + created_at + '\'' +
                        ",created_by = '" + created_by + '\'' +
                        ",number = '" + number + '\'' +
                        ",recharge_id = '" + recharge_id + '\'' +
                        ",user_id = '" + user_id + '\'' +
                        ",price = '" + price + '\'' +
                        ",domain = '" + domain + '\'' +
                        ",state_id = '" + state_id + '\'' +
                        ",order_id = '" + order_id + '\'' +
                        ",city_id = '" + city_id + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}
