package app.com.tnonline.data.remote.models.locality

data class LocalityRequest(
        var city_id: Int? = 0,
        var state_id: Int? = 0,
        var category_id: Int? = 0
)