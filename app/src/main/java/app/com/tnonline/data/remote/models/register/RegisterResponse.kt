package app.com.tnonline.data.remote.models.register

data class RegisterResponse(
	val data: Data? = null,
	val otp: Otp? = null,
	val message: String? = null,
	val status: String? = null
)
