package app.com.tnonline.data.remote.models.lawyer.request

data class LawyerRequest(
        var city_id: Int = 0,
        var state_id: Int = 0
)