package app.com.tnonline.data.remote.models.recharge.orderstatusupdate;

public class OrderStatusRequest {
    private String recharge_id ;
    private PaymentDesc payment_desc;
    private String payment_txn_id;
    private String payment_status;
    private String order_id;

    public void setRechargeId(String rechargeId) {
        this.recharge_id = rechargeId;
    }

    public String getRechargeId() {
        return recharge_id;
    }

    public void setPaymentDesc(PaymentDesc paymentDesc) {
        this.payment_desc = paymentDesc;
    }

    public PaymentDesc getPaymentDesc() {
        return payment_desc;
    }

    public void setPaymentTxnId(String paymentTxnId) {
        this.payment_txn_id = paymentTxnId;
    }

    public String getPaymentTxnId() {
        return payment_txn_id;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.payment_status = paymentStatus;
    }

    public String getPaymentStatus() {
        return payment_status;
    }

    public void setOrderId(String orderId) {
        this.order_id = orderId;
    }

    public String getOrderId() {
        return order_id;
    }

    @Override
    public String toString() {
        return
                "OrderStatusRequest{" +
                        "recharge_id = '" + recharge_id + '\'' +
                        ",payment_desc = '" + payment_desc + '\'' +
                        ",payment_txn_id = '" + payment_txn_id + '\'' +
                        ",payment_status = '" + payment_status + '\'' +
                        ",order_id = '" + order_id + '\'' +
                        "}";
    }
}
