package app.com.tnonline.data.remote.models.appoinment

data class AppoinmentRequest(
        var classifieds_id: Int? = 0,
        var name: String? = null,
        var number: String? = null,
        var email: String? = null,
        var professionals: Int? = null,
        var appointment_date: String? = null,
        var appointment_time: String? = null,
        var comments: String? = null
)