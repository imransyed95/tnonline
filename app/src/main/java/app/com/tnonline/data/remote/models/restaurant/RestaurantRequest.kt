package app.com.tnonline.data.remote.models.restaurant

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class RestaurantRequest {

    @SerializedName("city_id")
    @Expose
    var cityId: String? = null
    @SerializedName("category_id")
    @Expose
    var categoryId: String? = null
    @SerializedName("locality_id")
    @Expose
    var localityId: String? = null
    @SerializedName("state_id")
    @Expose
    var stateId: String? = null
    @SerializedName("mid_id")
    @Expose
    var midId: String? = null
    @SerializedName("amenities")
    @Expose
    var amenities: String? = null
    @SerializedName("cuisine")
    @Expose
    var cuisine: String? = null

}