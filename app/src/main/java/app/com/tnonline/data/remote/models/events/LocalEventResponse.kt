package app.com.tnonline.data.remote.models.events

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



data class LocalEventResponse (

    @SerializedName("status")
    @Expose
     val status: String? = null,
    @SerializedName("message")
    @Expose
     val message: String? = null,
    @SerializedName("data")
    @Expose
     val data: Datas? = null

    )


data class Datas(

    @SerializedName("current_page")
    @Expose
     val currentPage: Int? = null,
    @SerializedName("data")
    @Expose
     val data: List<Datums>? = null,
    @SerializedName("first_page_url")
    @Expose
     val firstPageUrl: String? = null,
    @SerializedName("from")
    @Expose
     val from: Int? = null,
    @SerializedName("last_page")
    @Expose
     val lastPage: Int? = null,
    @SerializedName("last_page_url")
    @Expose
     val lastPageUrl: String? = null,
    @SerializedName("next_page_url")
    @Expose
     val nextPageUrl: Any? = null,
    @SerializedName("path")
    @Expose
     val path: String? = null,
    @SerializedName("per_page")
    @Expose
     val perPage: Int? = null,
    @SerializedName("prev_page_url")
    @Expose
     val prevPageUrl: Any? = null,
    @SerializedName("to")
    @Expose
     val to: Int? = null,
    @SerializedName("total")
    @Expose
     val total: Int? = null
)

data class Datums(


    @SerializedName("event_id")
    @Expose
     val eventId: Int? = null,
    @SerializedName("security_id")
    @Expose
     val securityId: Int? = null,
    @SerializedName("security_code")
    @Expose
     val securityCode: String? = null,
    @SerializedName("event_code")
    @Expose
     val eventCode: String? = null,
    @SerializedName("host_name")
    @Expose
     val hostName: String? = null,
    @SerializedName("host_email")
    @Expose
     val hostEmail: String? = null,
    @SerializedName("host_mobile_no")
    @Expose
     val hostMobileNo: String? = null,
    @SerializedName("title_of_event")
    @Expose
     val titleOfEvent: String? = null,
    @SerializedName("event_description")
    @Expose
     val eventDescription: String? = null,
    @SerializedName("cover_image")
    @Expose
     val coverImage: String? = null,
    @SerializedName("target_audience")
    @Expose
     val targetAudience: String? = null,
    @SerializedName("event_category_id")
    @Expose
     val eventCategoryId: Int? = null,
    @SerializedName("amenities")
    @Expose
     val amenities: String? = null,
    @SerializedName("tags")
    @Expose
     val tags: String? = null,
    @SerializedName("start_date_time")
    @Expose
     val startDateTime: String? = null,
    @SerializedName("end_date_time")
    @Expose
     val endDateTime: String? = null,
    @SerializedName("start_date")
    @Expose
     val startDate: String? = null,
    @SerializedName("end_date")
    @Expose
     val endDate: String? = null,
    @SerializedName("facebook_url")
    @Expose
     val facebookUrl: String? = null,
    @SerializedName("twiter_url")
    @Expose
     val twiterUrl: String? = null,
    @SerializedName("instagram_url")
    @Expose
     val instagramUrl: String? = null,
    @SerializedName("tnonline_categories")
    @Expose
     val tnonlineCategories: Int? = null,
    @SerializedName("user_id")
    @Expose
     val userId: Int? = null,
    @SerializedName("rsvp_status")
    @Expose
     val rsvpStatus: Int? = null,
    @SerializedName("ticket_url")
    @Expose
     val ticketUrl: String? = null,
    @SerializedName("rsvp_questions")
    @Expose
     val rsvpQuestions: String? = null,
    @SerializedName("sms_alert")
    @Expose
     val smsAlert: Int? = null,
    @SerializedName("approval_by")
    @Expose
     val approvalBy: Int? = null,
    @SerializedName("approval_at")
    @Expose
     val approvalAt: String? = null,
    @SerializedName("show_event")
    @Expose
     val showEvent: Int? = null,
    @SerializedName("event_url")
    @Expose
     val eventUrl: String? = null,
    @SerializedName("comments")
    @Expose
     val comments: String? = null,
    @SerializedName("address1")
    @Expose
     val address1: String? = null,
    @SerializedName("address2")
    @Expose
     val address2: String? = null,
    @SerializedName("locality_id")
    @Expose
     val localityId: Int? = null,
    @SerializedName("zone_id")
    @Expose
     val zoneId: Int? = null,
    @SerializedName("city_id")
    @Expose
     val cityId: Int? = null,
    @SerializedName("state_id")
    @Expose
     val stateId: Int? = null,
    @SerializedName("pin_code")
    @Expose
     val pinCode: Int? = null,
    @SerializedName("module_id")
    @Expose
     val moduleId: Int? = null,
    @SerializedName("category_id")
    @Expose
     val categoryId: Int? = null,
    @SerializedName("created_at")
    @Expose
     val createdAt: String? = null,
    @SerializedName("created_by")
    @Expose
     val createdBy: Int? = null,
    @SerializedName("updated_at")
    @Expose
     val updatedAt: String? = null,
    @SerializedName("updated_by")
    @Expose
     val updatedBy: Int? = null,
    @SerializedName("current_plan_post")
    @Expose
     val currentPlanPost: Int? = null,
    @SerializedName("post_expire_status")
    @Expose
     val postExpireStatus: Int? = null,
    @SerializedName("email_status")
    @Expose
     val emailStatus: Any? = null,
    @SerializedName("status")
    @Expose
     val status: Int? = null,
    @SerializedName("latitude")
    @Expose
     val latitude: Any? = null,
    @SerializedName("longitude")
    @Expose
     val longitude: Any? = null

)