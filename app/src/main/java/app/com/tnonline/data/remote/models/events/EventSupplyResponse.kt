package app.com.tnonline.data.remote.models.events

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class EventSupplyResponse (

        @SerializedName("status")
        @Expose
        val status: String? = null,
        @SerializedName("message")
        @Expose
        val message: String? = null,
        @SerializedName("data")
        @Expose
        val data: EventPageLink? = null

)

class EventPageLink{

    @SerializedName("current_page")
    @Expose
     val currentPage: Int? = null
    @SerializedName("data")
    @Expose
     val data: List<SupplyData>? = null
    @SerializedName("first_page_url")
    @Expose
     val firstPageUrl: String? = null
    @SerializedName("from")
    @Expose
     val from: Int? = null
    @SerializedName("last_page")
    @Expose
     val lastPage: Int? = null
    @SerializedName("last_page_url")
    @Expose
     val lastPageUrl: String? = null
    @SerializedName("next_page_url")
    @Expose
     val nextPageUrl: Any? = null
    @SerializedName("path")
    @Expose
     val path: String? = null
    @SerializedName("per_page")
    @Expose
     val perPage: Int? = null
    @SerializedName("prev_page_url")
    @Expose
     val prevPageUrl: Any? = null
    @SerializedName("to")
    @Expose
     val to: Int? = null
    @SerializedName("total")
    @Expose
     val total: Int? = null
}

class SupplyData{

    @SerializedName("id")
    @Expose
     val id: Int? = null
    @SerializedName("description")
    @Expose
     val description: String? = null
    @SerializedName("supply_for")
    @Expose
     val supplyFor: List<String>? = null
    @SerializedName("can_supply")
    @Expose
     val canSupply: List<String>? = null
    @SerializedName("chair_type")
    @Expose
     val chairType: List<String>? = null
    @SerializedName("table_type")
    @Expose
     val tableType: List<String>? = null
    @SerializedName("table_tops")
    @Expose
     val tableTops: List<Any>? = null
    @SerializedName("tent_type")
    @Expose
     val tentType: List<String>? = null
    @SerializedName("sounds_lightening")
    @Expose
     val soundsLightening: List<Any>? = null
    @SerializedName("user_name")
    @Expose
     val userName: String? = null
    @SerializedName("company_name")
    @Expose
     val companyName: String? = null
    @SerializedName("facebook_url")
    @Expose
     val facebookUrl: String? = null
    @SerializedName("twitter_url")
    @Expose
     val twitterUrl: String? = null
    @SerializedName("instagram_url")
    @Expose
     val instagramUrl: String? = null
    @SerializedName("address")
    @Expose
     val address: String? = null
    @SerializedName("pincode")
    @Expose
     val pincode: Any? = null
    @SerializedName("city_name")
    @Expose
     val cityName: String? = null
    @SerializedName("link")
    @Expose
     val link: String? = null
    @SerializedName("locality_name")
    @Expose
     val localityName: String? = null
    @SerializedName("contact_no")
    @Expose
     val contactNo: String? = null
    @SerializedName("email")
    @Expose
     val email: String? = null
    @SerializedName("image_name")
    @Expose
     val imageName: String? = null
}

