package app.com.tnonline.data.remote.models.recharge.orderstatusupdate;

public class PaymentDesc{
	private String status;
	private String paymentTransId;

	public PaymentDesc(String status, String paymentTransId) {
		this.status = status;
		this.paymentTransId = paymentTransId;
	}

	public PaymentDesc() {

	}

	public String getPaymentTransId() {
		return paymentTransId;
	}

	public void setPaymentTransId(String paymentTransId) {
		this.paymentTransId = paymentTransId;
	}



	public void setSstatus(String status){
		this.status = status;
	}

	public String getSstatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"PaymentDesc{" +
			"status = '" + status + '\'' +
					"paymentTransId = '" + paymentTransId + '\'' +
			"}";
		}
}
