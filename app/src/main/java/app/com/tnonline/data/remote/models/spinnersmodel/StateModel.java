package app.com.tnonline.data.remote.models.spinnersmodel;

public class StateModel{
	private int code;
	private String state;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"StateModel{" + 
			"code = '" + code + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}
