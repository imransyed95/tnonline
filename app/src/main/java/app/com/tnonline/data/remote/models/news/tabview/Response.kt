package app.com.tnonline.data.remote.models.news.tabview

data class Response(
	val data: Data? = null,
	val message: String? = null,
	val status: String? = null
)
