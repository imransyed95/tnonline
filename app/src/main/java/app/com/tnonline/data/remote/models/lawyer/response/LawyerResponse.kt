package app.com.tnonline.data.remote.models.lawyer.response

data class LawyerResponse(
        val data: ArrayList<Datum>? = null,
        val message: String? = null,
        val status: String? = null
)