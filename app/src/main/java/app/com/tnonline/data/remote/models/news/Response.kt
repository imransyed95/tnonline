package app.com.tnonline.data.remote.models.news

//import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

//@Generated("com.robohorse.robopojogenerator")
data class Response(

	@field:SerializedName("data")
	val data: List<News>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)