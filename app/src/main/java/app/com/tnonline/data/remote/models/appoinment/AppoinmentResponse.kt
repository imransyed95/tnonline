package app.com.tnonline.data.remote.models.appoinment

data class AppoinmentResponse(
        var status: String? = null,
        var msg: String? = null,
        var data: Boolean? = false
)