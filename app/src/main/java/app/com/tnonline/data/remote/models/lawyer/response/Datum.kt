package app.com.tnonline.data.remote.models.lawyer.response

data class Datum(
        val category_id: Int? = 0,
        val module_id: Int? = 0,
        val category_name: String? = null,
        val subcategory_end: String? = null,
        val message: String? = null,
        val created_by: Int? = 0,
        val created_at: String? = null,
        val updated_by: Int? = 0,
        val updated_at: String? = null,
        val category_status: Int? = 0,
        val category_image: String? = null
)