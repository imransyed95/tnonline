package app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.TrainBetweenStation

import com.google.gson.annotations.SerializedName

data class Classes (
    @field:SerializedName("available")
    val available: String? = null,
    @field:SerializedName("name")
    val name: String? = null,
    @field:SerializedName("code")
    val code: String? = null
)