package app.com.tnonline.data.remote.models.recharge.createorder;

public class RechargeRequest {
    private String number;
    private String recharge_type;
    private String operator_id;
    private String price;
    private String domain;
    private String state_id;
    private String city_id;



    private String device_type;

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setRechargeType(String rechargeType) {
        this.recharge_type = rechargeType;
    }

    public String getRechargeType() {
        return recharge_type;
    }

    public void setOperatorId(String operatorId) {
        this.operator_id = operatorId;
    }

    public String getOperatorId() {
        return operator_id;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getDomain() {
        return domain;
    }

    public void setStateId(String stateId) {
        this.state_id = stateId;
    }

    public String getStateId() {
        return state_id;
    }

    public void setCityId(String cityId) {
        this.city_id = cityId;
    }

    public String getCityId() {
        return city_id;
    }
    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }
    @Override
    public String toString() {
        return
                "RechargeRequest{" +
                        "number = '" + number + '\'' +
                        ",recharge_type = '" + recharge_type + '\'' +
                        ",operator_id = '" + operator_id + '\'' +
                        ",price = '" + price + '\'' +
                        ",domain = '" + domain + '\'' +
                        ",state_id = '" + state_id + '\'' +
                        ",city_id = '" + city_id + '\'' +
                        "}";
    }
}
