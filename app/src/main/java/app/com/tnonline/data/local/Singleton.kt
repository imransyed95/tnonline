package app.com.tnonline.data.local

import android.annotation.SuppressLint
import app.com.tnonline.data.remote.models.doctor.response.Datum

class SingleTon private constructor()//  mString = "Hello";
{
    var doctorDetailsList = Datum()
    var lawyerDetailsList = ArrayList<Datum>()
    var classified_id: Int? = 0


    companion object {

        @SuppressLint("StaticFieldLeak")
        private var mInstance: SingleTon? = null

        val instance: SingleTon
            get() {
                if (mInstance == null) {
                    mInstance = SingleTon()
                }
                return mInstance!!
            }
    }


}
