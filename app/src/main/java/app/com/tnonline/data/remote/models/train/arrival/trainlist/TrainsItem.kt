package app.com.tnonline.data.remote.models.train.arrival.trainlist

import com.google.gson.annotations.SerializedName

data class TrainsItem(

	@field:SerializedName("actarr")
	val actarr: String? = null,

	@field:SerializedName("number")
	val number: String? = null,

	@field:SerializedName("actdep")
	val actdep: String? = null,

	@field:SerializedName("schdep")
	val schdep: String? = null,

	@field:SerializedName("scharr")
	val scharr: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("delaydep")
	val delaydep: String? = null,

	@field:SerializedName("delayarr")
	val delayarr: String? = null
)