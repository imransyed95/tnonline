package app.com.tnonline.data.remote.models.train.arrival.arrival

import app.com.tnonline.data.remote.models.train.arrival.trainlist.TrainsItem
import com.google.gson.annotations.SerializedName

data class TrainArrivalsResponse(

	@field:SerializedName("response_code")
	val responseCode: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("debit")
	val debit: Int? = null,

	@field:SerializedName("trains")
	val trains: ArrayList<TrainsItem?>? = null
)