package app.com.tnonline.data.remote.models.register

data class Otp(
	val status: String? = null,
	val details: String? = null
)
