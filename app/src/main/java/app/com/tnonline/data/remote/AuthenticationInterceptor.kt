package app.com.tnonline.data.remote


import app.com.tnonline.data.local.SharedPreferences
import app.com.tnonline.utils.Constants
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.security.cert.Extension

class AuthenticationInterceptor : Interceptor {

    private val appPreferences = SharedPreferences()
    var request: Request? = null
    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            val tokenString = appPreferences.getString(Constants.LOGIN_AUTH)
            if (!tokenString.isNullOrEmpty()) {
                val original = chain.request()
                val builder = original.newBuilder()
                        .header("Authorization", "Bearer $tokenString")
                request = builder.build()
            }
            return chain.proceed(request!!)
        }catch (e:Exception)
        {

        }

        return chain.proceed(request!!)
    }
}