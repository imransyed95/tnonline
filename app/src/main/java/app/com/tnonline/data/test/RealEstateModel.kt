package app.com.tnonline.data.test

import app.com.tnonline.R

data class RealEstateModel (

    val image:Int= R.drawable.test_apartment_img,
    val name:String="",
    val price:String=""
)