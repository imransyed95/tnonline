package app.com.tnonline.data.test

import app.com.tnonline.R

data class PartHallModel (
    val image:String?= "",
    val name:String?="",
    val address:String?="",
    val type:String?="",
    val contact:String?= "",
    val email:String?="",
    val capacity:String?="",
    val description:String?="",
    var venueType:ArrayList<String>,
    var facilities:ArrayList<String>,
    var eventType:ArrayList<String>,
    var cateringFeature:ArrayList<String>,
    var cateringCuisine:ArrayList<String>,
    var tableType:ArrayList<String>,
    var tentType:ArrayList<String>

)