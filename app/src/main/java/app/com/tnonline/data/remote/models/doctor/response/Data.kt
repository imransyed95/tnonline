package app.com.tnonline.data.remote.models.doctor.response


data class Data (
        val currentPage: Int? = null,
        val data: List<Datum>? = null,
        val firstPageUrl: String? = null,
        val from: Int? = null,
        val lastPage: Int? = null,
        val lastPageUrl: String? = null,
        val nextPageUrl: Any? = null,
        val path: String? = null,
        val perPage: Int? = null,
        val prevPageUrl: String? = null,
        val to: Int? = null,
        val total: Int? = null
)

