package app.com.tnonline.data.remote.models.flight

data class DataItem(
	val updatedAt: String? = null,
	val updatedBy: String? = null,
	val analyticsId: String? = null,
	val createdAt: String? = null,
	val iframe: String? = null,
	val stateId: Int? = null,
	val title: String? = null,
	val category: String? = null,
	val createdBy: String? = null,
	val iframeUrl: String? = null,
	val cityId: Int? = null,
	val status: String? = null,
	val cityName: String? = null
)
