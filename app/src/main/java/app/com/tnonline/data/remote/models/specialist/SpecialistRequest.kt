package app.com.tnonline.data.remote.models.locality

data class SpecialistRequest(
        var city_id: Int? = 0,
        var state_id: Int? = 0,
        var locality_id: Int? = 0
)