package app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.TrainBetweenStation

import com.google.gson.annotations.SerializedName

data class fromStation (
        @field:SerializedName("code")
        val code: String? = null,
        @field:SerializedName("name")
        val name: String? = null
)
