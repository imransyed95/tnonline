package app.com.tnonline.data.remote.models.events

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class PartyHallResponse(

        @SerializedName("status")
        @Expose
        val status: String? = null,
        @SerializedName("message")
        @Expose
        val message: String? = null,
        @SerializedName("data")
        @Expose val data: Data? = null

)

data class Data(

        @SerializedName("current_page")
        @Expose
        val currentPage: Int? = null,
        @SerializedName("data")
        @Expose
        val data: List<Datum>? = null,
        @SerializedName("first_page_url")
        @Expose
        val firstPageUrl: String? = null,
        @SerializedName("last_page")
        @Expose
        val lastPage: Int? = null,
        @SerializedName("last_page_url")
        @Expose
        val lastPageUrl: String? = null,
        @SerializedName("next_page_url")
        @Expose
        val nextPageUrl: String? = null,
        @SerializedName("per_page")
        @Expose
        val perPage: Int? = null,
        @SerializedName("prev_page_url")
        @Expose
        val prevPageUrl: Any? = null,
        @SerializedName("total")
        @Expose
        val total: Int? = null

)

data class Datum(

        @SerializedName("id")
        @Expose
        val id: Int? = null,
        @SerializedName("name_of_hall")
        @Expose
        val nameOfHall: String? = null,
        @SerializedName("venu_type")
        @Expose
        val venuType: String? = null,
        @SerializedName("capacity")
        @Expose
        val capacity: String? = null,
        @SerializedName("facility")
        @Expose
        val facility: List<String>? = null,
        @SerializedName("features")
        @Expose
        val features: List<String>? = null,
        @SerializedName("best_suited")
        @Expose
        val bestSuited: List<String>? = null,
        @SerializedName("facebook_url")
        @Expose
        val facebookUrl: String? = null,
        @SerializedName("twitter_url")
        @Expose
        val twitterUrl: String? = null,
        @SerializedName("instagram_url")
        @Expose
        val instagramUrl: String? = null,
        @SerializedName("description")
        @Expose
        val description: String? = null,
        @SerializedName("venue_configuration")
        @Expose
        val venueConfiguration: List<String>? = null,
        @SerializedName("adrress")
        @Expose
        val adrress: String? = null,
        @SerializedName("pincode")
        @Expose
        val pincode: Int? = null,
        @SerializedName("user_email")
        @Expose
        val userEmail: String? = null,
        @SerializedName("user_mobile")
        @Expose
        val userMobile: String? = null,
        @SerializedName("user_name")
        @Expose
        val userName: String? = null,
        @SerializedName("image_name")
        @Expose
        val imageName: String? = null
)


