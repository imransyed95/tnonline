package app.com.tnonline.data.remote.models.login

data class LoginResponse(
	val data: Data? = null,
	val city: City? = null,
	val message: String? = null,
	val status: String? = null,
	val token: String? = null
)
