package app.com.tnonline.data.remote.models.city

data class CityListResponse(
		val msg: String? = null,
		val data: ArrayList<DataItem>? = null,
		val status: String? = null
)
