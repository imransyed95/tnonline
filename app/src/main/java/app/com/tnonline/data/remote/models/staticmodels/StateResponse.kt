package app.com.tnonline.data.remote.models.staticmodels

data class StateResponse(
        var code: Int? = null,
        var state: String? = null
)
