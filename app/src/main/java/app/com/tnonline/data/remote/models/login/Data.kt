package app.com.tnonline.data.remote.models.login

data class Data(
	val pincode: String? = null,
	val role: String? = null,
	val address2: String? = null,
	val address1: String? = null,
	val tnonline_categories: String? = null,
	val created_at: String? = null,
	val last_login_date: String? = null,
	val created_by: Int? = null,
	val verify_user: Int? = null,
	val full_name: String? = null,
	val city_zone: String? = null,
	val updated_at: String? = null,
	val user_id: Int? = null,
	val zone_admin_count: Int? = null,
	val dob: String? = null,
	val contact_no: String? = null,
	val updated_by: Int? = null,
	val place: String? = null,
	val state_id: Int? = null,
	val email: String? = null,
	val city_id: Int? = null,
	val status: Int? = null
)
