package app.com.tnonline.data.remote.models.staticmodels

data class ProviderOperatorResponse(
        val code: String? = null,
        val type: String? = null,
        val operator: String? = null
)
