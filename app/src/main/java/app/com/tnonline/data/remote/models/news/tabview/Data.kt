package app.com.tnonline.data.remote.models.news.tabview

data class Data(
	val language: List<String?>? = null,
	val category: List<String?>? = null
)
