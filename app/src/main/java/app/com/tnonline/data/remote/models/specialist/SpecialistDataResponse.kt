package app.com.tnonline.data.remote.models.locality

data class SpecialistDataResponse(

        var status: String? = null,
        var msg: String? = null,
        var data: List<SpecialistData>?  = null

)

data class SpecialistData (
        var propertytype_id: Int = 0,
        var propertytype_name: String? = null,
        var img_name: String? = null,
        val created_at: String? = null,
        var created_by: String? = null,
        var updated_at:String?=null,
        var updated_by:String?=null,
        var status: String? = null,
        var property_category: Int = 0
)
