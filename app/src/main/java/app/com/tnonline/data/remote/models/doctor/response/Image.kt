package app.com.tnonline.data.remote.models.doctor.response

data class Image (
        var img_id:Int?=0,
        var image_name:String?=null,
        var status:Int?=0,
        var module_id:Int?=0,
        var category_id:Int?=0,
        var subcategory_id:Int?=0,
        var created_at:String?=null,
        var created_by:String?=null,
        var updated_at:String?=null,
        var updated_by:String?=null

)