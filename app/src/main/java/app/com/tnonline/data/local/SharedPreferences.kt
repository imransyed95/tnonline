package app.com.tnonline.data.local

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import app.com.tnonline.TNOnlineApp

class SharedPreferences {

    private val mSharedPreferences: SharedPreferences

    init {
        val mContext = TNOnlineApp.getInstance()
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
    }

    companion object {

        private const val APP_ENV = "APP_ENV"
        private const val APP_ENV_DEV = "dev"
        private const val APP_ENV_TES = "testing"
        private const val APP_ENV_STAGE = "staging"
        private const val APP_ENV_PROD = "prod"
        private const val AUTH_TOKEN = "auth_token"
        private const val IS_TABLET = "is_tablet"
        private val IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch"


        fun getAppEnv(context: Context): Boolean {
            return APP_ENV_DEV == PreferenceManager.getDefaultSharedPreferences(context).getString(APP_ENV, "")
        }
    }

    val isTablet: Boolean
        get() = mSharedPreferences.getBoolean(IS_TABLET, false)

    fun setString(key: String, value: String) {
        mSharedPreferences.edit().putString(key, value).apply()
    }

    fun getString(key: String): String {
        return mSharedPreferences.getString(key, "")
    }

    fun setInt(key: String, value: Int) {
        mSharedPreferences.edit().putInt(key, value).apply()
    }

    fun getInt(key: String): Int {
        return mSharedPreferences.getInt(key, 0)
    }

    fun setFloat(key: String, value: Float) {
        mSharedPreferences.edit().putFloat(key, value).apply()
    }

    fun getFloat(key: String): Float {
        return mSharedPreferences.getFloat(key, 0f)
    }

    fun setBool(key: String, value: Boolean) {
        mSharedPreferences.edit().putBoolean(key, value).apply()
    }

    fun getBool(key: String): Boolean {
        return mSharedPreferences.getBoolean(key, false)
    }

    fun setIsTablet() {
        val editor = mSharedPreferences.edit()
        editor.putBoolean(IS_TABLET, true)
        editor.apply()
    }

    fun setFirstTimeLaunch(isFirstTime: Boolean) {
        val editor = mSharedPreferences.edit()
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime)
        editor.apply()
    }

    fun isFirstTimeLaunch(): Boolean {
        return mSharedPreferences.getBoolean(IS_FIRST_TIME_LAUNCH, true)
    }
}