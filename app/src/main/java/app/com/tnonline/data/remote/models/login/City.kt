package app.com.tnonline.data.remote.models.login

data class City(
        val img: String? = null,
        val weather_api_id: Int? = null,
        val latitude: String? = null,
        val icon: String? = null,
        val rating: Int? = null,
        val link: String? = null,
        val city_code: String? = null,
        val created_at: String? = null,
        val created_by: Int? = null,
        val city_image: String? = null,
        val city_name: String? = null,
        val updated_at: String? = null,
        val city_status: Int? = null,
        val updated_by: Int? = null,
        val state_id: Int? = null,
        val city_id: Int? = null,
        val longitude: String? = null
)
