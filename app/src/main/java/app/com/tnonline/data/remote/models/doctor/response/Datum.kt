package app.com.tnonline.data.remote.models.doctor.response




data class Datum (
     val classifieds_id: Int? = null,
     val specialist_in: String? = null,
     val services: String? = null,
     val educational_qualification: String? = null,
     val licence_number: String? = null,
     val description: String? = null,
     val company_name: String? = null,
     val others: String? = null,
     val fb_id: String? = null,
     val twitter_id: String? = null,
     val instagram_id: String? = null,
     val host_name: String? = null,
     val host_email: String? = null,
     val contact_no: String? = null,
     val practicing_at: String? = null,
     val appointments: String? = null,
     val address: String? = null,
     val locality_id: Int? = null,
     val zone_id: Int? = null,
     val city_id: Int? = null,
     val state_id: Int? = null,
     val pincode: Int? = null,
     val module_id: Int? = null,
     val category_id: Int? = null,
     val subcategory_id: Int? = null,
     val user_id: Int? = null,
     val created_at: String? = null,
     val created_by: String? = null,
     val updated_at: String? = null,
     val updated_by: String? = null,
     val current_plan_post: Int? = null,
     val post_expire_status: Int? = null,
     val email_status: String? = null,
     val status: Int? = null,
     val image: String? = null
)