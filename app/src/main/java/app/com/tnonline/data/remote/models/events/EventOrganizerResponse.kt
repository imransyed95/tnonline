package app.com.tnonline.data.remote.models.events

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class EventOrganizerResponse (

    @SerializedName("status")
    @Expose
    val status: String? = null,
    @SerializedName("message")
    @Expose
    val message: String? = null,
    @SerializedName("data")
    @Expose
    val data: Link? = null
)

public class Link{

    @SerializedName("current_page")
    @Expose
     val currentPage: Int? = null
    @SerializedName("data")
    @Expose
     val data: List<OrganizerData>? = null
    @SerializedName("first_page_url")
    @Expose
     val firstPageUrl: String? = null
    @SerializedName("from")
    @Expose
     val from: Int? = null
    @SerializedName("last_page")
    @Expose
     val lastPage: Int? = null
    @SerializedName("last_page_url")
    @Expose
     val lastPageUrl: String? = null
    @SerializedName("next_page_url")
    @Expose
     val nextPageUrl: Any? = null
    @SerializedName("path")
    @Expose
     val path: String? = null
    @SerializedName("per_page")
    @Expose
     val perPage: Int? = null
    @SerializedName("prev_page_url")
    @Expose
     val prevPageUrl: Any? = null
    @SerializedName("to")
    @Expose
     val to: Int? = null
    @SerializedName("total")
    @Expose
     val total: Int? = null
}

public class OrganizerData{

    @SerializedName("organizers_id")
    @Expose
     val organizersId: Int? = null
    @SerializedName("event_title")
    @Expose
     val eventTitle: String? = null
    @SerializedName("event_type")
    @Expose
     val eventType: List<String>? = null
    @SerializedName("catering_features")
    @Expose
     val cateringFeatures: List<String>? = null
    @SerializedName("catering_services")
    @Expose
     val cateringServices: List<Any>? = null
    @SerializedName("catering_cusine")
    @Expose
     val cateringCusine: List<String>? = null
    @SerializedName("photography_facilities")
    @Expose
     val photographyFacilities: List<String>? = null
    @SerializedName("photography_features")
    @Expose
     val photographyFeatures: List<String>? = null
    @SerializedName("event_planner_facilities")
    @Expose
     val eventPlannerFacilities: List<String>? = null
    @SerializedName("dj_facilities")
    @Expose
     val djFacilities: List<Any>? = null
    @SerializedName("genre_of_music_features")
    @Expose
     val genreOfMusicFeatures: List<Any>? = null
    @SerializedName("decoration_features")
    @Expose
     val decorationFeatures: List<String>? = null
    @SerializedName("makeup_artist")
    @Expose
     val makeupArtist: List<String>? = null
    @SerializedName("makeup_features")
    @Expose
     val makeupFeatures: List<String>? = null
    @SerializedName("facebook_id")
    @Expose
     val facebookId: String? = null
    @SerializedName("twitter_id")
    @Expose
     val twitterId: String? = null
    @SerializedName("instagram_id")
    @Expose
     val instagramId: String? = null
    @SerializedName("event_description")
    @Expose
     val eventDescription: String? = null
    @SerializedName("email")
    @Expose
     val email: String? = null
    @SerializedName("contact_no")
    @Expose
     val contactNo: String? = null
    @SerializedName("address")
    @Expose
     val address: String? = null
    @SerializedName("pincode")
    @Expose
     val pincode: Any? = null
    @SerializedName("city_name")
    @Expose
     val cityName: String? = null
    @SerializedName("link")
    @Expose
     val link: String? = null
    @SerializedName("locality_name")
    @Expose
     val localityName: String? = null
    @SerializedName("image_name")
    @Expose
     val imageName: String? = null
}