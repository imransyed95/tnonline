package app.com.tnonline.data.remote.models.login

data class LoginRequest(
        var password: String? = null,
        var email: String? = null
)
