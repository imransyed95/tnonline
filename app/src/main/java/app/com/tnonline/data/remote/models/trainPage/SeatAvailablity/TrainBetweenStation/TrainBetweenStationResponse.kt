package app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.TrainBetweenStation

import com.google.gson.annotations.SerializedName

data class TrainBetweenStationResponse(

        @field:SerializedName("response_code")
        val responseCode: Int? = null,

        @field:SerializedName("total")
        val total: Int? = null,

        @field:SerializedName("debit")
        val debit: Int? = null,

        @field:SerializedName("trains")
        val trains: ArrayList<Trains?>? = null
)