package app.com.tnonline.data.remote.models.events

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



data class PartyHallRequest (

    @SerializedName("city_id")
    @Expose
    private val cityId: String? = null
)