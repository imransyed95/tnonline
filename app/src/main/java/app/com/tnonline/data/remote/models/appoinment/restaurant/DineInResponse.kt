package app.com.tnonline.data.remote.models.appoinment.restaurant


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class DineInResponse (
        @SerializedName("status")
        @Expose
        val status: String? = null,
        @SerializedName("message")
        @Expose
        val message: String? = null
)
