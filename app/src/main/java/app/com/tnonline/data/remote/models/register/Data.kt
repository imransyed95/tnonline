package app.com.tnonline.data.remote.models.register

data class Data(
	val pincode: Int? = null,
	val role: Int? = null,
	val address2: String? = null,
	val address1: String? = null,
	val tnonline_categories: String? = null,
	val created_at: String? = null,
	val last_login_date: String? = null,
	val created_by: Int? = null,
	val verify_user: Int? = null,
	val full_name: String? = null,
	val city_zone: String? = null,
	val updated_at: String? = null,
	val zone_admin_count: Int? = null,
	val user_id: Int? = null,
	val contact_no: String? = null,
	val dob: String? = null,
	val updated_by: Int? = null,
	val state_id: Int? = null,
	val place: String? = null,
	val email: String? = null,
	val city_id: String? = null,
	val status: Int? = null
)