package app.com.tnonline.data.remote.models.locality

data class LocalityDataResponse(

        var status: String? = null,
        var msg: String? = null,
        var data: List<LocalityData>?  = null

)