package app.com.tnonline.data.remote.models.locality

data class LocalityData (
        var locality_id: Int = 0,
        var city_id: Int = 0,
        var locality_name: String? = null,
        val locality_status: String? = null,
        var zone_id:Int?=0,
        var zone_status:Int?=0,
        var created_at:String?=null,
        var created_by:String?=null,
        var updated_at:String?=null,
        var updated_by:String?=null,
        var state_id:Int?=null
)
