package app.com.tnonline.data.remote.models.trainPage.SeatAvailablity


import com.google.gson.annotations.SerializedName

data class AutoCompleteResponse(

        @field:SerializedName("response_code")
        val responseCode: Int? = null,

        @field:SerializedName("debit")
        val debit: Int? = null,

        @field:SerializedName("stations")
        val stations: ArrayList<DataItem?>? = null
)