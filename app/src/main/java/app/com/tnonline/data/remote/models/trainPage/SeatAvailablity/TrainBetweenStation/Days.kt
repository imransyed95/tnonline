package app.com.tnonline.data.remote.models.trainPage.SeatAvailablity.TrainBetweenStation

import com.google.gson.annotations.SerializedName

data class Days (
        @field:SerializedName("runs")
        val runs: String? = null,
        @field:SerializedName("code")
        val code: String? = null
)