package app.com.tnonline.data.remote.models.flight

data class FlightResponse(
		val data: ArrayList<DataItem>? = null,
	val message: String? = null,
	val status: String? = null
)
