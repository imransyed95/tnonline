package app.com.tnonline.data.remote.models.news

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class News {

    @SerializedName("link")
    @Expose
    var link: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("image")
    @Expose
    var image: String? = null
    @SerializedName("Desc")
    @Expose
    var desc: String? = null
    @SerializedName("Pub")
    @Expose
    var pub: String? = null
}