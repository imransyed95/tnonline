package app.com.tnonline

import android.app.Application
import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager

class TNOnlineApp : Application() {


    companion object {
        private var sInstance: TNOnlineApp? = null

        fun getInstance(): TNOnlineApp? {
            return sInstance
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }

    override fun onCreate() {
        super.onCreate()

        sInstance = this

        val applicationInfo: ApplicationInfo
        try {
            applicationInfo = packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA)
            val bundle = applicationInfo.metaData
        } catch (e: PackageManager.NameNotFoundException) {
            // ignored
        }
    }

}